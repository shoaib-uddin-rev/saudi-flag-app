(self["webpackChunkSaudi_Flag_App"] = self["webpackChunkSaudi_Flag_App"] || []).push([["src_app_pages_flag-history_flag-history_module_ts"],{

/***/ 94161:
/*!***************************************************************************!*\
  !*** ./src/app/pages/flag-history/content-area/content-area.component.ts ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ContentAreaComponent": () => (/* binding */ ContentAreaComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _raw_loader_content_area_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./content-area.component.html */ 5376);
/* harmony import */ var _content_area_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./content-area.component.scss */ 44296);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 42741);




let ContentAreaComponent = class ContentAreaComponent {
    constructor(renderer2) {
        this.renderer2 = renderer2;
        this.showScroll = true;
        this.lang = 'en';
        this.top = 0;
        this.textHeight = 240;
        this.imageHeight = 0;
        this.textScrollHeight = 240;
        this.options = { autoHide: false };
        this.lang = localStorage.getItem('lang');
    }
    get contentHeight() {
        return this.tcontentHeight;
    }
    set contentHeight(value) {
        this.tcontentHeight = value;
        console.log(value);
    }
    get content() {
        return this.tcontent;
    }
    set content(value) {
        this.tcontent = value;
        this.getImageHeight();
    }
    ngOnInit() { }
    getImageHeight() {
        setTimeout(() => (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__awaiter)(this, void 0, void 0, function* () {
            const scrollElement = yield this.imageView.nativeElement;
            const p = scrollElement.clientHeight;
            const textElement = yield this.htmlcontent.nativeElement;
            const q = textElement.scrollHeight;
            console.log(q);
            if (p != 0) {
                this.imageHeight = p;
                console.log('image height', p);
                console.log('text height', q);
                console.log('scroll height', this.contentHeight);
                // this.textHeight = this.contentHeight - p - 40;
                console.log(this.textHeight);
            }
        }), 1000);
    }
    logScrolling($event) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__awaiter)(this, void 0, void 0, function* () {
            const offset = $event.target.offsetHeight;
            // console.log($event);
            const scrollTop = $event.target.scrollTop;
            const scrollHeight = $event.target.scrollHeight;
            const parent = yield this.parentScroll.nativeElement;
            const po = parent.scrollTop;
            // console.log('po' + po);
            // console.log('parent' + parent);
            // let _offset = scrollHeight > 1000 ? (offset + scrollHeight) : offset - 200;
            // const denominator: number = scrollHeight > 1000 ? (_offset + scrollTop + 2000) : _offset + scrollTop + 250;
            // const sct: number = (scrollHeight) / denominator * scrollTop;
            console.log(scrollHeight);
            const sh = scrollHeight > 1000 ? scrollHeight + 150 : scrollHeight - 100;
            // console.log('sh' + sh);
            this.top = (this.textHeight / sh) * po; //this.top + 5;
            // console.log(scrollHeight, po, this.textHeight, this.top);
            // if (po >= offset) {
            //   return;
            // } else {
            let rtop = this.top.toString() + 'px';
            // console.log(rtop);
            this.renderer2.setStyle(this.scroll.nativeElement, 'margin-top', rtop);
            // }
        });
    }
};
ContentAreaComponent.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Renderer2 }
];
ContentAreaComponent.propDecorators = {
    scroll: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.ViewChild, args: ['scroll', { read: _angular_core__WEBPACK_IMPORTED_MODULE_3__.ElementRef },] }],
    scrolldiv: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.ViewChild, args: ['scrolldiv', { read: _angular_core__WEBPACK_IMPORTED_MODULE_3__.ElementRef },] }],
    imageView: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.ViewChild, args: ['imageView',] }],
    htmlcontent: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.ViewChild, args: ['htmlcontent',] }],
    parentScroll: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.ViewChild, args: ['parentScroll',] }],
    image: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Input }],
    slide: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Input }],
    showScroll: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Input }],
    lang: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Input }],
    contentHeight: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Input }],
    content: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Input }]
};
ContentAreaComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-content-area',
        template: _raw_loader_content_area_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_content_area_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], ContentAreaComponent);



/***/ }),

/***/ 91578:
/*!*******************************************************************!*\
  !*** ./src/app/pages/flag-history/flag-history-routing.module.ts ***!
  \*******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FlagHistoryPageRoutingModule": () => (/* binding */ FlagHistoryPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 29535);
/* harmony import */ var _flag_history_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./flag-history.page */ 4469);




const routes = [
    {
        path: '',
        component: _flag_history_page__WEBPACK_IMPORTED_MODULE_0__.FlagHistoryPage
    }
];
let FlagHistoryPageRoutingModule = class FlagHistoryPageRoutingModule {
};
FlagHistoryPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], FlagHistoryPageRoutingModule);



/***/ }),

/***/ 24821:
/*!***********************************************************!*\
  !*** ./src/app/pages/flag-history/flag-history.module.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FlagHistoryPageModule": () => (/* binding */ FlagHistoryPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ 16274);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ 93324);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic/angular */ 34595);
/* harmony import */ var _flag_history_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./flag-history-routing.module */ 91578);
/* harmony import */ var _flag_history_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./flag-history.page */ 4469);
/* harmony import */ var src_app_components_shared_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/components/shared.module */ 16369);
/* harmony import */ var _content_area_content_area_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./content-area/content-area.component */ 94161);
/* harmony import */ var simplebar_angular__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! simplebar-angular */ 16152);










let FlagHistoryPageModule = class FlagHistoryPageModule {
};
FlagHistoryPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_6__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_8__.IonicModule,
            _flag_history_routing_module__WEBPACK_IMPORTED_MODULE_0__.FlagHistoryPageRoutingModule,
            src_app_components_shared_module__WEBPACK_IMPORTED_MODULE_2__.SharedModule,
            simplebar_angular__WEBPACK_IMPORTED_MODULE_9__.SimplebarAngularModule
        ],
        declarations: [_flag_history_page__WEBPACK_IMPORTED_MODULE_1__.FlagHistoryPage, _content_area_content_area_component__WEBPACK_IMPORTED_MODULE_3__.ContentAreaComponent]
    })
], FlagHistoryPageModule);



/***/ }),

/***/ 4469:
/*!*********************************************************!*\
  !*** ./src/app/pages/flag-history/flag-history.page.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FlagHistoryPage": () => (/* binding */ FlagHistoryPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _raw_loader_flag_history_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./flag-history.page.html */ 23077);
/* harmony import */ var _flag_history_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./flag-history.page.scss */ 66357);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../base-page/base-page */ 62168);





let FlagHistoryPage = class FlagHistoryPage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injector) {
        super(injector);
        this.loading = true;
        this.sort = 0;
        this.first_slide_data = [];
        this.second_slide_data = [];
        this.third_slide_data = [];
        this.imagenum = 1;
        this.lang = 'en';
        this.contentHeight = 0;
        this.isNetworkOngoing = false;
        this.lang = localStorage.getItem('lang');
    }
    ngOnInit() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            yield this.initialize();
            this.sync();
        });
    }
    sync() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            if (!this.isNetworkOngoing) {
                yield this.sqlite.syncFlagHistoryData();
                this.isNetworkOngoing = false;
            }
        });
    }
    initialize() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            this.title = this.nav.getQueryParams().title;
            this.icon = this.nav.getQueryParams().icon;
            // this.network.getFlagHistory().then((res) => {
            //   console.log(res);
            // })
            const key = 'flag_history';
            // const res = this.jsonService.getJsonData(key);
            const res = yield this.sqlite.getFlagHistoryData(this.lang);
            this.first_slide_data = res.filter((x) => x.sort == '1');
            this.second_slide_data = res.filter((x) => x.sort == '2');
            this.third_slide_data = res.filter((x) => x.sort == '3');
            this.loading = false;
            const scrollElement = yield this.ionContent.getScrollElement();
            this.contentHeight = scrollElement.scrollHeight;
        });
    }
    moveSlide(num) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            this.sort = num;
            const scrollElement = yield this.ionContent.getScrollElement();
            this.contentHeight = scrollElement.scrollHeight;
            console.log(this.contentHeight);
        });
    }
};
FlagHistoryPage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.Injector }
];
FlagHistoryPage.propDecorators = {
    slides: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.ViewChild, args: ['slides', { static: true },] }],
    ionContent: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.ViewChild, args: ['ionContent',] }]
};
FlagHistoryPage = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Component)({
        selector: 'app-flag-history',
        template: _raw_loader_flag_history_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_flag_history_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], FlagHistoryPage);



/***/ }),

/***/ 44296:
/*!*****************************************************************************!*\
  !*** ./src/app/pages/flag-history/content-area/content-area.component.scss ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".text-div {\n  overflow: scroll;\n  padding: 0px 20px;\n}\n.text-div ::ng-deep p {\n  font-family: \"SST-Arabic\" !important;\n  color: var(--ion-color-primary);\n  font-size: 15px;\n  line-height: 22px;\n}\n.text-div ::ng-deep .mt-100 {\n  margin-top: 100px;\n}\n.text-div ::ng-deep strong {\n  width: 50px;\n  background: #5f503e;\n  padding: 2px 6px;\n  margin-bottom: 5px;\n  color: #ffffff;\n}\n.scroll-div {\n  display: flex;\n  width: 15px;\n  position: fixed;\n  margin-left: -1px;\n  justify-content: center;\n  padding-top: 20px;\n  background: #e6e6e6;\n  border-radius: 15px;\n}\n.scroll-div .scroll {\n  background: #6d5846;\n  width: 90%;\n  border: #fff 3px solid;\n  height: 35px;\n  border-radius: 33%;\n  position: relative;\n  z-index: 1;\n  overflow-y: scroll;\n}\n::-webkit-scrollbar {\n  display: none;\n}\n.left {\n  margin-left: 26px;\n}\n.right {\n  margin-right: 26px;\n}\n::ng-deep .date {\n  background: #6e5946;\n  text-align: center;\n  font-weight: 900;\n  color: #fff !important;\n}\n.content-ar ::ng-deep .date {\n  display: inline;\n  padding: 2px 15px;\n}\n.content-en ::ng-deep .date {\n  display: inline;\n  padding: 2px 15px;\n}\n.increased-height {\n  height: 60px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRlbnQtYXJlYS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFXQTtFQU1FLGdCQUFBO0VBQ0EsaUJBQUE7QUFmRjtBQXlCRTtFQUNFLG9DQUFBO0VBQ0EsK0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUF2Qko7QUF5QkU7RUFDRSxpQkFBQTtBQXZCSjtBQXlCRTtFQUNFLFdBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0FBdkJKO0FBMkJBO0VBQ0UsYUFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBRUEsaUJBQUE7RUFDQSx1QkFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtBQXpCRjtBQTJCRTtFQUNFLG1CQUFBO0VBQ0EsVUFBQTtFQUNBLHNCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0Esa0JBQUE7QUF6Qko7QUE2QkE7RUFDRSxhQUFBO0FBMUJGO0FBNkJBO0VBQ0UsaUJBQUE7QUExQkY7QUE2QkE7RUFDRSxrQkFBQTtBQTFCRjtBQTZCQTtFQUNFLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLHNCQUFBO0FBMUJGO0FBOEJFO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0FBM0JKO0FBZ0NFO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0FBN0JKO0FBaUNBO0VBQ0UsdUJBQUE7QUE5QkYiLCJmaWxlIjoiY29udGVudC1hcmVhLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmhpc3Rvcnktb3V0ZXIge1xyXG4gIC8vIHdpZHRoOiAxMDAlO1xyXG4gIC8vIGhlaWdodDogNzV2aDtcclxufVxyXG5cclxuLmltYWdlLWRpdiB7XHJcbiAgLy8gcG9zaXRpb246IGZpeGVkO1xyXG4gIC8vIGhlaWdodDogMzZ2aDtcclxuICAvLyB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLnRleHQtZGl2IHtcclxuICAvLyBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgLy8gLyogbWFyZ2luLXRvcDogMzZ2aDsgKi9cclxuICAvLyBoZWlnaHQ6IDM4dmg7XHJcbiAgLy8gdG9wOiA1OXZoO1xyXG4gIC8vIHdpZHRoOiAxMDAlO1xyXG4gIG92ZXJmbG93OiBzY3JvbGw7XHJcbiAgcGFkZGluZzogMHB4IDIwcHg7XHJcblxyXG4gIC8vIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAvLyAvKiBtYXJnaW4tdG9wOiAzNnZoOyAqL1xyXG4gIC8vIGhlaWdodDogMTAwJTtcclxuICAvLyB0b3A6IDM4dmg7XHJcbiAgLy8gd2lkdGg6IDEwMCU7XHJcbiAgLy8gb3ZlcmZsb3c6IHNjcm9sbCAhaW1wb3J0YW50O1xyXG4gIC8vIHBhZGRpbmc6IDBweCAyMHB4O1xyXG5cclxuICA6Om5nLWRlZXAgcCB7XHJcbiAgICBmb250LWZhbWlseTogXCJTU1QtQXJhYmljXCIgIWltcG9ydGFudDtcclxuICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XHJcbiAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICBsaW5lLWhlaWdodDogMjJweDtcclxuICB9XHJcbiAgOjpuZy1kZWVwIC5tdC0xMDAge1xyXG4gICAgbWFyZ2luLXRvcDogMTAwcHg7XHJcbiAgfVxyXG4gIDo6bmctZGVlcCBzdHJvbmcge1xyXG4gICAgd2lkdGg6IDUwcHg7XHJcbiAgICBiYWNrZ3JvdW5kOiAjNWY1MDNlO1xyXG4gICAgcGFkZGluZzogMnB4IDZweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDVweDtcclxuICAgIGNvbG9yOiAjZmZmZmZmO1xyXG4gIH1cclxufVxyXG5cclxuLnNjcm9sbC1kaXYge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgd2lkdGg6IDE1cHg7XHJcbiAgcG9zaXRpb246IGZpeGVkO1xyXG4gIC8vIGhlaWdodDogY2FsYygxMDAlIC0gODBweCk7XHJcbiAgbWFyZ2luLWxlZnQ6IC0xcHg7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgcGFkZGluZy10b3A6IDIwcHg7XHJcbiAgYmFja2dyb3VuZDogcmdiKDIzMCAyMzAgMjMwKTtcclxuICBib3JkZXItcmFkaXVzOiAxNXB4O1xyXG5cclxuICAuc2Nyb2xsIHtcclxuICAgIGJhY2tncm91bmQ6IHJnYigxMDksIDg4LCA3MCk7XHJcbiAgICB3aWR0aDogOTAlO1xyXG4gICAgYm9yZGVyOiAjZmZmIDNweCBzb2xpZDtcclxuICAgIGhlaWdodDogMzVweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDMzJTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHotaW5kZXg6IDE7XHJcbiAgICBvdmVyZmxvdy15OiBzY3JvbGw7XHJcbiAgfVxyXG59XHJcblxyXG46Oi13ZWJraXQtc2Nyb2xsYmFyIHtcclxuICBkaXNwbGF5OiBub25lO1xyXG59XHJcblxyXG4ubGVmdCB7XHJcbiAgbWFyZ2luLWxlZnQ6IDI2cHg7XHJcbn1cclxuXHJcbi5yaWdodCB7XHJcbiAgbWFyZ2luLXJpZ2h0OiAyNnB4O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLmRhdGUge1xyXG4gIGJhY2tncm91bmQ6ICM2ZTU5NDY7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGZvbnQtd2VpZ2h0OiA5MDA7XHJcbiAgY29sb3I6ICNmZmYgIWltcG9ydGFudDtcclxufVxyXG5cclxuLmNvbnRlbnQtYXIge1xyXG4gIDo6bmctZGVlcCAuZGF0ZSB7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmU7XHJcbiAgICBwYWRkaW5nOiAycHggMTVweDtcclxuICB9XHJcbn1cclxuXHJcbi5jb250ZW50LWVuIHtcclxuICA6Om5nLWRlZXAgLmRhdGUge1xyXG4gICAgZGlzcGxheTogaW5saW5lO1xyXG4gICAgcGFkZGluZzogMnB4IDE1cHg7XHJcbiAgfVxyXG59XHJcblxyXG4uaW5jcmVhc2VkLWhlaWdodCB7XHJcbiAgaGVpZ2h0OiA2MHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuIl19 */");

/***/ }),

/***/ 66357:
/*!***********************************************************!*\
  !*** ./src/app/pages/flag-history/flag-history.page.scss ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("ion-content {\n  --background: url('bg.png') 0 0/100% 100% no-repeat;\n}\n\n.green-bg {\n  background-color: var(--ion-color-primary) !important;\n  color: #fff !important;\n}\n\n.year-div {\n  background-color: var(--ion-color-primary);\n}\n\n.year-div .year-sub-div {\n  color: #fff;\n  font-weight: bolder;\n  text-align: center;\n  padding: 6px;\n  font-size: 12px;\n  border-color: #fff;\n  border-width: 1px;\n  border-style: solid;\n  border-top-width: 0px;\n  border-bottom-width: 0px;\n  height: 100%;\n}\n\n.tab-div {\n  background-color: #fff;\n  border: 1px solid var(--ion-color-primary);\n}\n\n.tab-div .tab-sub-div {\n  color: var(--ion-color-primary);\n  font-weight: bolder;\n  padding: 0;\n  height: 100%;\n  display: flex;\n  justify-content: center;\n  border-top: 1px solid #fff;\n}\n\n.tab-div .tab-sub-div .sub-div {\n  width: 33.33%;\n  text-align: center;\n}\n\n.tab-div .tab-sub-div:nth-child(1) {\n  border-right: 1px solid var(--ion-color-primary);\n}\n\n.tab-div .inner-sub-div {\n  border-right: 1px solid var(--ion-color-primary);\n}\n\n.history {\n  overflow: hidden;\n  position: relative;\n}\n\n.history ion-slide {\n  display: block;\n}\n\napp-content-area {\n  width: 100%;\n  display: block;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImZsYWctaGlzdG9yeS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxtREFBQTtBQUNKOztBQUVBO0VBQ0UscURBQUE7RUFDQSxzQkFBQTtBQUNGOztBQUVBO0VBQ0UsMENBQUE7QUFDRjs7QUFDRTtFQUNFLFdBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxxQkFBQTtFQUNBLHdCQUFBO0VBQ0EsWUFBQTtBQUNKOztBQUdBO0VBQ0Usc0JBQUE7RUFDQSwwQ0FBQTtBQUFGOztBQUVFO0VBQ0UsK0JBQUE7RUFDQSxtQkFBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsMEJBQUE7QUFBSjs7QUFFSTtFQUNFLGFBQUE7RUFDQSxrQkFBQTtBQUFOOztBQUlFO0VBQ0UsZ0RBQUE7QUFGSjs7QUFJRTtFQUNFLGdEQUFBO0FBRko7O0FBTUE7RUFHRSxnQkFBQTtFQUNBLGtCQUFBO0FBTEY7O0FBT0U7RUFDRSxjQUFBO0FBTEo7O0FBU0E7RUFDRSxXQUFBO0VBQ0EsY0FBQTtBQU5GIiwiZmlsZSI6ImZsYWctaGlzdG9yeS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XHJcbiAgICAtLWJhY2tncm91bmQ6IHVybChcIi4vLi4vLi4vLi4vYXNzZXRzL2ltZ3MvYmcucG5nXCIpIDAgMC8xMDAlIDEwMCUgbm8tcmVwZWF0O1xyXG59XHJcblxyXG4uZ3JlZW4tYmcge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KSAhaW1wb3J0YW50O1xyXG4gIGNvbG9yOiAjZmZmICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi55ZWFyLWRpdiB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG5cclxuICAueWVhci1zdWItZGl2IHtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHBhZGRpbmc6IDZweDtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIGJvcmRlci1jb2xvcjogI2ZmZjtcclxuICAgIGJvcmRlci13aWR0aDogMXB4O1xyXG4gICAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcclxuICAgIGJvcmRlci10b3Atd2lkdGg6IDBweDtcclxuICAgIGJvcmRlci1ib3R0b20td2lkdGg6IDBweDtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICB9XHJcbn1cclxuXHJcbi50YWItZGl2IHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxuXHJcbiAgLnRhYi1zdWItZGl2IHtcclxuICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XHJcbiAgICBmb250LXdlaWdodDogYm9sZGVyO1xyXG4gICAgcGFkZGluZzogMDtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCAjZmZmO1xyXG5cclxuICAgIC5zdWItZGl2IHtcclxuICAgICAgd2lkdGg6IDMzLjMzJTtcclxuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLnRhYi1zdWItZGl2Om50aC1jaGlsZCgxKSB7XHJcbiAgICBib3JkZXItcmlnaHQ6IDFweCBzb2xpZCB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XHJcbiAgfVxyXG4gIC5pbm5lci1zdWItZGl2IHtcclxuICAgIGJvcmRlci1yaWdodDogMXB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxuICB9XHJcbn1cclxuXHJcbi5oaXN0b3J5IHtcclxuICAvLyBoZWlnaHQ6IGNhbGMoMTAwdmggLSAyMDBweCk7XHJcbiAgLy8gYmFja2dyb3VuZDogYmx1ZTtcclxuICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuXHJcbiAgaW9uLXNsaWRlIHtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gIH1cclxufVxyXG5cclxuYXBwLWNvbnRlbnQtYXJlYSB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbn1cclxuXHJcbi8vIC50ZXh0LWRpdiB7XHJcbi8vICAgICBwIHtcclxuLy8gICAgICAgICBzcGFuIHtcclxuLy8gICAgICAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KSAhaW1wb3J0YW50O1xyXG4vLyAgICAgICAgIH1cclxuLy8gICAgIH1cclxuXHJcbi8vICAgICBwICB7XHJcbi8vICAgICAgICAgc3Ryb25nIHtcclxuLy8gICAgICAgICAgICAgY29sb3I6ICNmZmY7XHJcbi8vICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM1ZjUwM2U7XHJcbi8vICAgICAgICAgfVxyXG5cclxuLy8gICAgIH1cclxuLy8gfVxyXG4iXX0= */");

/***/ }),

/***/ 5376:
/*!*******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/flag-history/content-area/content-area.component.html ***!
  \*******************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<div class=\"history-outer\">\r\n  <div class=\"image-div\" #imageView>\r\n    <img [src]=\"image\">\r\n  </div>\r\n  <div (scroll)=\"logScrolling($event)\" class=\"text-div custom-scrollbar\" [class.text-left]=\"lang === 'en'\"\r\n    [class.text-right]=\"lang === 'ar'\" [style.height.px]=\"textHeight\" #parentScroll>\r\n    <div #scrolldiv *ngIf=\"showScroll\" class=\"scroll-div\" [style.height.px]=\"textHeight\">\r\n      <div #scroll class=\"scroll\" [ngClass]=\"slide == 1 ? 'increased-height' : ''\"></div>\r\n    </div>\r\n\r\n    <div [class]=\"lang == 'en' ? 'content-en' : 'content-ar'\">\r\n      <div [class]=\"lang == 'en' ? 'left' : 'right'\" [innerHtml]=\"content\" #htmlcontent></div>\r\n    </div>\r\n  </div>\r\n</div>\r\n");

/***/ }),

/***/ 23077:
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/flag-history/flag-history.page.html ***!
  \*************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header>\r\n  <app-header [title]=\"title\" format=\"png\" [icon]=\"icon\"></app-header>\r\n  <ion-toolbar [hidden]=\"loading\" style=\"padding-top: 0\" class=\"fade-in-top\">\r\n    <ion-grid class=\"ion-no-padding\">\r\n      <ion-row class=\"year-div ion-no-padding\" [ngClass]=\"lang == 'ar' ? 'rtl' : ''\">\r\n        <ion-col style=\"border-width: 0;\" class=\"year-sub-div ion-no-padding\" size=\"4\">\r\n          {{first_slide_data[0]?.date}}\r\n        </ion-col>\r\n        <ion-col class=\"year-sub-div ion-no-padding\" size=\"4\">\r\n          {{second_slide_data[0]?.date}}\r\n        </ion-col>\r\n        <ion-col style=\"border-width: 0;\" class=\"year-sub-div ion-no-padding\" size=\"4\">\r\n          {{third_slide_data[0]?.date}}\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row class=\"tab-div\" [ngClass]=\"lang == 'ar' ? 'rtl' : ''\">\r\n        <ion-col (click)=\"moveSlide(0)\" class=\"tab-sub-div\" [ngClass]=\"sort == 0 ? 'green-bg' : ''\" size=\"4\">\r\n          {{lang == 'en' ? '1' : '١'}}\r\n        </ion-col>\r\n        <ion-col class=\"tab-sub-div\" size=\"4\">\r\n          <div (click)=\"moveSlide(1)\" [ngClass]=\"sort == 1 ? 'green-bg' : ''\" class=\"sub-div inner-sub-div\">\r\n            {{lang == 'en' ? '2' : '٢'}}\r\n          </div>\r\n          <div (click)=\"moveSlide(2)\" [ngClass]=\"sort == 2 ? 'green-bg' : ''\" class=\"sub-div inner-sub-div\">\r\n            {{lang == 'en' ? '3' : '٣'}}\r\n          </div>\r\n          <div (click)=\"moveSlide(3)\" [style]=\"lang == 'ar' ? 'border-left: 1px solid var(--ion-color-primary);' : ''\"\r\n            [ngClass]=\"sort == 3 ? 'green-bg' : ''\" class=\"sub-div inner-sub-div\">\r\n            {{lang == 'en' ? '4' : '٤'}}\r\n          </div>\r\n        </ion-col>\r\n        <ion-col (click)=\"moveSlide(4)\" [ngClass]=\"sort == 4 ? 'green-bg' : ''\" class=\"tab-sub-div\" size=\"4\">\r\n          {{lang == 'en' ? '5' : '٥'}}\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n  </ion-toolbar>\r\n  <!-- <ion-toolbar>\r\n    <div class=\"image-div\">\r\n      <img [src]=\"'assets/imgs/Flags/' + imagenum + '.png'\">\r\n    </div>\r\n  </ion-toolbar> -->\r\n</ion-header>\r\n\r\n<ion-content [hidden]=\"loading\" #ionContent [forceOverscroll]=\"false\" [scrollY]=\"false\" class=\"fade-in-top\">\r\n\r\n  <!-- <img class=\"background-image\" src=\"./../../../assets/imgs/bg.png\"> -->\r\n\r\n  <div class=\"history\" [style]=\"lang == 'ar' ? 'direction : rtl;' : ''\" [hidden]=\"loading\">\r\n    <!-- <ion-slides #slides>\r\n      <ion-slide> -->\r\n    <app-content-area [hidden]=\"sort == 1 || sort == 2 || sort == 3 || sort == 4\" [image]=\"'assets/imgs/Flags/1.png'\"\r\n      [content]=\"first_slide_data[0]?.content\" [contentHeight]=\"contentHeight\" [slide]=\"1\">\r\n    </app-content-area>\r\n    <!-- </ion-slide>\r\n      <ion-slide> -->\r\n    <app-content-area [hidden]=\"sort == 0 || sort == 2 || sort == 3 || sort == 4\" [image]=\"'assets/imgs/Flags/2.png'\"\r\n      [content]=\"second_slide_data[0]?.content\" [contentHeight]=\"contentHeight\" [slide]=\"2\">\r\n    </app-content-area>\r\n    <!-- </ion-slide>\r\n      <ion-slide> -->\r\n    <app-content-area [hidden]=\"sort == 0 || sort == 1 || sort == 3 || sort == 4\" [showScroll]=\"false\"\r\n      [image]=\"'assets/imgs/Flags/3.png'\" [content]=\"second_slide_data[1]?.content\" [contentHeight]=\"contentHeight\">\r\n    </app-content-area>\r\n    <!-- </ion-slide>\r\n      <ion-slide> -->\r\n    <app-content-area [hidden]=\"sort == 0 || sort == 1 || sort == 2 || sort == 4\" [showScroll]=\"false\"\r\n      [image]=\"'assets/imgs/Flags/4.png'\" [content]=\"second_slide_data[2]?.content\" [contentHeight]=\"contentHeight\">\r\n    </app-content-area>\r\n    <!-- </ion-slide>\r\n      <ion-slide> -->\r\n    <app-content-area [hidden]=\"sort == 0 || sort == 1 || sort == 2 || sort == 3\" [showScroll]=\"false\"\r\n      [image]=\"'assets/imgs/Flags/5.png'\" [content]=\"third_slide_data[0]?.content\" [contentHeight]=\"contentHeight\">\r\n    </app-content-area>\r\n    <!-- </ion-slide>\r\n    </ion-slides> -->\r\n  </div>\r\n\r\n</ion-content>\r\n\r\n\r\n<div class=\"spinner\" [hidden]=\"!loading\">\r\n  <ion-spinner color=\"primary\"></ion-spinner>\r\n</div>\r\n\r\n<contact-us-fab></contact-us-fab>\r\n");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_flag-history_flag-history_module_ts.js.map