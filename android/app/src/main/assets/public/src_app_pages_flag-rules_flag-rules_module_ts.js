(self["webpackChunkSaudi_Flag_App"] = self["webpackChunkSaudi_Flag_App"] || []).push([["src_app_pages_flag-rules_flag-rules_module_ts"],{

/***/ 35496:
/*!***************************************************************!*\
  !*** ./src/app/pages/flag-rules/flag-rules-routing.module.ts ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FlagRulesPageRoutingModule": () => (/* binding */ FlagRulesPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 29535);
/* harmony import */ var _flag_rules_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./flag-rules.page */ 42587);




const routes = [
    {
        path: '',
        component: _flag_rules_page__WEBPACK_IMPORTED_MODULE_0__.FlagRulesPage
    }
];
let FlagRulesPageRoutingModule = class FlagRulesPageRoutingModule {
};
FlagRulesPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], FlagRulesPageRoutingModule);



/***/ }),

/***/ 10307:
/*!*******************************************************!*\
  !*** ./src/app/pages/flag-rules/flag-rules.module.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FlagRulesPageModule": () => (/* binding */ FlagRulesPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ 16274);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 93324);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 34595);
/* harmony import */ var _flag_rules_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./flag-rules-routing.module */ 35496);
/* harmony import */ var _flag_rules_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./flag-rules.page */ 42587);
/* harmony import */ var src_app_components_shared_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/components/shared.module */ 16369);








let FlagRulesPageModule = class FlagRulesPageModule {
};
FlagRulesPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_5__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.IonicModule,
            _flag_rules_routing_module__WEBPACK_IMPORTED_MODULE_0__.FlagRulesPageRoutingModule,
            src_app_components_shared_module__WEBPACK_IMPORTED_MODULE_2__.SharedModule
        ],
        declarations: [_flag_rules_page__WEBPACK_IMPORTED_MODULE_1__.FlagRulesPage]
    })
], FlagRulesPageModule);



/***/ }),

/***/ 42587:
/*!*****************************************************!*\
  !*** ./src/app/pages/flag-rules/flag-rules.page.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FlagRulesPage": () => (/* binding */ FlagRulesPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _raw_loader_flag_rules_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./flag-rules.page.html */ 35598);
/* harmony import */ var _flag_rules_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./flag-rules.page.scss */ 68212);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var src_app_components_flag_rule_des_flag_rule_des_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/components/flag-rule-des/flag-rule-des.component */ 95225);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../base-page/base-page */ 62168);






let FlagRulesPage = class FlagRulesPage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_3__.BasePage {
    constructor(injector) {
        super(injector);
        this.isLoading = true;
    }
    ngOnInit() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            yield this.initialize();
        });
    }
    initialize() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.title = this.nav.getQueryParams().title;
            this.icon = this.nav.getQueryParams().icon;
            const key = 'flag_rules';
            this.data = this.jsonService.getJsonData(key);
            this.isLoading = false;
        });
    }
    desc(item) {
        this.modals.present(src_app_components_flag_rule_des_flag_rule_des_component__WEBPACK_IMPORTED_MODULE_2__.FlagRuleDesComponent, { rule: item });
    }
};
FlagRulesPage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Injector }
];
FlagRulesPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-flag-rules',
        template: _raw_loader_flag_rules_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_flag_rules_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], FlagRulesPage);



/***/ }),

/***/ 68212:
/*!*******************************************************!*\
  !*** ./src/app/pages/flag-rules/flag-rules.page.scss ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".flag-rules {\n  height: 100%;\n}\n.flag-rules .rule-row .rule-box {\n  background-color: var(--ion-color-primary);\n  height: 17vh;\n  color: #fff;\n  text-align: center;\n}\n.flag-rules .rule-row .content {\n  box-shadow: 1px 4px 8px 0px #5d4f42;\n  padding: 10px;\n}\n.flag-rules .rule-row .content img {\n  height: 60px;\n}\n.flag-rules .rule-row .content p {\n  margin: 0px;\n  font-weight: bolder;\n  font-size: 1.1em;\n  line-height: 1.1em;\n}\n.flag-rules .bottom-icon {\n  display: flex;\n  align-content: center;\n  justify-content: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImZsYWctcnVsZXMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUlBO0VBQ0ksWUFBQTtBQUhKO0FBTVE7RUFDSSwwQ0FBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7QUFKWjtBQVFRO0VBRUUsbUNBQUE7RUFFQSxhQUFBO0FBUlY7QUFTVTtFQUNJLFlBQUE7QUFQZDtBQVVVO0VBQ0ksV0FBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBQVJkO0FBYUk7RUFDSSxhQUFBO0VBQ0EscUJBQUE7RUFDQSx1QkFBQTtBQVhSIiwiZmlsZSI6ImZsYWctcnVsZXMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gaW9uLWNvbnRlbnQge1xyXG4vLyAgICAgLy8gLS1iYWNrZ3JvdW5kOiB1cmwoXCIuLy4uLy4uLy4uL2Fzc2V0cy9pbWdzL2JnLnBuZ1wiKSAwIDAvMTAwJSAxMDAlIG5vLXJlcGVhdDtcclxuLy8gfVxyXG5cclxuLmZsYWctcnVsZXMge1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG5cclxuICAgIC5ydWxlLXJvdyB7XHJcbiAgICAgICAgLnJ1bGUtYm94IHtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDE3dmg7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIC8vIG1hcmdpbi10b3A6IDE1cHg7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAuY29udGVudCB7XHJcbiAgICAgICAgICAvLyBtYXJnaW46IDEwcHg7XHJcbiAgICAgICAgICBib3gtc2hhZG93OiAxcHggNHB4IDhweCAwcHggIzVkNGY0MjtcclxuICAgICAgICAvLyAgIDVweCA1cHggOHB4IDFweCByZ2JhKDAsIDAsIDAsIDAuNDApXHJcbiAgICAgICAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgICAgICAgaW1nIHtcclxuICAgICAgICAgICAgICBoZWlnaHQ6IDYwcHg7XHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgcCB7XHJcbiAgICAgICAgICAgICAgbWFyZ2luOiAwcHg7XHJcbiAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcclxuICAgICAgICAgICAgICBmb250LXNpemU6IDEuMWVtO1xyXG4gICAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiAxLjFlbTtcclxuICAgICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5ib3R0b20taWNvbiB7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICB9XHJcbn1cclxuIl19 */");

/***/ }),

/***/ 35598:
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/flag-rules/flag-rules.page.html ***!
  \*********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<app-header [title]=\"title\" format=\"png\" [icon]=\"icon\"></app-header>\r\n\r\n\r\n<ion-content *ngIf=\"!isLoading\">\r\n\r\n    <img class=\"background-image\" src=\"./../../../assets/imgs/bg.png\">\r\n    <div *ngIf=\"!isLoading\" class=\"flag-rules ion-padding\">\r\n        <ion-row class=\"rule-row ion-margin-vertical\">\r\n            <ion-col size=\"6\" *ngFor=\"let item of data\" (click)=\"desc(item)\" class=\"fade-in-top\">\r\n                <div class=\"content rule-box\">\r\n                    <img [src]=\"'assets/imgs/flag-rules/' + item.key + '.png' \">\r\n                    <p>{{item.value}}</p>\r\n                </div>\r\n            </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n            <ion-col size=\"5\"></ion-col>\r\n            <ion-col size=\"2\" style=\"display: flex;justify-content: center;\" (click)=\"utility.openPdfLink()\">\r\n                <ion-icon style=\"font-size: 35px;\" color=\"primary\" name=\"file-tray-stacked-outline\"></ion-icon>\r\n            </ion-col>\r\n            <ion-col size=\"5\"></ion-col>\r\n        </ion-row>\r\n    </div>\r\n\r\n</ion-content>\r\n\r\n<div class=\"spinner\" *ngIf=\"isLoading\">\r\n    <ion-spinner color=\"primary\"></ion-spinner>\r\n</div>\r\n\r\n<contact-us-fab></contact-us-fab>\r\n");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_flag-rules_flag-rules_module_ts.js.map