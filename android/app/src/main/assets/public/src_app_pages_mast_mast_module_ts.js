(self["webpackChunkSaudi_Flag_App"] = self["webpackChunkSaudi_Flag_App"] || []).push([["src_app_pages_mast_mast_module_ts"],{

/***/ 71276:
/*!***************************************************!*\
  !*** ./src/app/pages/mast/mast-routing.module.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MastPageRoutingModule": () => (/* binding */ MastPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 29535);
/* harmony import */ var _mast_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./mast.page */ 81636);




const routes = [
    {
        path: '',
        component: _mast_page__WEBPACK_IMPORTED_MODULE_0__.MastPage
    }
];
let MastPageRoutingModule = class MastPageRoutingModule {
};
MastPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], MastPageRoutingModule);



/***/ }),

/***/ 5245:
/*!*******************************************!*\
  !*** ./src/app/pages/mast/mast.module.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MastPageModule": () => (/* binding */ MastPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ 16274);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 93324);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 34595);
/* harmony import */ var _mast_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./mast-routing.module */ 71276);
/* harmony import */ var _mast_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./mast.page */ 81636);
/* harmony import */ var src_app_components_shared_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/components/shared.module */ 16369);








let MastPageModule = class MastPageModule {
};
MastPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_5__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.IonicModule,
            _mast_routing_module__WEBPACK_IMPORTED_MODULE_0__.MastPageRoutingModule,
            src_app_components_shared_module__WEBPACK_IMPORTED_MODULE_2__.SharedModule
        ],
        declarations: [_mast_page__WEBPACK_IMPORTED_MODULE_1__.MastPage]
    })
], MastPageModule);



/***/ }),

/***/ 81636:
/*!*****************************************!*\
  !*** ./src/app/pages/mast/mast.page.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MastPage": () => (/* binding */ MastPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _raw_loader_mast_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./mast.page.html */ 50420);
/* harmony import */ var _mast_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./mast.page.scss */ 24360);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../base-page/base-page */ 62168);





let MastPage = class MastPage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injector) {
        super(injector);
        this.num = 0;
        this.isLoading = true;
        this.isNetworkOngoing = false;
    }
    ngOnInit() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            yield this.initialize();
            this.sync();
        });
    }
    sync() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            if (!this.isNetworkOngoing) {
                yield this.sqlite.syncMastData();
                this.isNetworkOngoing = false;
            }
        });
    }
    initialize() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            this.language = yield this.storageService.get('lang');
            this.title = this.nav.getQueryParams().title;
            this.icon = this.nav.getQueryParams().icon;
            const lang = this.jsonService.getJsonData('mast');
            // console.log(this.data);
            this.data = yield this.sqlite.getMastData(this.language);
            console.log(lang);
            this.flag_area = lang.filter((x) => x.key == 'flag_area');
            this.flag_height = lang.filter((x) => x.key == 'flag_height');
            this.flag_total_area = lang.filter((x) => x.key == 'flag_total_area');
            this.flag_weight = lang.filter((x) => x.key == 'flag_weight');
            console.log(this.flag_weight);
            this.isLoading = false;
        });
    }
    returnBg(img) {
        return `url("./../../../assets/imgs/mast/${img}.jpg")`;
    }
    openLatLong(data) {
        console.log(data);
        this.utility.openMap(data);
    }
};
MastPage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.Injector }
];
MastPage = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Component)({
        selector: 'app-mast',
        template: _raw_loader_mast_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_mast_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], MastPage);



/***/ }),

/***/ 24360:
/*!*******************************************!*\
  !*** ./src/app/pages/mast/mast.page.scss ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".top-row {\n  background-color: var(--ion-color-primary);\n}\n.top-row .top-col {\n  text-align: center;\n  color: #fff;\n  font-weight: 600;\n}\n.white-bg {\n  background-color: #fff !important;\n  color: var(--ion-color-primary) !important;\n}\n.img-container {\n  background-position: center;\n  background-size: cover;\n  top: 40px;\n  height: 100%;\n}\n.container {\n  position: absolute;\n  width: 100%;\n  bottom: 0;\n}\n.mast-detail-header {\n  background-color: #5f3b10;\n  display: flex;\n  justify-content: center;\n}\n.mast-detail-header p {\n  display: flex;\n  justify-content: center;\n  width: 25%;\n  margin: 0;\n  font-weight: bolder;\n  color: #fff;\n  font-size: 14px;\n}\n.mast-details {\n  background-color: #c69c6b;\n  display: flex;\n  justify-content: center;\n}\n.mast-details p {\n  display: flex;\n  justify-content: center;\n  width: 25%;\n  margin: 0;\n  font-size: 14px;\n  font-weight: bolder;\n  color: #fff;\n  border-color: rgba(95, 59, 16, 0.84);\n  border-width: 1px;\n  border-style: solid;\n  padding: 7px 0px;\n  line-height: 20px;\n}\n.footer {\n  padding-top: 10px;\n  padding-bottom: 10px;\n  background: var(--ion-color-primary);\n}\n.footer ion-item {\n  --background: var(--ion-color-primary);\n}\n.footer ion-item ion-icon {\n  color: #ffffffab;\n  font-size: 35px;\n}\n.footer ion-item ion-text {\n  color: #fff;\n  margin-left: 15px;\n  font-weight: bolder;\n}\n.footer .footer-text {\n  font-size: 0.9em;\n  line-height: 1.4em;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1hc3QucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUlBO0VBQ0ksMENBQUE7QUFISjtBQUtJO0VBQ0ksa0JBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7QUFIUjtBQU9BO0VBQ0ksaUNBQUE7RUFDQSwwQ0FBQTtBQUpKO0FBT0E7RUFDSSwyQkFBQTtFQUNBLHNCQUFBO0VBQ0EsU0FBQTtFQUNBLFlBQUE7QUFKSjtBQVlBO0VBQ0ksa0JBQUE7RUFDQSxXQUFBO0VBQ0EsU0FBQTtBQVRKO0FBWUE7RUFDSSx5QkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtBQVRKO0FBV0k7RUFDSSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxVQUFBO0VBQ0EsU0FBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7QUFUUjtBQWFBO0VBQ0kseUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7QUFWSjtBQVdJO0VBQ0ksYUFBQTtFQUNBLHVCQUFBO0VBQ0EsVUFBQTtFQUNBLFNBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0Esb0NBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtBQVRSO0FBYUE7RUFDSSxpQkFBQTtFQUNBLG9CQUFBO0VBQ0Esb0NBQUE7QUFWSjtBQVdJO0VBQ0ksc0NBQUE7QUFUUjtBQVdRO0VBQ0ksZ0JBQUE7RUFDQSxlQUFBO0FBVFo7QUFZUTtFQUNJLFdBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0FBVlo7QUFjSTtFQUNJLGdCQUFBO0VBQ0Esa0JBQUE7QUFaUiIsImZpbGUiOiJtYXN0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIGlvbi1jb250ZW50IHtcclxuLy8gICAgIC8vIC0tYmFja2dyb3VuZDogdXJsKFwiLi8uLi8uLi8uLi9hc3NldHMvaW1ncy9iZy5wbmdcIikgMCAwLzEwMCUgMTAwJSBuby1yZXBlYXQ7XHJcbi8vIH1cclxuXHJcbi50b3Atcm93IHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxuXHJcbiAgICAudG9wLWNvbCB7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgICB9XHJcbn1cclxuXHJcbi53aGl0ZS1iZyB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmICFpbXBvcnRhbnQ7XHJcbiAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5pbWctY29udGFpbmVyIHtcclxuICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgICB0b3A6IDQwcHg7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAvLyBpbWcge1xyXG4gICAgLy8gICAgIGhlaWdodDogMTAwJTtcclxuICAgIC8vICAgICB3aWR0aDogMTAwJTtcclxuICAgIC8vICAgICBvYmplY3QtZml0OiBjb3ZlcjtcclxuICAgIC8vIH1cclxufVxyXG5cclxuLmNvbnRhaW5lciB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGJvdHRvbTogMDtcclxufVxyXG5cclxuLm1hc3QtZGV0YWlsLWhlYWRlciB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoOTUgNTkgMTYpO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5cclxuICAgIHAge1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgd2lkdGg6IDI1JTtcclxuICAgICAgICBtYXJnaW46IDA7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcclxuICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5tYXN0LWRldGFpbHMge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDE5OCAxNTYgMTA3KTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIHAge1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgd2lkdGg6IDI1JTtcclxuICAgICAgICBtYXJnaW46IDA7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XHJcbiAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgYm9yZGVyLWNvbG9yOiByZ2JhKDk1LCA1OSwgMTYsIDAuODQpO1xyXG4gICAgICAgIGJvcmRlci13aWR0aDogMXB4O1xyXG4gICAgICAgIGJvcmRlci1zdHlsZTogc29saWQ7XHJcbiAgICAgICAgcGFkZGluZzogN3B4IDBweDtcclxuICAgICAgICBsaW5lLWhlaWdodDogMjBweDtcclxuICAgIH1cclxufVxyXG5cclxuLmZvb3RlciB7XHJcbiAgICBwYWRkaW5nLXRvcDogMTBweDtcclxuICAgIHBhZGRpbmctYm90dG9tOiAxMHB4O1xyXG4gICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG4gICAgaW9uLWl0ZW0ge1xyXG4gICAgICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG5cclxuICAgICAgICBpb24taWNvbiB7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjZmZmZmZmYWI7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMzVweDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlvbi10ZXh0IHtcclxuICAgICAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxNXB4O1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogYm9sZGVyO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAuZm9vdGVyLXRleHQge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMC45ZW07XHJcbiAgICAgICAgbGluZS1oZWlnaHQ6IDEuNGVtO1xyXG4gICAgfVxyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ 50420:
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/mast/mast.page.html ***!
  \*********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<app-header [title]=\"title\" format=\"png\" [icon]=\"icon\"></app-header>\r\n\r\n\r\n<ion-content [scrollY]=\"false\" *ngIf=\"!isLoading\" class=\"fade-in-top\">\r\n\r\n    <img class=\"background-image\" src=\"./../../../assets/imgs/bg.png\">\r\n\r\n    <ion-row class=\"top-row\">\r\n        <ion-col class=\"top-col\" (click)=\"num = 0\" [ngClass]=\"num == 1 ? 'white-bg' : ''\" size=\"6\">\r\n            {{data[0].title}}\r\n        </ion-col>\r\n        <ion-col class=\"top-col\" (click)=\"num = 1\" [ngClass]=\"num == 0 ? 'white-bg' : ''\" size=\"6\">\r\n            {{data[1].title}}\r\n        </ion-col>\r\n    </ion-row>\r\n\r\n    <div class=\"img-container\" [ngStyle]=\"{'background-image': returnBg(data[num].image)}\">\r\n        <!-- <img [src]=\"'../../../assets/imgs/mast/' +  + '.jpg' \" alt=\"\"> -->\r\n    </div>\r\n\r\n    <div class=\"container\">\r\n        <div class=\"mast-detail-header\">\r\n            <p>\r\n                {{flag_height[0].value}}\r\n            </p>\r\n            <p>\r\n                {{flag_area[0].value}}\r\n            </p>\r\n            <p>\r\n                {{flag_weight[0].value}}\r\n            </p>\r\n            <p>\r\n                {{flag_total_area[0].value}}\r\n            </p>\r\n        </div>\r\n        <div class=\"mast-details\">\r\n            <p>\r\n                {{data[num].flag_height}}\r\n            </p>\r\n            <p>\r\n                {{data[num].flag_area}}\r\n            </p>\r\n            <p>\r\n                {{data[num].flag_weight}}\r\n            </p>\r\n            <p>\r\n                {{data[num].flag_total_area}}\r\n            </p>\r\n        </div>\r\n    </div>\r\n\r\n\r\n</ion-content>\r\n\r\n<div class=\"spinner\" *ngIf=\"isLoading\">\r\n    <ion-spinner color=\"primary\"></ion-spinner>\r\n</div>\r\n\r\n\r\n<ion-footer *ngIf=\"!isLoading\" class=\"footer fade-in\">\r\n    <ion-item (click)=\"openLatLong(data[num])\" lines=\"none\">\r\n        <ion-icon name=\"location\"></ion-icon>\r\n        <ion-text class=\"footer-text\">\r\n            {{data[num].location}}\r\n        </ion-text>\r\n        <ion-button style=\"margin-left: 0;\" slot=\"end\" fill=\"clear\" (click)=\"openLatLong(data[num])\">\r\n            <ion-icon name=\"ellipsis-horizontal\"></ion-icon>\r\n        </ion-button>\r\n    </ion-item>\r\n</ion-footer>");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_mast_mast_module_ts.js.map