(self["webpackChunkSaudi_Flag_App"] = self["webpackChunkSaudi_Flag_App"] || []).push([["src_app_pages_national-anthem_national-anthem_module_ts"],{

/***/ 60810:
/*!*************************************************************************!*\
  !*** ./src/app/pages/national-anthem/national-anthem-routing.module.ts ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NationalAnthemPageRoutingModule": () => (/* binding */ NationalAnthemPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 29535);
/* harmony import */ var _national_anthem_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./national-anthem.page */ 17701);




const routes = [
    {
        path: '',
        component: _national_anthem_page__WEBPACK_IMPORTED_MODULE_0__.NationalAnthemPage
    }
];
let NationalAnthemPageRoutingModule = class NationalAnthemPageRoutingModule {
};
NationalAnthemPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], NationalAnthemPageRoutingModule);



/***/ }),

/***/ 69129:
/*!*****************************************************************!*\
  !*** ./src/app/pages/national-anthem/national-anthem.module.ts ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NationalAnthemPageModule": () => (/* binding */ NationalAnthemPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ 16274);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 93324);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 34595);
/* harmony import */ var _national_anthem_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./national-anthem-routing.module */ 60810);
/* harmony import */ var _national_anthem_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./national-anthem.page */ 17701);
/* harmony import */ var src_app_components_shared_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/components/shared.module */ 16369);








let NationalAnthemPageModule = class NationalAnthemPageModule {
};
NationalAnthemPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_5__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.IonicModule,
            _national_anthem_routing_module__WEBPACK_IMPORTED_MODULE_0__.NationalAnthemPageRoutingModule,
            src_app_components_shared_module__WEBPACK_IMPORTED_MODULE_2__.SharedModule
        ],
        declarations: [_national_anthem_page__WEBPACK_IMPORTED_MODULE_1__.NationalAnthemPage]
    })
], NationalAnthemPageModule);



/***/ }),

/***/ 17701:
/*!***************************************************************!*\
  !*** ./src/app/pages/national-anthem/national-anthem.page.ts ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NationalAnthemPage": () => (/* binding */ NationalAnthemPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _raw_loader_national_anthem_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./national-anthem.page.html */ 10666);
/* harmony import */ var _national_anthem_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./national-anthem.page.scss */ 38443);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../base-page/base-page */ 62168);





let NationalAnthemPage = class NationalAnthemPage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injector) {
        super(injector);
        this.isPlaying = false;
        this.lang = '';
        this.timelaspse = -1;
        this.timer_ids = [];
        this.interval_times_en = [
            0,
            10000,
            14000,
            18000,
            23000,
            29000
        ];
        this.interval_times_ar = [
            0,
            10000,
            15000,
            17000,
            23000,
            29000
        ];
        this.audioService.preload();
        this.storageService.get('lang').then((x) => {
            this.lang = x;
        });
    }
    ngOnInit() {
        this.title = this.nav.getQueryParams().title;
        this.icon = this.nav.getQueryParams().icon;
        console.log(this.title);
    }
    ngOnDestroy() {
        if (this.isPlaying) {
            this.playPause();
        }
    }
    playPause() {
        if (!this.isPlaying) {
            this.audioService.play();
            let array = [];
            if (this.lang == 'en') {
                array = this.interval_times_en;
            }
            else if (this.lang == 'ar') {
                array = this.interval_times_ar;
            }
            array.forEach(element => {
                const id = setTimeout(() => {
                    this.timelaspse++;
                    if (this.timelaspse >= 5) {
                        this.clearTimeout();
                        this.playPause();
                        return;
                    }
                }, element);
                this.timer_ids.push(id);
            });
        }
        else if (this.isPlaying) {
            this.audioService.pause();
            this.clearTimeout();
            this.timelaspse = -1;
        }
        console.log("here");
        this.isPlaying = !this.isPlaying;
    }
    clearTimeout() {
        this.timer_ids.forEach(element => {
            if (element) {
                clearTimeout(element);
            }
        });
    }
};
NationalAnthemPage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Injector }
];
NationalAnthemPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-national-anthem',
        template: _raw_loader_national_anthem_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_national_anthem_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], NationalAnthemPage);



/***/ }),

/***/ 38443:
/*!*****************************************************************!*\
  !*** ./src/app/pages/national-anthem/national-anthem.page.scss ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".top-flag {\n  height: 140px;\n  margin-top: 40px;\n  text-align: center;\n}\n.top-flag img {\n  height: 100%;\n}\n.play-pause {\n  height: 60px;\n  text-align: center;\n  margin: 15px;\n  margin-top: 35px;\n  margin-bottom: 0;\n}\n.play-pause img {\n  height: 100%;\n}\n.play-box {\n  display: flex;\n  height: 100px;\n  align-items: center;\n  justify-content: center;\n}\n.play-box .pause-line {\n  width: 100%;\n}\n.play-box .pause-line div {\n  overflow: hidden;\n  border-top: 1px solid #fff;\n}\n.play-box .play-gif {\n  height: 100%;\n  width: 100%;\n}\n.play-box .play-gif img {\n  width: 100%;\n  height: 100%;\n}\n.sub-box {\n  text-align: center;\n}\n.sub-box p {\n  font-weight: bolder;\n  color: #fff;\n  font-size: 20px;\n  margin: 0;\n}\n.line-height-ar {\n  line-height: 33px;\n}\n.line-height-en {\n  line-height: 25px;\n}\n.between-space {\n  width: 25px;\n  display: inline-block;\n}\n.show-animated {\n  animation: fade-in 1s ease-in both;\n}\n/* ----------------------------------------------\n * Generated by Animista on 2021-8-20 17:5:46\n * Licensed under FreeBSD License.\n * See http://animista.net/license for more info.\n * w: http://animista.net, t: @cssanimista\n * ---------------------------------------------- */\n/**\n * ----------------------------------------\n * animation fade-in\n * ----------------------------------------\n */\n@keyframes fade-in {\n  0% {\n    opacity: 0;\n  }\n  100% {\n    opacity: 1;\n  }\n}\n.hide-animated {\n  opacity: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5hdGlvbmFsLWFudGhlbS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBSUE7RUFDSSxhQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBQUhKO0FBS0k7RUFDSSxZQUFBO0FBSFI7QUFPQTtFQUNJLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0FBSko7QUFNSTtFQUNJLFlBQUE7QUFKUjtBQVFBO0VBQ0ksYUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0FBTEo7QUFPSTtFQUNJLFdBQUE7QUFMUjtBQU9RO0VBQ0ksZ0JBQUE7RUFDQSwwQkFBQTtBQUxaO0FBU0k7RUFDSSxZQUFBO0VBQ0EsV0FBQTtBQVBSO0FBU1E7RUFDSSxXQUFBO0VBQ0EsWUFBQTtBQVBaO0FBWUE7RUFDSSxrQkFBQTtBQVRKO0FBV0k7RUFDSSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsU0FBQTtBQVRSO0FBYUE7RUFDSSxpQkFBQTtBQVZKO0FBY0E7RUFDSSxpQkFBQTtBQVhKO0FBY0E7RUFDSSxXQUFBO0VBQ0EscUJBQUE7QUFYSjtBQWNBO0VBRUksa0NBQUE7QUFYSjtBQWNBOzs7OzttREFBQTtBQU9BOzs7O0VBQUE7QUFhQTtFQUNJO0lBQ0ksVUFBQTtFQVpOO0VBY0U7SUFDSSxVQUFBO0VBWk47QUFDRjtBQWVBO0VBQ0ksVUFBQTtBQWJKIiwiZmlsZSI6Im5hdGlvbmFsLWFudGhlbS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XHJcbiAgICAvLyAtLWJhY2tncm91bmQ6IHVybChcIi4vLi4vLi4vLi4vYXNzZXRzL2ltZ3MvbmF0aW9uYWwtYW50aGVtL05hdGlvbmFsQW50aGVtLnBuZ1wiKSAwIDAvMTAwJSAxMDAlIG5vLXJlcGVhdDtcclxufVxyXG5cclxuLnRvcC1mbGFnIHtcclxuICAgIGhlaWdodDogMTQwcHg7XHJcbiAgICBtYXJnaW4tdG9wOiA0MHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG5cclxuICAgIGltZyB7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgfVxyXG59XHJcblxyXG4ucGxheS1wYXVzZSB7XHJcbiAgICBoZWlnaHQ6IDYwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBtYXJnaW46IDE1cHg7XHJcbiAgICBtYXJnaW4tdG9wOiAzNXB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMDtcclxuXHJcbiAgICBpbWcge1xyXG4gICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgIH1cclxufVxyXG5cclxuLnBsYXktYm94IHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBoZWlnaHQ6IDEwMHB4O1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5cclxuICAgIC5wYXVzZS1saW5lIHtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuXHJcbiAgICAgICAgZGl2IHtcclxuICAgICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICAgICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNmZmY7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5wbGF5LWdpZiB7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG5cclxuICAgICAgICBpbWcge1xyXG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuLnN1Yi1ib3gge1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG5cclxuICAgIHAge1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XHJcbiAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgICAgIG1hcmdpbjogMDtcclxuICAgIH1cclxufVxyXG5cclxuLmxpbmUtaGVpZ2h0LWFyIHtcclxuICAgIGxpbmUtaGVpZ2h0OiAzM3B4O1xyXG59XHJcblxyXG5cclxuLmxpbmUtaGVpZ2h0LWVuIHtcclxuICAgIGxpbmUtaGVpZ2h0OiAyNXB4O1xyXG59XHJcblxyXG4uYmV0d2Vlbi1zcGFjZSB7XHJcbiAgICB3aWR0aDogMjVweDtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxufVxyXG5cclxuLnNob3ctYW5pbWF0ZWQge1xyXG4gICAgLXdlYmtpdC1hbmltYXRpb246IGZhZGUtaW4gMXMgZWFzZS1pbiBib3RoO1xyXG4gICAgYW5pbWF0aW9uOiBmYWRlLWluIDFzIGVhc2UtaW4gYm90aDtcclxufVxyXG5cclxuLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gKiBHZW5lcmF0ZWQgYnkgQW5pbWlzdGEgb24gMjAyMS04LTIwIDE3OjU6NDZcclxuICogTGljZW5zZWQgdW5kZXIgRnJlZUJTRCBMaWNlbnNlLlxyXG4gKiBTZWUgaHR0cDovL2FuaW1pc3RhLm5ldC9saWNlbnNlIGZvciBtb3JlIGluZm8uXHJcbiAqIHc6IGh0dHA6Ly9hbmltaXN0YS5uZXQsIHQ6IEBjc3NhbmltaXN0YVxyXG4gKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tICovXHJcblxyXG4vKipcclxuICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gKiBhbmltYXRpb24gZmFkZS1pblxyXG4gKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAqL1xyXG5ALXdlYmtpdC1rZXlmcmFtZXMgZmFkZS1pbiB7XHJcbiAgICAwJSB7XHJcbiAgICAgICAgb3BhY2l0eTogMDtcclxuICAgIH1cclxuICAgIDEwMCUge1xyXG4gICAgICAgIG9wYWNpdHk6IDE7XHJcbiAgICB9XHJcbn1cclxuQGtleWZyYW1lcyBmYWRlLWluIHtcclxuICAgIDAlIHtcclxuICAgICAgICBvcGFjaXR5OiAwO1xyXG4gICAgfVxyXG4gICAgMTAwJSB7XHJcbiAgICAgICAgb3BhY2l0eTogMTtcclxuICAgIH1cclxufVxyXG5cclxuLmhpZGUtYW5pbWF0ZWQge1xyXG4gICAgb3BhY2l0eTogMDtcclxufVxyXG5cclxuXHJcbiJdfQ== */");

/***/ }),

/***/ 10666:
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/national-anthem/national-anthem.page.html ***!
  \*******************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<app-header [title]=\"title\" format=\"png\" [icon]=\"icon\"></app-header>\r\n\r\n<ion-content>\r\n\r\n    <img class=\"background-image\" src=\"./../../../assets/imgs/national-anthem/NationalAnthem.png\">\r\n\r\n    <div class=\"top-flag\">\r\n        <img src=\"../../../assets/imgs/national-anthem/flag-Trans.png\">\r\n    </div>\r\n\r\n    <div class=\"play-pause\">\r\n        <img (click)=\"playPause()\" *ngIf=\"isPlaying\" src=\"../../../assets/imgs/national-anthem/pause.png\">\r\n        <img (click)=\"playPause()\" *ngIf=\"!isPlaying\" src=\"../../../assets/imgs/national-anthem/playBig.png\">\r\n    </div>\r\n\r\n    <div class=\"play-box\">\r\n        <div *ngIf=\"!isPlaying\" class=\"pause-line\">\r\n            <div></div>\r\n        </div>\r\n        <div *ngIf=\"isPlaying\" class=\"play-gif\">\r\n            <img src=\"../../../assets/imgs/national-anthem/animationsound.gif\">\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"sub-box\">\r\n        <p class=\"line-height-en\" *ngIf=\"lang == 'en'\">\r\n            <span [class.hide-animated]=\"timelaspse < 0\" [class.show-animated]=\"timelaspse > -1\">Hasten for glory and\r\n                supremacy <br>\r\n                Glorify the creator of the heavens</span>\r\n            <br>\r\n            <span [class.hide-animated]=\"timelaspse < 1\" [class.show-animated]=\"timelaspse > 0\">and raise the green\r\n                flag<br>\r\n                carrying the written light</span> <br>\r\n            <span [class.hide-animated]=\"timelaspse < 2\" [class.show-animated]=\"timelaspse > 1\"> Chant \"God is the\r\n                greatest\" O my country</span>\r\n            <br>\r\n            <span [class.hide-animated]=\"timelaspse < 3\" [class.show-animated]=\"timelaspse > 2\">my country, Long live\r\n                the pride of Muslims</span> <br>\r\n            <span [class.hide-animated]=\"timelaspse < 4\" [class.show-animated]=\"timelaspse > 3\">Long live the king, for\r\n                the flag and the country.</span> <br>\r\n        </p>\r\n        <p class=\"line-height-ar\" *ngIf=\"lang == 'ar'\">\r\n            <span [class.hide-animated]=\"timelaspse < 0\" [class.show-animated]=\"timelaspse > -1\">\r\n                سَارِعِي لِلْمَجْدِ وَالْعَلْيَاء <span class=\"between-space\"></span> مَجِّدِي لِخَالِقِ السَّمَاء\r\n            </span><br>\r\n            <span [class.hide-animated]=\"timelaspse < 1\" [class.show-animated]=\"timelaspse > 0\">\r\n                وَارْفَعِ الخَفَّاقَ أَخْضَرْ <span class=\"between-space\"></span> يَحْمِلُ النُّورَ الْمُسَطَّرْ\r\n            </span><br>\r\n            <span [class.hide-animated]=\"timelaspse < 2\" [class.show-animated]=\"timelaspse > 1\">\r\n                رَدّدِي اللهُ أكْبَر <span class=\"between-space\"></span><span class=\"between-space\"></span> يَا\r\n                مَوْطِنِي\r\n            </span><br>\r\n            <span [class.hide-animated]=\"timelaspse < 3\" [class.show-animated]=\"timelaspse > 2\">\r\n                مَوْطِنِي عِشْتَ فَخْرَ الْمُسلِمِين\r\n            </span><br>\r\n            <span [class.hide-animated]=\"timelaspse < 4\" [class.show-animated]=\"timelaspse > 3\">\r\n                عَاشَ الْمَلِكْ: لِلْعَلَمْ وَالْوَطَنْ\r\n            </span><br>\r\n        </p>\r\n    </div>\r\n</ion-content>\r\n\r\n<contact-us-fab></contact-us-fab>\r\n");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_national-anthem_national-anthem_module_ts.js.map