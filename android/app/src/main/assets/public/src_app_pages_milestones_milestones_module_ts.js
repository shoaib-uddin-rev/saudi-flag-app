(self["webpackChunkSaudi_Flag_App"] = self["webpackChunkSaudi_Flag_App"] || []).push([["src_app_pages_milestones_milestones_module_ts"],{

/***/ 15243:
/*!***************************************************************!*\
  !*** ./src/app/pages/milestones/milestones-routing.module.ts ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MilestonesPageRoutingModule": () => (/* binding */ MilestonesPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 29535);
/* harmony import */ var _milestones_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./milestones.page */ 98296);




const routes = [
    {
        path: '',
        component: _milestones_page__WEBPACK_IMPORTED_MODULE_0__.MilestonesPage
    }
];
let MilestonesPageRoutingModule = class MilestonesPageRoutingModule {
};
MilestonesPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], MilestonesPageRoutingModule);



/***/ }),

/***/ 99836:
/*!*******************************************************!*\
  !*** ./src/app/pages/milestones/milestones.module.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MilestonesPageModule": () => (/* binding */ MilestonesPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ 16274);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 93324);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 34595);
/* harmony import */ var _milestones_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./milestones-routing.module */ 15243);
/* harmony import */ var _milestones_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./milestones.page */ 98296);
/* harmony import */ var src_app_components_shared_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/components/shared.module */ 16369);








let MilestonesPageModule = class MilestonesPageModule {
};
MilestonesPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_5__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.IonicModule,
            _milestones_routing_module__WEBPACK_IMPORTED_MODULE_0__.MilestonesPageRoutingModule,
            src_app_components_shared_module__WEBPACK_IMPORTED_MODULE_2__.SharedModule
        ],
        declarations: [_milestones_page__WEBPACK_IMPORTED_MODULE_1__.MilestonesPage]
    })
], MilestonesPageModule);



/***/ }),

/***/ 98296:
/*!*****************************************************!*\
  !*** ./src/app/pages/milestones/milestones.page.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MilestonesPage": () => (/* binding */ MilestonesPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _raw_loader_milestones_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./milestones.page.html */ 22808);
/* harmony import */ var _milestones_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./milestones.page.scss */ 89181);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var src_app_components_milestone_detail_milestone_detail_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/components/milestone-detail/milestone-detail.component */ 52919);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../base-page/base-page */ 62168);






let MilestonesPage = class MilestonesPage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_3__.BasePage {
    constructor(injector, renderer2) {
        super(injector);
        this.renderer2 = renderer2;
        this.data = [];
        this.percent = 0;
        this.number = 0;
        this.slide_ended = false;
        this.isLoading = true;
        this.top = 0;
        this.lang = 'en';
        this.isNetworkOngoing = false;
        this.slideOpts = {
            on: {
                beforeInit() {
                    const swiper = this;
                    swiper.classNames.push(`${swiper.params.containerModifierClass}flip`);
                    swiper.classNames.push(`${swiper.params.containerModifierClass}3d`);
                    const overwriteParams = {
                        slidesPerView: 1,
                        slidesPerColumn: 1,
                        slidesPerGroup: 1,
                        watchSlidesProgress: true,
                        spaceBetween: 0,
                        virtualTranslate: true,
                    };
                    swiper.params = Object.assign(swiper.params, overwriteParams);
                    swiper.originalParams = Object.assign(swiper.originalParams, overwriteParams);
                },
                setTranslate() {
                    const swiper = this;
                    const { $, slides, rtlTranslate: rtl } = swiper;
                    for (let i = 0; i < slides.length; i += 1) {
                        const $slideEl = slides.eq(i);
                        let progress = $slideEl[0].progress;
                        if (swiper.params.flipEffect.limitRotation) {
                            progress = Math.max(Math.min($slideEl[0].progress, 1), -1);
                        }
                        const offset$$1 = $slideEl[0].swiperSlideOffset;
                        const rotate = -180 * progress;
                        let rotateY = rotate;
                        let rotateX = 0;
                        let tx = -offset$$1;
                        let ty = 0;
                        if (!swiper.isHorizontal()) {
                            ty = tx;
                            tx = 0;
                            rotateX = -rotateY;
                            rotateY = 0;
                        }
                        else if (rtl) {
                            rotateY = -rotateY;
                        }
                        $slideEl[0].style.zIndex =
                            -Math.abs(Math.round(progress)) + slides.length;
                        if (swiper.params.flipEffect.slideShadows) {
                            // Set shadows
                            let shadowBefore = swiper.isHorizontal()
                                ? $slideEl.find('.swiper-slide-shadow-left')
                                : $slideEl.find('.swiper-slide-shadow-top');
                            let shadowAfter = swiper.isHorizontal()
                                ? $slideEl.find('.swiper-slide-shadow-right')
                                : $slideEl.find('.swiper-slide-shadow-bottom');
                            if (shadowBefore.length === 0) {
                                shadowBefore = swiper.$(`<div class="swiper-slide-shadow-${swiper.isHorizontal() ? 'left' : 'top'}"></div>`);
                                $slideEl.append(shadowBefore);
                            }
                            if (shadowAfter.length === 0) {
                                shadowAfter = swiper.$(`<div class="swiper-slide-shadow-${swiper.isHorizontal() ? 'right' : 'bottom'}"></div>`);
                                $slideEl.append(shadowAfter);
                            }
                            if (shadowBefore.length)
                                shadowBefore[0].style.opacity = Math.max(-progress, 0);
                            if (shadowAfter.length)
                                shadowAfter[0].style.opacity = Math.max(progress, 0);
                        }
                        $slideEl.transform(`translate3d(${tx}px, ${ty}px, 0px) rotateX(${rotateX}deg) rotateY(${rotateY}deg)`);
                    }
                },
                setTransition(duration) {
                    const swiper = this;
                    const { slides, activeIndex, $wrapperEl } = swiper;
                    slides
                        .transition(duration)
                        .find('.swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left')
                        .transition(duration);
                    if (swiper.params.virtualTranslate && duration !== 0) {
                        let eventTriggered = false;
                        // eslint-disable-next-line
                        slides.eq(activeIndex).transitionEnd(function onTransitionEnd() {
                            if (eventTriggered)
                                return;
                            if (!swiper || swiper.destroyed)
                                return;
                            eventTriggered = true;
                            swiper.animating = false;
                            const triggerEvents = ['webkitTransitionEnd', 'transitionend'];
                            for (let i = 0; i < triggerEvents.length; i += 1) {
                                $wrapperEl.trigger(triggerEvents[i]);
                            }
                        });
                    }
                },
            },
        };
        this.pdfs = [];
        this.lang = localStorage.getItem('lang');
    }
    ngOnInit() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            yield this.initialize();
            this.sync();
        });
    }
    sync() {
        if (!this.isNetworkOngoing) {
            this.sqlite.syncMilestonesData().then(() => {
                this.isNetworkOngoing = !this.isNetworkOngoing;
                console.log(this.isNetworkOngoing);
            });
        }
    }
    initialize() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.title = this.nav.getQueryParams().title;
            this.icon = this.nav.getQueryParams().icon;
            const lang = yield this.storageService.get('lang');
            // this.data = this.jsonService.getJsonData('milestones');
            this.data = yield this.sqlite.getMileStonesData(lang);
            console.log('from sqlite', this.data);
            if (this.lang == 'en') {
                for (var i = 0; i < 10; i++) {
                    this.pdfs.push(i + 1);
                }
            }
            if (this.lang == 'ar') {
            }
            this.isLoading = false;
        });
    }
    // logEnd = false;
    logScrolling($event) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            const el = yield this.content.getScrollElement();
            let sct;
            // if (total_height > el.scrollHeight - el.offsetHeight) {
            //   sct = (el.scrollTop * (scrollHeight / (height - scrollHeight))) - 152;
            // } else {
            console.log(el.scrollTop, el.scrollHeight - el.offsetHeight, el.offsetHeight, el.scrollHeight);
            if (el.scrollTop + 152 < el.scrollHeight - el.offsetHeight) {
                sct =
                    el.scrollTop * (el.offsetHeight / (el.scrollHeight - el.offsetHeight));
            }
            // }
            this.top = sct;
            let rtop = this.top.toString() + 'px';
            this.renderer2.setStyle(this.scroll.nativeElement, 'margin-top', rtop);
        });
    }
    details(item) {
        // console.log(item);
        this.modals.present(src_app_components_milestone_detail_milestone_detail_component__WEBPACK_IMPORTED_MODULE_2__.MilestoneDetailComponent, { item: item });
    }
    onSlideChange() {
        this.number++;
        console.log(this.slide_ended);
        this.percent = (this.number / this.pdfs.length) * 100;
    }
    logEndFunc($event) {
        // this.top = 0;
        // this.logScrolling($event);
        // $event.target.complete();
        // this.logEnd = true;
    }
};
MilestonesPage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Injector },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Renderer2 }
];
MilestonesPage.propDecorators = {
    content: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ViewChild, args: ['content',] }],
    scroll: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ViewChild, args: ['scroll', { read: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ElementRef },] }],
    scrolldiv: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ViewChild, args: ['scrolldiv', { read: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ElementRef },] }]
};
MilestonesPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-milestones',
        template: _raw_loader_milestones_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_milestones_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], MilestonesPage);



/***/ }),

/***/ 89181:
/*!*******************************************************!*\
  !*** ./src/app/pages/milestones/milestones.page.scss ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("*,\n*::before,\n*::after {\n  box-sizing: border-box;\n}\n\nion-content {\n  direction: ltr !important;\n  --background: url('bg.png') 0 0/100% 100% no-repeat;\n}\n\n.timeline {\n  position: relative;\n  width: 100%;\n  margin: 0 auto;\n  padding: 15px 0;\n  --padding-bottom: 70px;\n}\n\n.scroll-div {\n  display: flex;\n  width: 15px;\n  background: #e6e6e6;\n  position: fixed;\n  left: 49%;\n  height: calc(100vh - 80px);\n  margin-left: -1px;\n  justify-content: center;\n}\n\n.scroll-div .scroll {\n  background: #6d5846;\n  width: 90%;\n  border: #fff 3px solid;\n  height: 74px;\n  border-radius: 33%;\n  position: relative;\n  z-index: 1;\n  overflow-y: scroll;\n}\n\n.container {\n  position: relative;\n  background: inherit;\n  width: 50%;\n}\n\n.container.left {\n  left: 4%;\n}\n\n.container.right {\n  left: 52%;\n}\n\n.container.left .content {\n  box-shadow: -5px 7px 6px 0px rgba(0, 0, 0, 0.25);\n}\n\n.container.right .content {\n  box-shadow: 9px 5px 7px 0px rgba(0, 0, 0, 0.25);\n}\n\n.container.right::after {\n  left: -8px;\n}\n\n.container.right::before {\n  left: 8px;\n}\n\n.container .date {\n  position: absolute;\n  display: inline-block;\n  top: calc(50% - 8px);\n  text-align: center;\n  font-size: 14px;\n  font-weight: bold;\n  color: #006e51;\n  text-transform: uppercase;\n  letter-spacing: 1px;\n  z-index: 1;\n}\n\n.container .content {\n  background: var(--ion-color-primary);\n  width: 90%;\n  text-align: center;\n  display: flex;\n  height: 100px;\n  position: relative;\n  margin-top: 30px;\n  border-radius: 10px 0 0 10px;\n  flex-direction: column;\n  padding: 0 5px 0 5px;\n}\n\n.container.right .content {\n  border-radius: 0 10px 10px 0;\n}\n\n.container .content h2 {\n  font-size: 13px;\n  font-weight: bolder;\n  color: white;\n  margin-bottom: 0;\n}\n\n.container .content p {\n  margin-bottom: 18px;\n  font-size: 14px;\n  line-height: 22px;\n  color: #fff;\n}\n\n::-webkit-scrollbar {\n  display: none;\n}\n\n.pdf-pages {\n  --bullet-background: #000000;\n  --bullet-background-active: var(--ion-color-primary);\n}\n\n.pdf-pages .swiper-pagination {\n  bottom: -80px !important;\n}\n\n.skill-per {\n  height: 14px;\n  background: var(--ion-color-primary);\n  border-radius: 3px;\n  position: relative;\n  animation: fillBars 2.5s 1;\n}\n\n@keyframes fillBars {\n  from {\n    width: 0;\n  }\n  to {\n    width: 100%;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1pbGVzdG9uZXMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7RUFHRSxzQkFBQTtBQUNGOztBQUVBO0VBQ0UseUJBQUE7RUFDQSxtREFBQTtBQUNGOztBQUVBO0VBQ0Usa0JBQUE7RUFDQSxXQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSxzQkFBQTtBQUNGOztBQUVBO0VBQ0UsYUFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxTQUFBO0VBRUEsMEJBQUE7RUFDQSxpQkFBQTtFQUNBLHVCQUFBO0FBQUY7O0FBRUU7RUFFRSxtQkFBQTtFQUNBLFVBQUE7RUFDQSxzQkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLGtCQUFBO0FBREo7O0FBS0E7RUFDRSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsVUFBQTtBQUZGOztBQUtBO0VBQ0UsUUFBQTtBQUZGOztBQUtBO0VBQ0UsU0FBQTtBQUZGOztBQUtBO0VBQ0UsZ0RBQUE7QUFGRjs7QUFLQTtFQUNFLCtDQUFBO0FBRkY7O0FBS0E7RUFDRSxVQUFBO0FBRkY7O0FBS0E7RUFDRSxTQUFBO0FBRkY7O0FBS0E7RUFDRSxrQkFBQTtFQUNBLHFCQUFBO0VBQ0Esb0JBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSx5QkFBQTtFQUNBLG1CQUFBO0VBQ0EsVUFBQTtBQUZGOztBQUtBO0VBQ0Usb0NBQUE7RUFDQSxVQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSw0QkFBQTtFQUNBLHNCQUFBO0VBQ0Esb0JBQUE7QUFGRjs7QUFLQTtFQUNFLDRCQUFBO0FBRkY7O0FBS0E7RUFDRSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QUFGRjs7QUFLQTtFQUNFLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsV0FBQTtBQUZGOztBQUtBO0VBQ0UsYUFBQTtBQUZGOztBQUtBO0VBQ0UsNEJBQUE7RUFDQSxvREFBQTtBQUZGOztBQUdFO0VBQ0Usd0JBQUE7QUFESjs7QUFNQTtFQUNFLFlBQUE7RUFDQSxvQ0FBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSwwQkFBQTtBQUhGOztBQU1BO0VBQ0U7SUFDRSxRQUFBO0VBSEY7RUFLQTtJQUNFLFdBQUE7RUFIRjtBQUNGIiwiZmlsZSI6Im1pbGVzdG9uZXMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiKixcclxuKjo6YmVmb3JlLFxyXG4qOjphZnRlciB7XHJcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxufVxyXG5cclxuaW9uLWNvbnRlbnQge1xyXG4gIGRpcmVjdGlvbjogbHRyICFpbXBvcnRhbnQ7XHJcbiAgLS1iYWNrZ3JvdW5kOiB1cmwoXCIuLy4uLy4uLy4uL2Fzc2V0cy9pbWdzL2JnLnBuZ1wiKSAwIDAvMTAwJSAxMDAlIG5vLXJlcGVhdDtcclxufVxyXG5cclxuLnRpbWVsaW5lIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgbWFyZ2luOiAwIGF1dG87XHJcbiAgcGFkZGluZzogMTVweCAwO1xyXG4gIC0tcGFkZGluZy1ib3R0b206IDcwcHg7XHJcbn1cclxuXHJcbi5zY3JvbGwtZGl2IHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIHdpZHRoOiAxNXB4O1xyXG4gIGJhY2tncm91bmQ6IHJnYigyMzAgMjMwIDIzMCk7XHJcbiAgcG9zaXRpb246IGZpeGVkO1xyXG4gIGxlZnQ6IDQ5JTtcclxuICAvLyBoZWlnaHQ6IDEwMCU7XHJcbiAgaGVpZ2h0OiBjYWxjKDEwMHZoIC0gODBweCk7XHJcbiAgbWFyZ2luLWxlZnQ6IC0xcHg7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcblxyXG4gIC5zY3JvbGwge1xyXG4gICAgLy8gbWFyZ2luLXRvcDogMXJlbTtcclxuICAgIGJhY2tncm91bmQ6IHJnYigxMDksIDg4LCA3MCk7XHJcbiAgICB3aWR0aDogOTAlO1xyXG4gICAgYm9yZGVyOiAjZmZmIDNweCBzb2xpZDtcclxuICAgIGhlaWdodDogNzRweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDMzJTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHotaW5kZXg6IDE7XHJcbiAgICBvdmVyZmxvdy15OiBzY3JvbGw7XHJcbiAgfVxyXG59XHJcblxyXG4uY29udGFpbmVyIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgYmFja2dyb3VuZDogaW5oZXJpdDtcclxuICB3aWR0aDogNTAlO1xyXG59XHJcblxyXG4uY29udGFpbmVyLmxlZnQge1xyXG4gIGxlZnQ6IDQlO1xyXG59XHJcblxyXG4uY29udGFpbmVyLnJpZ2h0IHtcclxuICBsZWZ0OiA1MiU7XHJcbn1cclxuXHJcbi5jb250YWluZXIubGVmdCAuY29udGVudCB7XHJcbiAgYm94LXNoYWRvdzogLTVweCA3cHggNnB4IDBweCByZ2IoMCAwIDAgLyAyNSUpO1xyXG59XHJcblxyXG4uY29udGFpbmVyLnJpZ2h0IC5jb250ZW50IHtcclxuICBib3gtc2hhZG93OiA5cHggNXB4IDdweCAwcHggcmdiKDAgMCAwIC8gMjUlKTtcclxufVxyXG5cclxuLmNvbnRhaW5lci5yaWdodDo6YWZ0ZXIge1xyXG4gIGxlZnQ6IC04cHg7XHJcbn1cclxuXHJcbi5jb250YWluZXIucmlnaHQ6OmJlZm9yZSB7XHJcbiAgbGVmdDogOHB4O1xyXG59XHJcblxyXG4uY29udGFpbmVyIC5kYXRlIHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIHRvcDogY2FsYyg1MCUgLSA4cHgpO1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBmb250LXNpemU6IDE0cHg7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgY29sb3I6ICMwMDZlNTE7XHJcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICBsZXR0ZXItc3BhY2luZzogMXB4O1xyXG4gIHotaW5kZXg6IDE7XHJcbn1cclxuXHJcbi5jb250YWluZXIgLmNvbnRlbnQge1xyXG4gIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxuICB3aWR0aDogOTAlO1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGhlaWdodDogMTAwcHg7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIG1hcmdpbi10b3A6IDMwcHg7XHJcbiAgYm9yZGVyLXJhZGl1czogMTBweCAwIDAgMTBweDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIHBhZGRpbmc6IDAgNXB4IDAgNXB4O1xyXG59XHJcblxyXG4uY29udGFpbmVyLnJpZ2h0IC5jb250ZW50IHtcclxuICBib3JkZXItcmFkaXVzOiAwIDEwcHggMTBweCAwO1xyXG59XHJcblxyXG4uY29udGFpbmVyIC5jb250ZW50IGgyIHtcclxuICBmb250LXNpemU6IDEzcHg7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgbWFyZ2luLWJvdHRvbTogMDtcclxufVxyXG5cclxuLmNvbnRhaW5lciAuY29udGVudCBwIHtcclxuICBtYXJnaW4tYm90dG9tOiAxOHB4O1xyXG4gIGZvbnQtc2l6ZTogMTRweDtcclxuICBsaW5lLWhlaWdodDogMjJweDtcclxuICBjb2xvcjogI2ZmZjtcclxufVxyXG5cclxuOjotd2Via2l0LXNjcm9sbGJhciB7XHJcbiAgZGlzcGxheTogbm9uZTtcclxufVxyXG5cclxuLnBkZi1wYWdlcyB7XHJcbiAgLS1idWxsZXQtYmFja2dyb3VuZDogIzAwMDAwMDtcclxuICAtLWJ1bGxldC1iYWNrZ3JvdW5kLWFjdGl2ZTogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG4gIC5zd2lwZXItcGFnaW5hdGlvbiB7XHJcbiAgICBib3R0b206IC04MHB4ICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG59XHJcblxyXG5cclxuLnNraWxsLXBlcntcclxuICBoZWlnaHQ6IDE0cHg7XHJcbiAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG4gIGJvcmRlci1yYWRpdXM6IDNweDtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgYW5pbWF0aW9uOiBmaWxsQmFycyAyLjVzIDE7XHJcbn1cclxuXHJcbkBrZXlmcmFtZXMgZmlsbEJhcnN7XHJcbiAgZnJvbXtcclxuICAgIHdpZHRoOiAwO1xyXG4gIH1cclxuICB0b3tcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxufSJdfQ== */");

/***/ }),

/***/ 22808:
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/milestones/milestones.page.html ***!
  \*********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<app-header [title]=\"title\" format=\"png\" [icon]=\"icon\"></app-header>\r\n\r\n<ion-content>\r\n\r\n  <div class=\"ion-text-center\" *ngIf=\"isLoading\">\r\n    <ion-spinner></ion-spinner>\r\n  </div>\r\n\r\n  <ion-slides [hidden]=\"isLoading\" class=\"pdf-pages\" (ionSlideDidChange)=\"onSlideChange()\" (ionSlideReachEnd)=\"slide_ended = true\" pager=\"true\" [options]=\"slideOpts\" [mode]=\"'ios'\"\r\n    [scrollbar]=\"false\">\r\n    <ion-slide *ngFor=\"let pdf of pdfs; let i = index\">\r\n      <img [src]=\"'assets/pdfs/' + lang + '/' + this.utility.strings.twoLetterNumber(pdf) + '.png'  \">\r\n    </ion-slide>\r\n  </ion-slides>\r\n\r\n  <div class=\"skill\">\r\n    <div class=\"skill-bar\">\r\n      <div class=\"skill-per\" [style]=\"'max-width:' + percent.toString() + '%'\"></div>\r\n    </div>\r\n  </div>\r\n\r\n</ion-content>\r\n\r\n\r\n\r\n\r\n\r\n<!-- <ion-content [scrollEvents]=\"true\" (ionScroll)=\"logScrolling($event)\" (ionScrollEnd)=\"logEndFunc($event)\"\r\n    class=\"timeline\" *ngIf=\"!isLoading\" #content class=\"fade-in-top\">\r\n\r\n    <div #scrolldiv class=\"scroll-div\">\r\n        <div #scroll class=\"scroll\"></div>\r\n    </div>\r\n\r\n\r\n    <div *ngFor=\"let item of data; let i = index\" class=\"container\" [ngClass]=\"i % 2 ? 'left' : 'right' \">\r\n        <div (click)=\"details(item)\" class=\"content\">\r\n            <h2 [style]=\"lang == 'ar' ? 'font-size : 16px;' : 'font-size : 12px;'\">{{item.title}}</h2>\r\n            <p>\r\n                {{item.date}}\r\n            </p>\r\n        </div>\r\n    </div>\r\n</ion-content>\r\n\r\n<div class=\"spinner\" *ngIf=\"isLoading\">\r\n    <ion-spinner color=\"primary\"></ion-spinner>\r\n</div> -->\r\n\r\n<!-- <img class=\"background-image\" src=\"./../../../assets/imgs/bg.png\"> -->");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_milestones_milestones_module_ts.js.map