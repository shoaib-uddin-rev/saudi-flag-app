(self["webpackChunkSaudi_Flag_App"] = self["webpackChunkSaudi_Flag_App"] || []).push([["main"],{

/***/ 98255:
/*!*******************************************************!*\
  !*** ./$_lazy_route_resources/ lazy namespace object ***!
  \*******************************************************/
/***/ ((module) => {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(() => {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = () => ([]);
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 98255;
module.exports = webpackEmptyAsyncContext;

/***/ }),

/***/ 70809:
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppRoutingModule": () => (/* binding */ AppRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ 29535);



const routes = [
    {
        path: '',
        redirectTo: 'pages',
        pathMatch: 'full',
    },
    {
        path: 'pages',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_pages_pages_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./pages/pages.module */ 59525)).then((m) => m.PagesModule),
    },
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_1__.NgModule)({
        imports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule.forRoot(routes, { preloadingStrategy: _angular_router__WEBPACK_IMPORTED_MODULE_2__.PreloadAllModules }),
        ],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule],
    })
], AppRoutingModule);



/***/ }),

/***/ 20721:
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppComponent": () => (/* binding */ AppComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _raw_loader_app_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./app.component.html */ 91106);
/* harmony import */ var _app_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./app.component.scss */ 43069);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _services_events_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./services/events.service */ 16253);
/* harmony import */ var _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/android-permissions/ngx */ 68491);
/* harmony import */ var _services_sqlite_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./services/sqlite.service */ 2709);







let AppComponent = class AppComponent {
    constructor(events, androidPermissions, sqlite) {
        this.events = events;
        this.androidPermissions = androidPermissions;
        this.sqlite = sqlite;
        this.dir = "ltr";
        this.initialize().then(() => {
            this.events.subscribe('language:change', (lang) => {
                if (lang == "en") {
                    this.dir = "ltr";
                }
                else if (lang == "ar") {
                    this.dir = "rtl";
                }
            });
        });
    }
    initialize() {
        return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            yield this.askForPermissions();
            yield this.sqlite.initialize();
        }));
    }
    askForPermissions() {
        return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).then(((result) => (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
                if (result.hasPermission) {
                    resolve(true);
                }
                else {
                    this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).then((result) => {
                        if (result.hasPermission) {
                            resolve(true);
                        }
                        else {
                            this.exitApp();
                        }
                    });
                }
            })));
        }));
    }
    exitApp() {
        navigator['app'].exitApp();
    }
};
AppComponent.ctorParameters = () => [
    { type: _services_events_service__WEBPACK_IMPORTED_MODULE_2__.EventsService },
    { type: _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_3__.AndroidPermissions },
    { type: _services_sqlite_service__WEBPACK_IMPORTED_MODULE_4__.SqliteService }
];
AppComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-root',
        template: _raw_loader_app_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_app_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], AppComponent);



/***/ }),

/***/ 50023:
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppModule": () => (/* binding */ AppModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/platform-browser */ 93220);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/router */ 29535);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @ionic/angular */ 34595);
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app.component */ 20721);
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./app-routing.module */ 70809);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/common/http */ 31887);
/* harmony import */ var _services_interceptor_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./services/interceptor.service */ 9374);
/* harmony import */ var _pscoped_ngx_pub_sub__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @pscoped/ngx-pub-sub */ 42285);
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./services/api.service */ 43734);
/* harmony import */ var _services_basic_storage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./services/basic/storage.service */ 94274);
/* harmony import */ var _services_events_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./services/events.service */ 16253);
/* harmony import */ var _services_network_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./services/network.service */ 99919);
/* harmony import */ var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/in-app-browser/ngx */ 38544);
/* harmony import */ var _ionic_native_app_launcher_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/app-launcher/ngx */ 91234);
/* harmony import */ var _ionic_native_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/launch-navigator/ngx */ 86206);
/* harmony import */ var _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/android-permissions/ngx */ 68491);
/* harmony import */ var simplebar_angular__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! simplebar-angular */ 16152);



















let AppModule = class AppModule {
};
AppModule = (0,tslib__WEBPACK_IMPORTED_MODULE_11__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_12__.NgModule)({
        declarations: [_app_component__WEBPACK_IMPORTED_MODULE_0__.AppComponent],
        entryComponents: [],
        imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_13__.BrowserModule, _ionic_angular__WEBPACK_IMPORTED_MODULE_14__.IonicModule.forRoot(), _app_routing_module__WEBPACK_IMPORTED_MODULE_1__.AppRoutingModule, _angular_common_http__WEBPACK_IMPORTED_MODULE_15__.HttpClientModule, simplebar_angular__WEBPACK_IMPORTED_MODULE_16__.SimplebarAngularModule],
        providers: [
            { provide: _angular_router__WEBPACK_IMPORTED_MODULE_17__.RouteReuseStrategy, useClass: _ionic_angular__WEBPACK_IMPORTED_MODULE_14__.IonicRouteStrategy },
            { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_15__.HTTP_INTERCEPTORS, useClass: _services_interceptor_service__WEBPACK_IMPORTED_MODULE_2__.InterceptorService, multi: true },
            _pscoped_ngx_pub_sub__WEBPACK_IMPORTED_MODULE_18__.NgxPubSubService,
            _services_events_service__WEBPACK_IMPORTED_MODULE_5__.EventsService,
            _services_basic_storage_service__WEBPACK_IMPORTED_MODULE_4__.StorageService,
            _services_api_service__WEBPACK_IMPORTED_MODULE_3__.ApiService,
            _services_network_service__WEBPACK_IMPORTED_MODULE_6__.NetworkService,
            _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_7__.InAppBrowser,
            _ionic_native_app_launcher_ngx__WEBPACK_IMPORTED_MODULE_8__.AppLauncher,
            _ionic_native_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_9__.LaunchNavigator,
            _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_10__.AndroidPermissions
            // PhotoViewer
        ],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_0__.AppComponent],
    })
], AppModule);



/***/ }),

/***/ 11048:
/*!***************************************!*\
  !*** ./src/app/config/main.config.ts ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Config": () => (/* binding */ Config)
/* harmony export */ });
const Config = {
    // api: 'https://zuul2.zuulsystems.com/api',
    api: 'https://activiaarabia.com/saudiflag/public/api',
    public: 'https://activiaarabia.com/saudiflag/public',
    local: 'http://127.0.0.1:8000/api',
    API_KEY: '4314791685',
    API_SECRET: 'girnqlhg4738qtgbfu438',
};
// export const Config = {
//   api: 'http://ec2-18-217-195-71.us-east-2.compute.amazonaws.com/public/index.php/api',
//   imageURL: 'http://ec2-18-217-195-71.us-east-2.compute.amazonaws.com/public/index.php/images'
// };


/***/ }),

/***/ 43734:
/*!*****************************************!*\
  !*** ./src/app/services/api.service.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ApiService": () => (/* binding */ ApiService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ 31887);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _config_main_config__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../config/main.config */ 11048);




let ApiService = class ApiService {
    constructor(http) {
        this.http = http;
        this.url = _config_main_config__WEBPACK_IMPORTED_MODULE_0__.Config.api;
    }
    get(endpoint, params, reqOpts) {
        return this.http.get(this.url + '/' + endpoint, reqOpts);
    }
    post(endpoint, body, reqOpts) {
        return this.http.post(this.url + '/' + endpoint, body, reqOpts);
    }
    put(endpoint, body, reqOpts) {
        return this.http.put(this.url + '/' + endpoint, body, reqOpts);
    }
    delete(endpoint, reqOpts) {
        return this.http.delete(this.url + '/' + endpoint, reqOpts);
    }
    patch(endpoint, body, reqOpts) {
        return this.http.patch(this.url + '/' + endpoint, body, reqOpts);
    }
};
ApiService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__.HttpClient }
];
ApiService = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Injectable)({
        providedIn: 'root',
    })
], ApiService);



/***/ }),

/***/ 18119:
/*!**************************************************!*\
  !*** ./src/app/services/basic/alerts.service.ts ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AlertsService": () => (/* binding */ AlertsService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _strings_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./strings.service */ 98601);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ 34595);





let AlertsService = class AlertsService {
    constructor(alertController, toastCtrl, strings) {
        this.alertController = alertController;
        this.toastCtrl = toastCtrl;
        this.strings = strings;
    }
    showAlert(msg, title = 'Alert') {
        return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__awaiter)(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: 'my-custom-class',
                header: title,
                message: msg,
                buttons: [
                    {
                        text: 'OK',
                        cssClass: 'secondary',
                        handler: (blah) => {
                            resolve(true);
                        },
                    },
                ],
            });
            yield alert.present();
        }));
    }
    presentSuccessToast(msg, cssClass) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__awaiter)(this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: this.strings.capitalizeEachFirst(msg),
                duration: 5000,
                position: 'top',
                cssClass: cssClass,
            });
            toast.present();
        });
    }
    presentFailureToast(msg, cssClass) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__awaiter)(this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: this.strings.capitalizeEachFirst(msg ? msg : 'ERROR'),
                duration: 5000,
                position: 'top',
                cssClass: cssClass,
            });
            toast.present();
        });
    }
    presentToast(msg) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__awaiter)(this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: msg,
                duration: 5000,
                position: 'bottom',
            });
            toast.present();
        });
    }
    presentConfirm(okText = 'OK', cancelText = 'Cancel', title = 'Are You Sure?', message = '') {
        return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__awaiter)(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: title,
                message: message,
                buttons: [
                    {
                        text: cancelText,
                        role: 'cancel',
                        handler: () => {
                            resolve(false);
                        },
                    },
                    {
                        text: okText,
                        handler: () => {
                            resolve(true);
                        },
                    },
                ],
            });
            alert.present();
        }));
    }
    presentInput(okText = 'OK', cancelText = 'Cancel', title = 'Are You Sure?', message = '') {
        return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__awaiter)(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: title,
                message: message,
                inputs: [
                    {
                        name: 'qrcode',
                        placeholder: 'QRCODE',
                    },
                ],
                buttons: [
                    {
                        text: cancelText,
                        role: 'cancel',
                        handler: () => {
                            resolve(false);
                        },
                    },
                    {
                        text: okText,
                        handler: (data) => {
                            if (!data.qrcode || data.qrcode == '') {
                                resolve(null);
                            }
                            else {
                                resolve(data.qrcode);
                            }
                        },
                    },
                ],
            });
            alert.present();
        }));
    }
    presentRadioSelections(title, message, inputs, okText = 'OK', cancelText = 'Cancel') {
        return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__awaiter)(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: title,
                message: message,
                inputs: inputs,
                buttons: [
                    {
                        text: cancelText,
                        role: 'cancel',
                        handler: () => {
                            resolve(false);
                        },
                    },
                    {
                        text: okText,
                        handler: (data) => {
                            resolve(data);
                        },
                    },
                ],
            });
            alert.present();
        }));
    }
};
AlertsService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__.AlertController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__.ToastController },
    { type: _strings_service__WEBPACK_IMPORTED_MODULE_0__.StringsService }
];
AlertsService = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Injectable)({
        providedIn: 'root',
    })
], AlertsService);



/***/ }),

/***/ 55509:
/*!***************************************************!*\
  !*** ./src/app/services/basic/loading.service.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoadingService": () => (/* binding */ LoadingService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ 34595);



let LoadingService = class LoadingService {
    constructor(loadingController) {
        this.loadingController = loadingController;
    }
    showLoader(message = 'Please wait...') {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__awaiter)(this, void 0, void 0, function* () {
            this.loading = yield this.loadingController.create({
                cssClass: 'my-custom-class',
                message: message,
            });
            yield this.loading.present();
        });
    }
    hideLoader() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__awaiter)(this, void 0, void 0, function* () {
            if (this.loading) {
                this.loading.dismiss();
            }
        });
    }
};
LoadingService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__.LoadingController }
];
LoadingService = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Injectable)({
        providedIn: 'root'
    })
], LoadingService);



/***/ }),

/***/ 94274:
/*!***************************************************!*\
  !*** ./src/app/services/basic/storage.service.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "StorageService": () => (/* binding */ StorageService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ionic/angular */ 34595);



let StorageService = class StorageService {
    constructor(platform) {
        this.platform = platform;
    }
    set(key, data) {
        return new Promise((resolve) => {
            localStorage.setItem(key, data);
            resolve(key);
        });
    }
    get(key) {
        return new Promise((resolve) => {
            const v = localStorage.getItem(key);
            resolve(v);
        });
    }
};
StorageService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_0__.Platform }
];
StorageService = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Injectable)({
        providedIn: 'root',
    })
], StorageService);



/***/ }),

/***/ 98601:
/*!***************************************************!*\
  !*** ./src/app/services/basic/strings.service.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "StringsService": () => (/* binding */ StringsService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 42741);


let StringsService = class StringsService {
    constructor() { }
    twoLetterNumber(n) {
        if (n < 10) {
            return '0' + n;
        }
        else {
            return '' + n + '';
        }
    }
    capitalizeEachFirst(str) {
        if (!str) {
            return '';
        }
        const splitStr = str.toLowerCase().split(' ');
        for (let i = 0; i < splitStr.length; i++) {
            // You do not need to check if i is larger than splitStr length, as your for does that for you
            // Assign it back to the array
            splitStr[i] =
                splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
        }
        // Directly return the joined string
        return splitStr.join(' ');
    }
    checkIfMatchingPasswords(passwordKey, passwordConfirmationKey) {
        return (group) => {
            const passwordInput = group.controls[passwordKey];
            const passwordConfirmationInput = group.controls[passwordConfirmationKey];
            if (passwordInput.value !== passwordConfirmationInput.value) {
                return passwordConfirmationInput.setErrors({ notEquivalent: true });
            }
            else {
                return passwordConfirmationInput.setErrors(null);
            }
        };
    }
};
StringsService.ctorParameters = () => [];
StringsService = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_1__.Injectable)({
        providedIn: 'root',
    })
], StringsService);



/***/ }),

/***/ 16253:
/*!********************************************!*\
  !*** ./src/app/services/events.service.ts ***!
  \********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "EventsService": () => (/* binding */ EventsService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _pscoped_ngx_pub_sub__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @pscoped/ngx-pub-sub */ 42285);



let EventsService = class EventsService {
    constructor(pubsubSvc) {
        this.pubsubSvc = pubsubSvc;
        this.latestEvent = 'randomLast';
        this.historicalEvent = 'randomHistory';
        pubsubSvc.registerEventWithHistory(this.historicalEvent, 6);
        pubsubSvc.registerEventWithLastValue(this.latestEvent, undefined);
    }
    publish(key, data = {}) {
        this.pubsubSvc.publishEvent(key, data);
    }
    subscribe(key, handler) {
        this.pubsubSvc.subscribe(key, data => handler(data));
        // this.subscriptions[key] = 
    }
    unsubscribe(key) {
        // this.subscriptions[key].unsubscribe();
    }
};
EventsService.ctorParameters = () => [
    { type: _pscoped_ngx_pub_sub__WEBPACK_IMPORTED_MODULE_0__.NgxPubSubService }
];
EventsService = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Injectable)({
        providedIn: 'root'
    })
], EventsService);



/***/ }),

/***/ 9374:
/*!*************************************************!*\
  !*** ./src/app/services/interceptor.service.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "InterceptorService": () => (/* binding */ InterceptorService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _config_main_config__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../config/main.config */ 11048);
/* harmony import */ var _basic_storage_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./basic/storage.service */ 94274);
/* harmony import */ var _utility_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./utility.service */ 70494);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ 58538);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ 32812);







let InterceptorService = class InterceptorService {
    constructor(storage, utility) {
        this.storage = storage;
        this.utility = utility;
        this.api = _config_main_config__WEBPACK_IMPORTED_MODULE_0__.Config.api;
        this.api_key = _config_main_config__WEBPACK_IMPORTED_MODULE_0__.Config.API_KEY;
        this.api_secret = _config_main_config__WEBPACK_IMPORTED_MODULE_0__.Config.API_SECRET;
    }
    intercept(req, next) {
        return (0,rxjs__WEBPACK_IMPORTED_MODULE_3__.from)(this.callToken()).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_4__.switchMap)((token) => {
            const cloneRequest = this.addSecret(req, token);
            return next.handle(cloneRequest);
        }));
    }
    callToken() {
        return new Promise((resolve) => {
            let lang = localStorage.getItem('lang');
            lang = lang ? lang : 'en';
            resolve(lang);
        });
    }
    addSecret(request, value) {
        let v = value ? value : '';
        let clone = request.clone({
            setHeaders: {
                lang: v,
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
        });
        return clone;
    }
};
InterceptorService.ctorParameters = () => [
    { type: _basic_storage_service__WEBPACK_IMPORTED_MODULE_1__.StorageService },
    { type: _utility_service__WEBPACK_IMPORTED_MODULE_2__.UtilityService }
];
InterceptorService = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Injectable)({
        providedIn: 'root',
    })
], InterceptorService);



/***/ }),

/***/ 99919:
/*!*********************************************!*\
  !*** ./src/app/services/network.service.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NetworkService": () => (/* binding */ NetworkService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _events_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./events.service */ 16253);
/* harmony import */ var _api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./api.service */ 43734);
/* harmony import */ var _utility_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./utility.service */ 70494);





let NetworkService = class NetworkService {
    constructor(utility, api, events) {
        this.utility = utility;
        this.api = api;
        this.events = events;
        // console.log('Hello NetworkProvider Provider');
    }
    getSplashData() {
        return this.httpGetResponse('get_splash_data', null, null);
    }
    getDashboardData() {
        return this.httpGetResponse('get_dashboard_data', null);
    }
    getMilestones() {
        return this.httpGetResponse('get_milestone_data', null);
    }
    getFlagHistory() {
        return this.httpGetResponse('get_flaghistory_data', null);
    }
    getFlagRulesMain() {
        return this.httpGetResponse('get_flagrules_data', null);
    }
    getFlagRuleData(key) {
        return this.httpGetResponse('get_flagrules_data/' + key, null);
    }
    getMastData() {
        return this.httpGetResponse('get_mast_data', null);
    }
    getGalleryImages() {
        return this.httpGetResponse('get_gallery_data', null);
    }
    postContactUsData(data) {
        console.log('contact data', data);
        return this.httpPostResponse('post_contact_us', data, null);
    }
    // get requests -- end
    httpPostResponse(key, data, id = null, showloader = false, showError = false, contenttype = 'application/json') {
        return this.httpResponse('post', key, data, id, showloader, showError, contenttype);
    }
    httpGetResponse(key, id = null, showloader = false, showError = false, contenttype = 'application/json') {
        return this.httpResponse('get', key, {}, id, showloader, showError, contenttype);
    }
    httpPatchResponse(key, data, id = null, showloader = false, showError = false, contenttype = 'application/json') {
        return new Promise((resolve, reject) => {
            id = id ? `/${id}` : '';
            const url = key + id;
            this.api.patch(key, data).subscribe((res) => {
                if (res.bool !== true) {
                    if (showError) {
                        this.utility.presentSuccessToast(res.message, '');
                    }
                    reject(null);
                }
                else {
                    resolve(res.result);
                }
            });
        });
    }
    // default 'Content-Type': 'application/json',
    httpResponse(type = 'get', key, data, id = null, showloader = false, showError = true, contenttype = 'application/json') {
        return new Promise((resolve, reject) => {
            if (showloader == true) {
                this.utility.showLoader();
            }
            const _id = id ? '/' + id : '';
            const url = key + _id;
            const seq = type == 'get' ? this.api.get(url, {}) : this.api.post(url, data);
            seq.subscribe((res) => {
                if (showloader === true) {
                    this.utility.hideLoader();
                }
                if (res.bool !== true) {
                    if (showError) {
                        this.utility.presentSuccessToast(res.message, '');
                    }
                    reject(null);
                }
                else {
                    resolve(res.data);
                }
            }, (err) => {
                const error = err.error;
                if (showloader === true) {
                    this.utility.hideLoader();
                }
                if (showError) {
                    this.utility.presentFailureToast(error.message, '');
                }
                console.log(err);
                // if(err.status === 401){
                //   this.router.navigate(['splash']);
                // }
                reject(null);
            });
        });
    }
    showFailure(err) {
        // console.error('ERROR', err);
        const _error = err ? err.message : 'check logs';
        this.utility.presentFailureToast(_error, '');
    }
    getActivityLogs() {
        return this.httpGetResponse('get_activity_logs', null, true);
    }
};
NetworkService.ctorParameters = () => [
    { type: _utility_service__WEBPACK_IMPORTED_MODULE_2__.UtilityService },
    { type: _api_service__WEBPACK_IMPORTED_MODULE_1__.ApiService },
    { type: _events_service__WEBPACK_IMPORTED_MODULE_0__.EventsService }
];
NetworkService = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Injectable)({
        providedIn: 'root',
    })
], NetworkService);



/***/ }),

/***/ 2709:
/*!********************************************!*\
  !*** ./src/app/services/sqlite.service.ts ***!
  \********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SqliteService": () => (/* binding */ SqliteService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic/angular */ 34595);
/* harmony import */ var _network_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./network.service */ 99919);
/* harmony import */ var _utility_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./utility.service */ 70494);
/* harmony import */ var _basic_storage_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./basic/storage.service */ 94274);
/* harmony import */ var _capacitor_community_sqlite__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @capacitor-community/sqlite */ 46610);
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @capacitor/core */ 68107);
/* harmony import */ var _capacitor_network__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @capacitor/network */ 10557);









const SQL_DB_NAME = '__flagapp.db';
let SqliteService = class SqliteService {
    constructor(injector, 
    // public sqlite: SQLite,
    platform, storage, network, utility) {
        this.platform = platform;
        this.storage = storage;
        this.network = network;
        this.utility = utility;
        this.msg = 'Sync In Progress ...';
        this.milestones = __webpack_require__(/*! ./../../assets/json/milestones.json */ 25578);
        this.flag_history = __webpack_require__(/*! ./../../assets/json/flag_history.json */ 69069);
        this.flag_rules = __webpack_require__(/*! ./../../assets/json/flag_rules.json */ 6715);
        this.mast = __webpack_require__(/*! ./../../assets/json/mast.json */ 62931);
    }
    initialize() {
        return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            if (_capacitor_core__WEBPACK_IMPORTED_MODULE_4__.Capacitor.getPlatform() != 'web') {
                yield this.initializeDatabase();
                resolve(true);
            }
            else {
                resolve(true);
            }
        }));
    }
    initializeDatabase() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
                yield this.platform.ready();
                // initialize database object
                const flag = yield this.createDatabase();
                if (!flag) {
                    resolve(false);
                    return;
                }
                // initialize all tables
                this.storage.get('is_database_initialized').then((v) => (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
                    if (!v) {
                        // await this.initializeSync();
                        yield this.initializeMilestonesTable();
                        yield this.setMileStonesInDatabase(this.milestones.data);
                        yield this.initializeFlaghistoryTable();
                        yield this.setFlagHistoryInDatabase(this.flag_history.data);
                        yield this.initializeFlagrulesTable();
                        yield this.setFlagrulesInDatabase(this.flag_rules.data);
                        yield this.initializeMastTable();
                        yield this.setMastInDatabase(this.mast.data);
                    }
                }));
                this.storage.set('is_database_initialized', true);
                resolve(true);
            }));
        });
    }
    // initializeSync() {
    //     return new Promise<any>((resolve) => {
    //         this.interval = setInterval(() => {
    //         })
    //     })
    // }
    createDatabase() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
                if (_capacitor_core__WEBPACK_IMPORTED_MODULE_4__.Capacitor.getPlatform() != 'web') {
                    const dbName = SQL_DB_NAME;
                    const dbNames = [SQL_DB_NAME];
                    const ret = yield _capacitor_community_sqlite__WEBPACK_IMPORTED_MODULE_3__.CapacitorSQLite.checkConnectionsConsistency({ dbNames: dbNames });
                    console.log("SQLITE INITIALIZED", ret);
                    if (ret.result == false) {
                        yield _capacitor_community_sqlite__WEBPACK_IMPORTED_MODULE_3__.CapacitorSQLite.createConnection({ database: dbName });
                        yield _capacitor_community_sqlite__WEBPACK_IMPORTED_MODULE_3__.CapacitorSQLite.open({ database: dbName });
                        resolve(true);
                    }
                    else {
                        yield _capacitor_community_sqlite__WEBPACK_IMPORTED_MODULE_3__.CapacitorSQLite.open({ database: dbName });
                        resolve(true);
                    }
                }
                else {
                    this.db = window.openDatabase(SQL_DB_NAME, '1.0', 'DEV', 5 * 1024 * 1024);
                    this.msg = 'Database initialized';
                    resolve(true);
                }
            }));
        });
    }
    syncMilestonesData() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
                const status = yield _capacitor_network__WEBPACK_IMPORTED_MODULE_5__.Network.getStatus();
                if (status.connected) {
                    let update = yield this.network.getMilestones();
                    if (update) {
                        yield this.setMileStonesInDatabase(update);
                    }
                    resolve(true);
                }
                else {
                    resolve(false);
                }
            }));
        });
    }
    syncFlagHistoryData() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
                const status = yield _capacitor_network__WEBPACK_IMPORTED_MODULE_5__.Network.getStatus();
                if (status.connected) {
                    const update = yield this.network.getFlagHistory();
                    if (update) {
                        yield this.setFlagHistoryInDatabase(update);
                    }
                    console.log(update);
                    resolve(true);
                }
                else {
                    resolve(false);
                }
            }));
        });
    }
    syncMastData() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
                const status = yield _capacitor_network__WEBPACK_IMPORTED_MODULE_5__.Network.getStatus();
                if (status.connected) {
                    const update = yield this.network.getMastData();
                    if (update) {
                        yield this.setMastInDatabase(update);
                    }
                    console.log(update);
                    resolve(true);
                }
                else {
                    resolve(false);
                }
            }));
        });
    }
    syncFlagRule(key) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
                const status = yield _capacitor_network__WEBPACK_IMPORTED_MODULE_5__.Network.getStatus();
                if (status.connected) {
                    const update = yield this.network.getFlagRuleData(key);
                    if (update) {
                        yield this.setFlagrulesInDatabase([update]);
                    }
                    console.log(update);
                    resolve(true);
                }
                else {
                    resolve(false);
                }
            }));
        });
    }
    initializeMilestonesTable() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            return new Promise(resolve => {
                // create statement
                let sql = 'CREATE TABLE IF NOT EXISTS milestones(';
                sql += 'id INT PRIMARY KEY, ';
                sql += 'title_en TEXT, ';
                sql += 'date_en TEXT, ';
                sql += 'content_en TEXT, ';
                sql += 'title_ar TEXT, ';
                sql += 'date_ar TEXT, ';
                sql += 'content_ar TEXT )';
                this.msg = 'Initializing milestones ...';
                console.log(this.msg);
                resolve(this.execute(sql, []));
            });
        });
    }
    setMileStonesInDatabase(milestones) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
                const insertRows = [];
                for (const row of milestones) {
                    let sql = 'INSERT OR REPLACE into milestones(';
                    sql += 'id, ';
                    sql += 'title_en, ';
                    sql += 'date_en, ';
                    sql += 'content_en, ';
                    sql += 'title_ar, ';
                    sql += 'date_ar, ';
                    sql += 'content_ar )';
                    sql += ' VALUES ';
                    const values = [];
                    sql += '( ';
                    sql += '?, ';
                    values.push(row.id);
                    sql += '?, ';
                    values.push(row.title_en);
                    sql += '?, ';
                    values.push(row.date_en);
                    sql += '?, ';
                    values.push(row.content_en);
                    sql += '?, ';
                    values.push(row.title_ar);
                    sql += '?, ';
                    values.push(row.date_ar);
                    sql += '? ';
                    values.push(row.content_ar);
                    sql += ') ';
                    // await this.execute(sql, values);
                    insertRows.push([
                        sql,
                        values
                    ]);
                }
                yield this.prepareBatch(insertRows);
                resolve(true);
            }));
        });
    }
    initializeFlaghistoryTable() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            return new Promise(resolve => {
                // create statement
                let sql = 'CREATE TABLE IF NOT EXISTS flag_history(';
                sql += 'id INT PRIMARY KEY, ';
                sql += 'tab TEXT, ';
                sql += 'sort TEXT, ';
                sql += 'date_en TEXT, ';
                sql += 'date_ar TEXT, ';
                sql += 'content_en TEXT, ';
                sql += 'content_ar TEXT )';
                this.msg = 'Initializing flag_history ...';
                console.log(this.msg);
                resolve(this.execute(sql, []));
            });
        });
    }
    setFlagHistoryInDatabase(history) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
                const insertRows = [];
                for (const row of history) {
                    let sql = 'INSERT OR REPLACE into flag_history(';
                    sql += 'id, ';
                    sql += 'tab, ';
                    sql += 'sort, ';
                    sql += 'date_en, ';
                    sql += 'date_ar, ';
                    sql += 'content_en, ';
                    sql += 'content_ar )';
                    sql += ' VALUES ';
                    const values = [];
                    sql += '( ';
                    sql += '?, ';
                    values.push(row.id);
                    sql += '?, ';
                    values.push(row.tab);
                    sql += '?, ';
                    values.push(row.sort);
                    sql += '?, ';
                    values.push(row.date_en);
                    sql += '?, ';
                    values.push(row.date_ar);
                    sql += '?, ';
                    values.push(row.content_en);
                    sql += '? ';
                    values.push(row.content_ar);
                    sql += ') ';
                    // await this.execute(sql, values);
                    insertRows.push([
                        sql,
                        values
                    ]);
                }
                yield this.prepareBatch(insertRows);
                resolve(true);
            }));
        });
    }
    initializeMastTable() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            return new Promise(resolve => {
                // create statement
                let sql = 'CREATE TABLE IF NOT EXISTS mast(';
                sql += 'id INT PRIMARY KEY, ';
                sql += 'title_en TEXT, ';
                sql += 'flag_height_en TEXT, ';
                sql += 'title_ar TEXT, ';
                sql += 'flag_height_ar TEXT, ';
                sql += 'flag_area_en TEXT, ';
                sql += 'flag_weight_en TEXT, ';
                sql += 'flag_total_area_en TEXT, ';
                sql += 'flag_area_ar TEXT, ';
                sql += 'flag_weight_ar TEXT, ';
                sql += 'flag_total_area_ar TEXT, ';
                sql += 'location_en TEXT, ';
                sql += 'location_ar TEXT, ';
                sql += 'latitude TEXT, ';
                sql += 'longitude TEXT, ';
                sql += 'image TEXT, ';
                sql += 'sort TEXT )';
                this.msg = 'Initializing mast ...';
                console.log(this.msg);
                resolve(this.execute(sql, []));
            });
        });
    }
    setMastInDatabase(mast) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
                const insertRows = [];
                for (const row of mast) {
                    let sql = 'INSERT OR REPLACE into mast(';
                    sql += 'id, ';
                    sql += 'title_en, ';
                    sql += 'flag_height_en, ';
                    sql += 'title_ar, ';
                    sql += 'flag_height_ar, ';
                    sql += 'flag_area_en, ';
                    sql += 'flag_weight_en, ';
                    sql += 'flag_total_area_en, ';
                    sql += 'flag_area_ar, ';
                    sql += 'flag_weight_ar, ';
                    sql += 'flag_total_area_ar, ';
                    sql += 'location_en, ';
                    sql += 'location_ar, ';
                    sql += 'latitude, ';
                    sql += 'longitude, ';
                    sql += 'image, ';
                    sql += 'sort )';
                    sql += ' VALUES ';
                    const values = [];
                    sql += '( ';
                    sql += '?, ';
                    values.push(row.id);
                    sql += '?, ';
                    values.push(row.title_en);
                    sql += '?, ';
                    values.push(row.flag_height_en);
                    sql += '?, ';
                    values.push(row.title_ar);
                    sql += '?, ';
                    values.push(row.flag_height_ar);
                    sql += '?, ';
                    values.push(row.flag_area_en);
                    sql += '?, ';
                    values.push(row.flag_weight_en);
                    sql += '?, ';
                    values.push(row.flag_total_area_en);
                    sql += '?, ';
                    values.push(row.flag_area_ar);
                    sql += '?, ';
                    values.push(row.flag_weight_ar);
                    sql += '?, ';
                    values.push(row.flag_total_area_ar);
                    sql += '?, ';
                    values.push(row.location_en);
                    sql += '?, ';
                    values.push(row.location_ar);
                    sql += '?, ';
                    values.push(row.latitude);
                    sql += '?, ';
                    values.push(row.longitude);
                    sql += '?, ';
                    values.push(row.image);
                    sql += '? ';
                    values.push(row.sort);
                    sql += ') ';
                    // await this.execute(sql, values);
                    insertRows.push([
                        sql,
                        values
                    ]);
                }
                yield this.prepareBatch(insertRows);
                resolve(true);
            }));
        });
    }
    initializeFlagrulesTable() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            return new Promise(resolve => {
                // create statement
                let sql = 'CREATE TABLE IF NOT EXISTS flag_rules(';
                sql += 'id INT PRIMARY KEY, ';
                sql += 'key TEXT, ';
                sql += 'content_en TEXT, ';
                sql += 'content_ar TEXT )';
                this.msg = 'Initializing flag_rules ...';
                console.log(this.msg);
                resolve(this.execute(sql, []));
            });
        });
    }
    setFlagrulesInDatabase(rules) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
                const insertRows = [];
                for (const row of rules) {
                    let sql = 'INSERT OR REPLACE into flag_rules(';
                    sql += 'id, ';
                    sql += 'key, ';
                    sql += 'content_en, ';
                    sql += 'content_ar )';
                    sql += ' VALUES ';
                    const values = [];
                    sql += '( ';
                    sql += '?, ';
                    values.push(row.id);
                    sql += '?, ';
                    values.push(row.key);
                    sql += '?, ';
                    values.push(row.content_en);
                    sql += '? ';
                    values.push(row.content_ar);
                    sql += ') ';
                    // await this.execute(sql, values);
                    insertRows.push([
                        sql,
                        values
                    ]);
                }
                yield this.prepareBatch(insertRows);
                resolve(true);
            }));
        });
    }
    getMileStonesData(lang) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
                const sql = `SELECT id,content_${lang},title_${lang},date_${lang} FROM milestones`;
                const d = yield this.execute(sql, []);
                if (!d) {
                    resolve(null);
                    return;
                }
                // var data = d as any[];
                const data = this.getRows(d);
                let milestones = [];
                if (data.length > 0) {
                    data.forEach(element => {
                        let object = {
                            id: element["id"],
                            content: element["content_" + lang],
                            title: element["title_" + lang],
                            date: element["date_" + lang]
                        };
                        milestones.push(object);
                    });
                    resolve(milestones);
                }
                else {
                    resolve(null);
                }
            }));
        });
    }
    getFlagHistoryData(lang) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
                const sql = `SELECT id,content_${lang},tab,sort,date_${lang} FROM flag_history`;
                const d = yield this.execute(sql, []);
                if (!d) {
                    resolve(null);
                    return;
                }
                // var data = d as any[];
                const data = this.getRows(d);
                let flag_history = [];
                if (data.length > 0) {
                    data.forEach(element => {
                        let object = {
                            id: element["id"],
                            content: element["content_" + lang],
                            date: element["date_" + lang],
                            sort: element["sort"],
                            tab: element["tab"]
                        };
                        flag_history.push(object);
                    });
                    resolve(flag_history);
                }
                else {
                    resolve(null);
                }
            }));
        });
    }
    getFlagRulesData(lang, key) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
                const sql = `SELECT id,key,content_${lang} FROM flag_rules where key = ?`;
                const d = yield this.execute(sql, [key]);
                if (!d) {
                    resolve(null);
                    return;
                }
                // var data = d as any[];
                const data = this.getRows(d);
                if (data.length > 0) {
                    const flag_rule = {
                        id: data[0].id,
                        content: data[0]["content_" + lang],
                        key: data[0].key
                    };
                    resolve(flag_rule);
                }
                else {
                    resolve(null);
                }
            }));
        });
    }
    getMastData(lang) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
                const sql = `SELECT id,title_${lang},flag_height_${lang},flag_area_${lang},flag_weight_${lang},flag_total_area_${lang},location_${lang},latitude,longitude,image,sort FROM mast`;
                const d = yield this.execute(sql, []);
                if (!d) {
                    resolve(null);
                    return;
                }
                // var data = d as any[];
                const data = this.getRows(d);
                let mast = [];
                if (data.length > 0) {
                    data.forEach(element => {
                        let object = {
                            id: element["id"],
                            title: element["title_" + lang],
                            flag_height: element["flag_height_" + lang],
                            flag_area: element["flag_area_" + lang],
                            flag_weight: element["flag_weight_" + lang],
                            flag_total_area: element["flag_total_area_" + lang],
                            location: element["location_" + lang],
                            latitude: element["latitude"],
                            sort: element["sort"],
                            longitude: element["longitude"],
                            image: element["image"],
                        };
                        mast.push(object);
                    });
                    resolve(mast);
                }
                else {
                    resolve(null);
                }
            }));
        });
    }
    execute(sql, params) {
        return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            return _capacitor_community_sqlite__WEBPACK_IMPORTED_MODULE_3__.CapacitorSQLite.query({ database: SQL_DB_NAME, statement: sql, values: params })
                .then((response) => {
                resolve(response);
            })
                .catch((err) => {
                console.error(err);
                resolve(null);
            });
        }));
    }
    prepareBatch(insertRows) {
        return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            const size = 250;
            const arrayOfArrays = [];
            for (let i = 0; i < insertRows.length; i += size) {
                arrayOfArrays.push(insertRows.slice(i, i + size));
            }
            for (const element of arrayOfArrays) {
                yield this.executeBatch(element);
                // await this.execute(s, p)
            }
            resolve(true);
        }));
    }
    executeBatch(array) {
        return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            if (!this.db) {
                yield this.platform.ready();
                yield this.createDatabase();
            }
            let command = array[0][0];
            if (!command) {
                resolve(null);
            }
            let cmd = command.split('VALUES')[0] + "VALUES ";
            let values = [];
            for (let i = 0; i < array.length; i++) {
                let extractedArray = array[i];
                let brackets = array[i][0].split('VALUES')[1];
                cmd += brackets + (i != array.length - 1 ? ", " : "");
                values = values.concat(array[i][1]);
            }
            // console.log("batch sql cmd: ", cmd);
            // console.log("batch sql cmd 00 : ", values);
            return _capacitor_community_sqlite__WEBPACK_IMPORTED_MODULE_3__.CapacitorSQLite.run({ database: SQL_DB_NAME, statement: cmd, values: values })
                .then((response) => {
                console.log("Response:", { response });
                resolve(response);
            })
                .catch((err) => {
                console.error(err);
                resolve(null);
            });
        }));
    }
    getRows(data) {
        const items = [];
        for (let i = 0; i < data.values.length; i++) {
            const item = data.values[i];
            items.push(item);
        }
        return items;
    }
    getAllRecords(table) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
                const sql = 'SELECT * FROM ' + table;
                const values = [];
                const d = yield this.execute(sql, values);
                if (!d) {
                    resolve([]);
                    return;
                }
                // var data = d as any[];
                const data = this.getRows(d);
                if (data.length > 0) {
                    resolve(data);
                }
                else {
                    resolve([]);
                }
            }));
        });
    }
};
SqliteService.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_7__.Injector },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__.Platform },
    { type: _basic_storage_service__WEBPACK_IMPORTED_MODULE_2__.StorageService },
    { type: _network_service__WEBPACK_IMPORTED_MODULE_0__.NetworkService },
    { type: _utility_service__WEBPACK_IMPORTED_MODULE_1__.UtilityService }
];
SqliteService = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Injectable)({
        providedIn: 'root'
    })
], SqliteService);



/***/ }),

/***/ 70494:
/*!*********************************************!*\
  !*** ./src/app/services/utility.service.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UtilityService": () => (/* binding */ UtilityService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _basic_strings_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./basic/strings.service */ 98601);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic/angular */ 34595);
/* harmony import */ var _basic_alerts_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./basic/alerts.service */ 18119);
/* harmony import */ var _basic_loading_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./basic/loading.service */ 55509);
/* harmony import */ var _basic_storage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./basic/storage.service */ 94274);
/* harmony import */ var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/in-app-browser/ngx */ 38544);
/* harmony import */ var _ionic_native_app_launcher_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/app-launcher/ngx */ 91234);
/* harmony import */ var _ionic_native_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/launch-navigator/ngx */ 86206);










// import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
let UtilityService = class UtilityService {
    constructor(loading, plt, alerts, strings, storage, iab, appLauncher, launchNavigator // private photoViewer: PhotoViewer
    ) {
        this.loading = loading;
        this.plt = plt;
        this.alerts = alerts;
        this.strings = strings;
        this.storage = storage;
        this.iab = iab;
        this.appLauncher = appLauncher;
        this.launchNavigator = launchNavigator;
        this.pdfLink = 'http://thisisvantage.com/saudiflag/public';
    }
    // openImage(link){
    //   this.photoViewer.show(link);
    // }
    openMap(data) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__awaiter)(this, void 0, void 0, function* () {
            // const destination = [data.latitude, data.longitude];
            let destination = data.latitude + ',' + data.longitude;
            if (this.plt.is('ios')) {
                window.open('maps://?q=' + destination, '_system');
            }
            else {
                let label = encodeURI('Label');
                window.open('geo:0,0?q=' + destination + '(' + label + ')', '_system');
            }
            // this.launchNavigator.navigate(destination)
            //   .then(
            //     success => console.log('Launched navigator'),
            //     error => console.log('Error launching navigator', error)
            //   );
            // const env = Capacitor.getPlatform();
            // const options: LaunchNavigatorOptions = {
            //   start: address,
            //   app: env === 'android' ? this.launchNavigator.APP.GOOGLE_MAPS : this.launchNavigator.APP.APPLE_MAPS
            // };
            // this.launchNavigator.navigate(address, options)
            // .then(
            //   success => console.log('Launched navigator'),
            //   error => console.log('Error launching navigator', error)
            // );
            // this.launchNavigator.navigate(address);
            // const options: LaunchNavigatorOptions = {
            //   start: 'London, ON',
            //   app: LaunchNavigator.APPS.UBER
            // }
            // this.launchNavigator.navigate('Toronto, ON', options)
            //   .then(
            //     success => console.log('Launched navigator'),
            //     error => console.log('Error launching navigator', error)
            //   );
        });
    }
    openPdfLink() {
        const url = this.pdfLink + '/rules-info.pdf';
        this.iab.create(url, '_system');
    }
    openPdfFile() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__awaiter)(this, void 0, void 0, function* () { });
    }
    showLoader(msg = 'Please wait...') {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__awaiter)(this, void 0, void 0, function* () {
            return yield this.loading.showLoader(msg);
        });
    }
    hideLoader() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__awaiter)(this, void 0, void 0, function* () {
            return yield this.loading.hideLoader();
        });
    }
    showAlert(msg, title = 'Alert') {
        return this.alerts.showAlert(msg, title);
    }
    presentToast(msg) {
        return this.alerts.presentToast(msg);
    }
    presentSuccessToast(msg, cssClass) {
        return this.alerts.presentSuccessToast(msg, cssClass);
    }
    presentFailureToast(msg, cssClass) {
        return this.alerts.presentFailureToast(msg, cssClass);
    }
    presentConfirm(okText = 'OK', cancelText = 'Cancel', title = 'Are You Sure?', message = '') {
        return this.alerts.presentConfirm((okText = okText), (cancelText = cancelText), (title = title), (message = message));
    }
    presentInput(okText = 'OK', cancelText = 'Cancel', title = 'Are You Sure?', message = '') {
        return this.alerts.presentInput((okText = okText), (cancelText = cancelText), (title = title), (message = message));
    }
    /** Storage Service */
    setKey(key, value) {
        return this.storage.set(key, value);
    }
    getKey(key) {
        return this.storage.get(key);
    }
    /** Strings Service */
    capitalizeEachFirst(str) {
        return this.strings.capitalizeEachFirst(str);
    }
    checkIfMatchingPasswords(passwordKey, passwordConfirmationKey) {
        return this.strings.checkIfMatchingPasswords(passwordKey, passwordConfirmationKey);
    }
};
UtilityService.ctorParameters = () => [
    { type: _basic_loading_service__WEBPACK_IMPORTED_MODULE_2__.LoadingService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__.Platform },
    { type: _basic_alerts_service__WEBPACK_IMPORTED_MODULE_1__.AlertsService },
    { type: _basic_strings_service__WEBPACK_IMPORTED_MODULE_0__.StringsService },
    { type: _basic_storage_service__WEBPACK_IMPORTED_MODULE_3__.StorageService },
    { type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_4__.InAppBrowser },
    { type: _ionic_native_app_launcher_ngx__WEBPACK_IMPORTED_MODULE_5__.AppLauncher },
    { type: _ionic_native_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_6__.LaunchNavigator }
];
UtilityService = (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_9__.Injectable)({
        providedIn: 'root',
    })
], UtilityService);



/***/ }),

/***/ 24766:
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "environment": () => (/* binding */ environment)
/* harmony export */ });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ 8835:
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ 90476);
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app/app.module */ 50023);
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./environments/environment */ 24766);




if (_environments_environment__WEBPACK_IMPORTED_MODULE_1__.environment.production) {
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.enableProdMode)();
}
(0,_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_3__.platformBrowserDynamic)().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_0__.AppModule)
    .catch(err => console.log(err));


/***/ }),

/***/ 50863:
/*!******************************************************************************************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/ lazy ^\.\/.*\.entry\.js$ include: \.entry\.js$ exclude: \.system\.entry\.js$ namespace object ***!
  \******************************************************************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var map = {
	"./ion-action-sheet.entry.js": [
		95261,
		"common",
		"node_modules_ionic_core_dist_esm_ion-action-sheet_entry_js"
	],
	"./ion-alert.entry.js": [
		26,
		"common",
		"node_modules_ionic_core_dist_esm_ion-alert_entry_js"
	],
	"./ion-app_8.entry.js": [
		29009,
		"common",
		"node_modules_ionic_core_dist_esm_ion-app_8_entry_js"
	],
	"./ion-avatar_3.entry.js": [
		27221,
		"common",
		"node_modules_ionic_core_dist_esm_ion-avatar_3_entry_js"
	],
	"./ion-back-button.entry.js": [
		34694,
		"common",
		"node_modules_ionic_core_dist_esm_ion-back-button_entry_js"
	],
	"./ion-backdrop.entry.js": [
		70993,
		"node_modules_ionic_core_dist_esm_ion-backdrop_entry_js"
	],
	"./ion-button_2.entry.js": [
		63645,
		"common",
		"node_modules_ionic_core_dist_esm_ion-button_2_entry_js"
	],
	"./ion-card_5.entry.js": [
		62245,
		"common",
		"node_modules_ionic_core_dist_esm_ion-card_5_entry_js"
	],
	"./ion-checkbox.entry.js": [
		23482,
		"common",
		"node_modules_ionic_core_dist_esm_ion-checkbox_entry_js"
	],
	"./ion-chip.entry.js": [
		4081,
		"common",
		"node_modules_ionic_core_dist_esm_ion-chip_entry_js"
	],
	"./ion-col_3.entry.js": [
		53315,
		"node_modules_ionic_core_dist_esm_ion-col_3_entry_js"
	],
	"./ion-datetime_3.entry.js": [
		64133,
		"common",
		"node_modules_ionic_core_dist_esm_ion-datetime_3_entry_js"
	],
	"./ion-fab_3.entry.js": [
		37542,
		"common",
		"node_modules_ionic_core_dist_esm_ion-fab_3_entry_js"
	],
	"./ion-img.entry.js": [
		21459,
		"node_modules_ionic_core_dist_esm_ion-img_entry_js"
	],
	"./ion-infinite-scroll_2.entry.js": [
		47618,
		"node_modules_ionic_core_dist_esm_ion-infinite-scroll_2_entry_js"
	],
	"./ion-input.entry.js": [
		90101,
		"common",
		"node_modules_ionic_core_dist_esm_ion-input_entry_js"
	],
	"./ion-item-option_3.entry.js": [
		82210,
		"common",
		"node_modules_ionic_core_dist_esm_ion-item-option_3_entry_js"
	],
	"./ion-item_8.entry.js": [
		47370,
		"common",
		"node_modules_ionic_core_dist_esm_ion-item_8_entry_js"
	],
	"./ion-loading.entry.js": [
		23652,
		"common",
		"node_modules_ionic_core_dist_esm_ion-loading_entry_js"
	],
	"./ion-menu_3.entry.js": [
		28220,
		"common",
		"node_modules_ionic_core_dist_esm_ion-menu_3_entry_js"
	],
	"./ion-modal.entry.js": [
		25500,
		"common",
		"node_modules_ionic_core_dist_esm_ion-modal_entry_js"
	],
	"./ion-nav_2.entry.js": [
		84913,
		"common",
		"node_modules_ionic_core_dist_esm_ion-nav_2_entry_js"
	],
	"./ion-popover.entry.js": [
		15078,
		"common",
		"node_modules_ionic_core_dist_esm_ion-popover_entry_js"
	],
	"./ion-progress-bar.entry.js": [
		14857,
		"common",
		"node_modules_ionic_core_dist_esm_ion-progress-bar_entry_js"
	],
	"./ion-radio_2.entry.js": [
		48958,
		"common",
		"node_modules_ionic_core_dist_esm_ion-radio_2_entry_js"
	],
	"./ion-range.entry.js": [
		14383,
		"common",
		"node_modules_ionic_core_dist_esm_ion-range_entry_js"
	],
	"./ion-refresher_2.entry.js": [
		97630,
		"common",
		"node_modules_ionic_core_dist_esm_ion-refresher_2_entry_js"
	],
	"./ion-reorder_2.entry.js": [
		81297,
		"common",
		"node_modules_ionic_core_dist_esm_ion-reorder_2_entry_js"
	],
	"./ion-ripple-effect.entry.js": [
		35492,
		"node_modules_ionic_core_dist_esm_ion-ripple-effect_entry_js"
	],
	"./ion-route_4.entry.js": [
		13752,
		"common",
		"node_modules_ionic_core_dist_esm_ion-route_4_entry_js"
	],
	"./ion-searchbar.entry.js": [
		7487,
		"common",
		"node_modules_ionic_core_dist_esm_ion-searchbar_entry_js"
	],
	"./ion-segment_2.entry.js": [
		61778,
		"common",
		"node_modules_ionic_core_dist_esm_ion-segment_2_entry_js"
	],
	"./ion-select_3.entry.js": [
		82904,
		"common",
		"node_modules_ionic_core_dist_esm_ion-select_3_entry_js"
	],
	"./ion-slide_2.entry.js": [
		81609,
		"node_modules_ionic_core_dist_esm_ion-slide_2_entry_js"
	],
	"./ion-spinner.entry.js": [
		31218,
		"common",
		"node_modules_ionic_core_dist_esm_ion-spinner_entry_js"
	],
	"./ion-split-pane.entry.js": [
		92849,
		"node_modules_ionic_core_dist_esm_ion-split-pane_entry_js"
	],
	"./ion-tab-bar_2.entry.js": [
		4127,
		"common",
		"node_modules_ionic_core_dist_esm_ion-tab-bar_2_entry_js"
	],
	"./ion-tab_2.entry.js": [
		28400,
		"common",
		"node_modules_ionic_core_dist_esm_ion-tab_2_entry_js"
	],
	"./ion-text.entry.js": [
		14397,
		"common",
		"node_modules_ionic_core_dist_esm_ion-text_entry_js"
	],
	"./ion-textarea.entry.js": [
		43391,
		"common",
		"node_modules_ionic_core_dist_esm_ion-textarea_entry_js"
	],
	"./ion-toast.entry.js": [
		66793,
		"common",
		"node_modules_ionic_core_dist_esm_ion-toast_entry_js"
	],
	"./ion-toggle.entry.js": [
		11695,
		"common",
		"node_modules_ionic_core_dist_esm_ion-toggle_entry_js"
	],
	"./ion-virtual-scroll.entry.js": [
		4180,
		"node_modules_ionic_core_dist_esm_ion-virtual-scroll_entry_js"
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(() => {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(() => {
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = () => (Object.keys(map));
webpackAsyncContext.id = 50863;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 43069:
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("* {\n  background: url('bg.png') 0 0/100% 100% no-repeat;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGlEQUFBO0FBQ0oiLCJmaWxlIjoiYXBwLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiKiB7XHJcbiAgICBiYWNrZ3JvdW5kOiB1cmwoXCIuLy4uL2Fzc2V0cy9pbWdzL2JnLnBuZ1wiKSAwIDAvMTAwJSAxMDAlIG5vLXJlcGVhdDtcclxufVxyXG5cclxuLy8gLmJnIHtcclxuLy8gICAgIGJhY2tncm91bmQtY29sb3I6IHJnYigxNTAsMTg1LDE2Nik7XHJcbi8vIH0iXX0= */");

/***/ }),

/***/ 91106:
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-app class=\"bg\" [style]=\"{'direction': dir }\">\r\n  <ion-router-outlet></ion-router-outlet>\r\n</ion-app>");

/***/ }),

/***/ 69069:
/*!*******************************************!*\
  !*** ./src/assets/json/flag_history.json ***!
  \*******************************************/
/***/ ((module) => {

"use strict";
module.exports = JSON.parse('{"data":[{"id":"1","date_en":"1744 – 1902","date_ar":"١١٥٧هـ  -  ١٣١٩هـ","tab":"1","content_en":"<p class=\\"p1\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 13px; line-height: normal; font-family: Helvetica; color: #000000;\\">\\r\\n    <span style=\\"color: #339966;\\">Green flag with a narrow white strip at the side, and the Islamic profession of faith\\r\\n        &ldquo;There is no God but Allah and Muhammad is his messenger&rdquo; in the middle.</span></p>\\r\\n<p class=\\"p2\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 13px; line-height: normal; font-family: Helvetica; color: #000000; min-height: 16px;\\">\\r\\n    &nbsp;</p>\\r\\n<p class=\\"p1\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 13px; line-height: normal; font-family: Helvetica; color: #000000;\\">\\r\\n    <p class=\\"date\\">1744</p></p>\\r\\n<p class=\\"p1\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 13px; line-height: normal; font-family: Helvetica; color: #000000;\\">\\r\\n    <span style=\\"color: #339966;\\">The first Saudi State used the flag to be an extension of Islamic History.</span></p>\\r\\n<p class=\\"p2\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 13px; line-height: normal; font-family: Helvetica; color: #000000; min-height: 16px;\\">\\r\\n    &nbsp;</p>\\r\\n<p class=\\"p1\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 13px; line-height: normal; font-family: Helvetica; color: #000000;\\">\\r\\n    <p class=\\"date\\">1818</p></p>\\r\\n<p class=\\"p1\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 13px; line-height: normal; font-family: Helvetica; color: #000000;\\">\\r\\n    <span style=\\"color: #339966;\\">The second Saudi State continued to use the same flag.</span></p>","content_ar":"<p class=\\"p1\\" dir=\\"rtl\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 18px; line-height: normal; font-family: \'Times New Roman\'; color: #000000;\\">\\r\\n    <span style=\\"color: #339966;\\">الراية خضراء وجــانبها أبيض </span><span style=\\"color: #339966;\\">ويتوسطها عبارة لا إله\\r\\n        إلا الله<span class=\\"Apple-converted-space\\">&nbsp;</span></span><span style=\\"color: #339966;\\">محمد رسول\\r\\n        الل</span></p>\\r\\n<p class=\\"p2\\"\\r\\n    style=\\"margin: 0px; text-align: right; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 18px; line-height: normal; font-family: \'Times New Roman\'; color: #000000; min-height: 21px;\\">\\r\\n    &nbsp;</p>\\r\\n<p class=\\"p1\\"\\r\\n    style=\\"margin: 0px; text-align: right; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 18px; line-height: normal; font-family: \'Times New Roman\'; color: #000000;\\">\\r\\n    <p class=\\"date\\">١١٥٧هـ&nbsp; - <span class=\\"s1\\"\\r\\n            style=\\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Helvetica;\\"><span\\r\\n                style=\\"font-family: \'Times New Roman\';\\">١٧٤٤م</span><br /></span></p><span class=\\"s1\\"\\r\\n        style=\\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Helvetica;\\"><br /></span><span\\r\\n        style=\\"color: #339966;\\">اتخذت الدولة السعودية الأولى الراية امتداداً للتاريخ الإسلامي</span><span class=\\"s1\\"\\r\\n        style=\\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Helvetica;\\"><br /></span>\\r\\n</p>\\r\\n<p class=\\"p1\\"\\r\\n    style=\\"margin: 0px; text-align: right; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 18px; line-height: normal; font-family: \'Times New Roman\'; color: #000000;\\">\\r\\n    <p class=\\"date\\">١٢٤٠ هـ&nbsp; - ١٨١٨م</p></p>\\r\\n<p class=\\"p1\\"\\r\\n    style=\\"margin: 0px; text-align: right; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 18px; line-height: normal; font-family: \'Times New Roman\'; color: #000000;\\">\\r\\n    <span class=\\"s1\\"\\r\\n        style=\\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Helvetica;\\"><br /></span><span\\r\\n        style=\\"color: #339966;\\">اســتـمــرت الدولــة السـعـودية الثانية في اتخاذ الراية نفسها</span><span class=\\"s1\\"\\r\\n        style=\\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: Helvetica;\\"><br /></span>\\r\\n</p>","sort":"1"},{"id":"2","date_en":"1902 – 1932","date_ar":"١٣١٩هـ - ١٣٥١ هـ","tab":"1","content_en":"<p class=\\"p1\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 13px; line-height: normal; font-family: Helvetica; color: #000000;\\">\\r\\n    Two crossed swords were added to the flag</p>\\r\\n<p class=\\"p2\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 13px; line-height: normal; font-family: Helvetica; color: #000000; min-height: 16px;\\">\\r\\n    &nbsp;</p>\\r\\n<p class=\\"p1\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 13px; line-height: normal; font-family: Helvetica; color: #000000;\\">\\r\\n    <p class=\\"date\\">1902</p></p>\\r\\n<p class=\\"p1\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 13px; line-height: normal; font-family: Helvetica; color: #000000;\\">\\r\\n    King Abdulaziz continued to use the same flag too.</p>\\r\\n<p class=\\"p2\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 13px; line-height: normal; font-family: Helvetica; color: #000000; min-height: 16px;\\">\\r\\n    &nbsp;</p>\\r\\n<p class=\\"p1\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 13px; line-height: normal; font-family: Helvetica; color: #000000;\\">\\r\\n    <p class=\\"date\\">1926</p></p>\\r\\n<p class=\\"p1\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 13px; line-height: normal; font-family: Helvetica; color: #000000;\\">\\r\\n    King Abdulaziz ordered the constituent authority to draft a new flag.</p>\\r\\n<p class=\\"p1\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 13px; line-height: normal; font-family: Helvetica; color: #000000;\\">\\r\\n    &nbsp;</p>\\r\\n<p class=\\"p2\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 13px; line-height: normal; font-family: Helvetica; color: #000000;\\">\\r\\n    <p class=\\"date\\">1937</p></p>\\r\\n<p class=\\"p2\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 13px; line-height: normal; font-family: Helvetica; color: #000000;\\">\\r\\n    Decision No.354 of the Consultative Assembly, to determine the size of the flag with 150 cm length and 100 cm width.\\r\\n</p>\\r\\n<p class=\\"p2\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 13px; line-height: normal; font-family: Helvetica; color: #000000;\\">\\r\\n    &nbsp;</p>\\r\\n<p class=\\"p2\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 13px; line-height: normal; font-family: Helvetica; color: #000000;\\">\\r\\n    <p class=\\"date\\">1937</p></p>\\r\\n<p class=\\"p2\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 13px; line-height: normal; font-family: Helvetica; color: #000000;\\">\\r\\n    Decision No. 50 of the Consultative Assembly, regarding the national flag and determining the Flag of the King,\\r\\n    Crown Prince, Army and Aviation, Internal Flag, Royal Saudi Navy Flag and Commercial Navy Flag.</p>\\r\\n<p class=\\"p1\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 13px; line-height: normal; font-family: Helvetica; color: #000000;\\">\\r\\n    &nbsp;</p>\\r\\n<p class=\\"p1\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 13px; line-height: normal; font-family: Helvetica; color: #000000;\\">\\r\\n    <p class=\\"date\\">1952</p></p>\\r\\n<p class=\\"p1\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 13px; line-height: normal; font-family: Helvetica; color: #000000;\\">\\r\\n    Decision No. 69 of the Consultative Assembly, regarding the sizes and amendments of flags.</p>\\r\\n<p class=\\"p1\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 13px; line-height: normal; font-family: Helvetica; color: #000000;\\">\\r\\n    &nbsp;</p>\\r\\n<p class=\\"p1\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 13px; line-height: normal; font-family: Helvetica; color: #000000;\\">\\r\\n    <p class=\\"date\\">1973</p></p>\\r\\n<p class=\\"p1\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 13px; line-height: normal; font-family: Helvetica; color: #000000;\\">\\r\\n    Decision No. 101 of the Council Ministers, regarding issuing the National Flag Law.</p>\\r\\n<p class=\\"p1\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 13px; line-height: normal; font-family: Helvetica; color: #000000;\\">\\r\\n    &nbsp;</p>\\r\\n<p class=\\"p1\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 13px; line-height: normal; font-family: Helvetica; color: #000000;\\">\\r\\n    <p class=\\"date\\">1978</p></p>\\r\\n<p class=\\"p1\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 13px; line-height: normal; font-family: Helvetica; color: #000000;\\">\\r\\n    Decision No.422 of the Council Ministers, regarding seven articles, indication the Executive Regulations of the\\r\\n    National Flag Law.</p>\\r\\n<p class=\\"p1\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 13px; line-height: normal; font-family: Helvetica; color: #000000;\\">\\r\\n    &nbsp;</p>\\r\\n<p class=\\"p1\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 13px; line-height: normal; font-family: Helvetica; color: #000000;\\">\\r\\n    <p class=\\"date\\">1987</p></p>\\r\\n<p class=\\"p1\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 13px; line-height: normal; font-family: Helvetica; color: #000000;\\">\\r\\n    Decision No. 7 of the HE Minister of Interior, regarding the Standard Specifications for National Flag.</p>","content_ar":"<p class=\\"p1\\" dir=\\"rtl\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 18px; line-height: normal; font-family: \'Times New Roman\'; color: #000000;\\">\\r\\n    <span style=\\"color: #000000; font-family: Times New Roman;\\"><span style=\\"font-size: 18px;\\">اضيف سيفان متقاطعان على\\r\\n            العلم</span></span></p>\\r\\n<p class=\\"p1\\" dir=\\"rtl\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 18px; line-height: normal; font-family: \'Times New Roman\'; color: #000000;\\">\\r\\n    <span style=\\"color: #000000; font-family: Times New Roman;\\"><span style=\\"font-size: 18px;\\">&nbsp;</span></span><span\\r\\n        style=\\"color: #000000; font-family: Times New Roman;\\"><span style=\\"font-size: 18px;\\">&nbsp;</span></span></p>\\r\\n<p class=\\"p1\\" dir=\\"rtl\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 18px; line-height: normal; font-family: \'Times New Roman\'; color: #000000;\\">\\r\\n    <p class=\\"date\\"><span style=\\"color: #000000; font-family: Times New Roman;\\"><span style=\\"font-size: 18px;\\">١٣١٩هـ ـ\\r\\n                ١٩٠٢م</span></span></p></p>\\r\\n<p class=\\"p1\\" dir=\\"rtl\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 18px; line-height: normal; font-family: \'Times New Roman\'; color: #000000;\\">\\r\\n    <span style=\\"color: #000000; font-family: Times New Roman;\\"><span style=\\"font-size: 18px;\\">استمر الملك عبدالعزيز في\\r\\n            اتخاذ الراية نفسها أيضا</span></span></p>\\r\\n<p class=\\"p1\\" dir=\\"rtl\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 18px; line-height: normal; font-family: \'Times New Roman\'; color: #000000;\\">\\r\\n    <span style=\\"color: #000000; font-family: Times New Roman;\\"><span style=\\"font-size: 18px;\\">&nbsp;</span></span></p>\\r\\n<p class=\\"p1\\" dir=\\"rtl\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 18px; line-height: normal; font-family: \'Times New Roman\'; color: #000000;\\">\\r\\n    <p class=\\"date\\"><span style=\\"color: #000000; font-family: Times New Roman;\\"><span style=\\"font-size: 18px;\\">١٣٤٤هـ ـ\\r\\n                ١٩٢٦م</span></span></p></p>\\r\\n<p class=\\"p1\\" dir=\\"rtl\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 18px; line-height: normal; font-family: \'Times New Roman\'; color: #000000;\\">\\r\\n    <span style=\\"color: #000000; font-family: Times New Roman;\\"><span style=\\"font-size: 18px;\\">أمــر المـلك\\r\\n            عــبدالعـزيــز الهــيـئــة التأسيسية بصياغة شكل علم جديد</span></span></p>\\r\\n<p class=\\"p1\\" dir=\\"rtl\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 18px; line-height: normal; font-family: \'Times New Roman\'; color: #000000;\\">\\r\\n    <span style=\\"color: #000000; font-family: Times New Roman;\\"><span style=\\"font-size: 18px;\\">&nbsp;</span></span></p>\\r\\n<p class=\\"p1\\" dir=\\"rtl\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 18px; line-height: normal; font-family: \'Times New Roman\'; color: #000000;\\">\\r\\n    <p class=\\"date\\"><span style=\\"color: #000000; font-family: Times New Roman;\\"><span style=\\"font-size: 18px;\\">١٣٥٥هـ ـ\\r\\n                ١٩٣٧م</span></span></p></p>\\r\\n<p class=\\"p1\\" dir=\\"rtl\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 18px; line-height: normal; font-family: \'Times New Roman\'; color: #000000;\\">\\r\\n    <span style=\\"color: #000000; font-family: Times New Roman;\\"><span style=\\"font-size: 18px;\\">٣٥٤&nbsp; قرار مجلس\\r\\n            الشورى رقم&nbsp; إقرار مقاس العلم على نسبة ١٥٠سم طولاً و ١٠٠ سم عرضا</span></span></p>\\r\\n<p class=\\"p1\\" dir=\\"rtl\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 18px; line-height: normal; font-family: \'Times New Roman\'; color: #000000;\\">\\r\\n    <span style=\\"color: #000000; font-family: Times New Roman;\\"><span style=\\"font-size: 18px;\\">&nbsp;</span></span></p>\\r\\n<p class=\\"p1\\" dir=\\"rtl\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 18px; line-height: normal; font-family: \'Times New Roman\'; color: #000000;\\">\\r\\n    <p class=\\"date\\"><span style=\\"color: #000000; font-family: Times New Roman;\\"><span style=\\"font-size: 18px;\\">١٣٥٦هـ ـ\\r\\n                ١٩٣٧م</span></span></p></p>\\r\\n<p class=\\"p1\\" dir=\\"rtl\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 18px; line-height: normal; font-family: \'Times New Roman\'; color: #000000;\\">\\r\\n    <span style=\\"color: #000000; font-family: Times New Roman;\\"><span style=\\"font-size: 18px;\\">٥٠&nbsp; قرار مجلس الشورى\\r\\n            رقم&nbsp; بشأن العلم الوطني وتخصيص علم الملك وولي العهد والجيش والطيران والعلم الداخلي والعلم البحري الملكي\\r\\n            السعودي والعلم البحري التجاري</span></span></p>\\r\\n<p class=\\"p1\\" dir=\\"rtl\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 18px; line-height: normal; font-family: \'Times New Roman\'; color: #000000;\\">\\r\\n    <span style=\\"color: #000000; font-family: Times New Roman;\\"><span style=\\"font-size: 18px;\\">&nbsp;</span></span></p>\\r\\n<p class=\\"p1\\" dir=\\"rtl\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 18px; line-height: normal; font-family: \'Times New Roman\'; color: #000000;\\">\\r\\n    <p class=\\"date\\"><span style=\\"color: #000000; font-family: Times New Roman;\\"><span style=\\"font-size: 18px;\\">١٣٩٣هـ ـ\\r\\n                ١٩٧٣م</span></span></p></p>\\r\\n<p class=\\"p1\\" dir=\\"rtl\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 18px; line-height: normal; font-family: \'Times New Roman\'; color: #000000;\\">\\r\\n    <span style=\\"color: #000000; font-family: Times New Roman;\\"><span style=\\"font-size: 18px;\\">قرار مجلس الوزراء رقم ١٠١\\r\\n            بصدور نظام العلم الوطني&nbsp;</span></span></p>\\r\\n<p class=\\"p1\\" dir=\\"rtl\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 18px; line-height: normal; font-family: \'Times New Roman\'; color: #000000;\\">\\r\\n    <span style=\\"color: #000000; font-family: Times New Roman;\\"><span style=\\"font-size: 18px;\\">&nbsp;</span></span></p>\\r\\n<p class=\\"p1\\" dir=\\"rtl\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 18px; line-height: normal; font-family: \'Times New Roman\'; color: #000000;\\">\\r\\n    <p class=\\"date\\"><span style=\\"color: #000000; font-family: Times New Roman;\\"><span style=\\"font-size: 18px;\\">١٣٩٨هـ ـ\\r\\n                ١٩٧٨م</span></span></p></p>\\r\\n<p class=\\"p1\\" dir=\\"rtl\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 18px; line-height: normal; font-family: \'Times New Roman\'; color: #000000;\\">\\r\\n    <span style=\\"color: #000000; font-family: Times New Roman;\\"><span style=\\"font-size: 18px;\\">اللائحة التنظيمية لنظام\\r\\n            العلم الوطني بقرار مجلس الوزراء رقم ٤٢٢ في ٧ مواد</span></span></p>\\r\\n<p class=\\"p1\\" dir=\\"rtl\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 18px; line-height: normal; font-family: \'Times New Roman\'; color: #000000;\\">\\r\\n    <span style=\\"color: #000000; font-family: Times New Roman;\\"><span style=\\"font-size: 18px;\\">&nbsp;</span></span></p>\\r\\n<p class=\\"p1\\" dir=\\"rtl\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 18px; line-height: normal; font-family: \'Times New Roman\'; color: #000000;\\">\\r\\n    <p class=\\"date\\"><span style=\\"color: #000000; font-family: Times New Roman;\\"><span style=\\"font-size: 18px;\\">١٤٠٧هـ ـ\\r\\n                ١٩٨٧م</span></span></p></p>\\r\\n<p class=\\"p1\\" dir=\\"rtl\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 18px; line-height: normal; font-family: \'Times New Roman\'; color: #000000;\\">\\r\\n    <span style=\\"color: #000000; font-family: Times New Roman;\\"><span style=\\"font-size: 18px;\\">قرار سمو وزير الداخلية\\r\\n            رقم ٧ بشأن المواصفات القياسية للعلم الوطني</span></span></p>\\r\\n<p class=\\"p1\\" dir=\\"rtl\\"\\r\\n    style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 18px; line-height: normal; font-family: \'Times New Roman\'; color: #000000;\\">\\r\\n    &nbsp;</p>","sort":"2"},{"id":"3","date_en":"1932 – Present","date_ar":"١٣٥١هـ - حاضر","tab":"1","content_en":"<p class=\\"p1\\" style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 13px; line-height: normal; font-family: Helvetica; color: #000000;\\">The flag is fully green and bears the Islamic profession of faith with unsheathed sword under.</p>","content_ar":"<p class=\\"p2\\" dir=\\"rtl\\" style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 18px; line-height: normal; font-family: \'Times New Roman\'; color: #000000;\\">الرايــة خضـــراء كلها وبهــا عــبارة لا إله إلا الله محــمــد رسول الله،<span class=\\"Apple-converted-space\\">&nbsp;</span>وتحتهـا سيف مسلول</p>","sort":"3"},{"id":"4","date_en":"1902 – 1932","date_ar":"١٣١٩هـ - ١٣٥١ هـ","tab":"2","content_en":"<p class=\\"p1\\" style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 13px; line-height: normal; font-family: Helvetica; color: #000000;\\">The two crossed swords were replaced with one unsheathed sword above</p>","content_ar":"<p>&nbsp;</p>\\r\\n<p class=\\"p2\\" dir=\\"rtl\\" style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 18px; line-height: normal; font-family: \'Times New Roman\'; color: #000000;\\">استبدال السيفان المتقاطعان&nbsp;بسيف مسلول في الأعلى</p>\\r\\n<p class=\\"p3\\" style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 18px; line-height: normal; font-family: Helvetica; color: #000000; min-height: 22px;\\">&nbsp;</p>","sort":"2"},{"id":"5","date_en":"1902 – 1932","date_ar":"١٣١٩هـ - ١٣٥١ هـ","tab":"3","content_en":"<p class=\\"p1\\" style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 13px; line-height: normal; font-family: Helvetica; color: #000000;\\"><span style=\\"color: #339966;\\">The sword was replaced with one under the Islamic procession of Faith, while a Quranic phrase of &ldquo;Help from Allah and an Imminent victory&rdquo; was added.</span></p>","content_ar":"<p class=\\"p1\\" dir=\\"rtl\\" style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 18px; line-height: normal; font-family: \'Times New Roman\'; color: #000000;\\"><span style=\\"color: #339966;\\">وضع السيف تحت عبارة لا إله إلا الله<span class=\\"Apple-converted-space\\">&nbsp;</span></span><span style=\\"color: #339966;\\">محــمد رســول الله وأضيف قوله تعالى&nbsp;</span><span style=\\"color: #339966;\\">(نصر من الله وفتح قريب)</span></p>\\r\\n<p class=\\"p2\\" style=\\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 18px; line-height: normal; font-family: Helvetica; color: #000000; min-height: 22px;\\">&nbsp;</p>","sort":"2"}]}');

/***/ }),

/***/ 6715:
/*!*****************************************!*\
  !*** ./src/assets/json/flag_rules.json ***!
  \*****************************************/
/***/ ((module) => {

"use strict";
module.exports = JSON.parse('{"data":[{"id":"1","key":"flag_raising_times","content_en":"<div> <img src=\\"assets/gifs/sun1.gif\\"/> </div>\\r\\n<p>The flag is to be raised, from sunrise to sunset during all weekends and feasts, on all roofs of government buildings and scientific institutions</p>","content_ar":"<div> <img src=\\"assets/gifs/sun1.gif\\"/> </div>\\r\\n<p>يرفع العلم من شروق الشمس إلى غروبها في أيام الإجازات الأسبوعية&nbsp;</p>\\r\\n<p>والأعياد على جميع المـباني الحكومية والمؤسسات التعليمية</p>\\r\\n<p>&nbsp;</p>"},{"id":"2","key":"royal_flag","content_en":"<div> <img src=\\"assets/gifs/flagemblem.gif\\"/> </div>\\r\\n<p>In the lower corner next to the flagpole there is gilded silk yarns forming the state’s motto “two swords crossed under a palm tree”</p>","content_ar":"<div> <img src=\\"assets/gifs/flagemblem.gif\\"/> </div>\\r\\n<p>يحتوي بالزاوية الدنيا المجاورة لسارة العلم على خيوط حريرية مذهبة تمثل شعار الدولة السيفان المتقاطعان والنخلة</p>"},{"id":"3","key":"flag_sizes","content_en":"<div> <img src=\\"assets/gifs/rule1.gif\\"/> </div>\\r\\n<p><strong>Parade Flag</strong><br>50 X 100 cm</p>\\r\\n<div><img src=\\"assets/gifs/rule3.gif\\"/> </div>\\r\\n<p><strong>Car Flag</strong><br>30 X 30 cm</p>\\r\\n<div> <img src=\\"assets/gifs/rule2.gif\\"/> </div>\\r\\n<p><strong>Office Flag</strong><br>8 X 16 cm</p>\\r\\n<div> <img src=\\"assets/gifs/rule4.gif\\"/> </div>\\r\\n<p>The height of the pole shall be 6m at least in public square and 3m at least on the roof of buildings, while the width shall be 80cm at least in both.</p>","content_ar":"<div> <img src=\\"assets/gifs/rule1.gif\\"/> </div>\\r\\n<p><strong>علم الإستعراض</strong><br> سم ١٠٠ x ٥٠ </p>\\r\\n<div><img src=\\"assets/gifs/rule3.gif\\"/> </div>\\r\\n<p><strong>علم السيارة</strong><br> سم ٣٠  x ٣٠ </p>\\r\\n<div> <img src=\\"assets/gifs/rule2.gif\\"/> </div>\\r\\n<p><strong>علم المكتب</strong><br> سم ١٦ x ٨ </p>\\r\\n<div> <img src=\\"assets/gifs/rule4.gif\\"/> </div>\\r\\n<p> لا يقل ارتفاع السارية في الميدان العام عن ٦ ٣ أمتار و لايقل ارتغاعه عن أمتار فوق المباني كما لا يقل العرض عن ٨٠ سم لكلاهما </p>"},{"id":"4","key":"flag_status","content_en":"<div> <img src=\\"assets/gifs/torn.gif\\"/> </div>\\r\\n<p>The flag shall never be raised pale or in bad condition</p>","content_ar":"<div> <img src=\\"assets/gifs/torn.gif\\"/> </div>\\r\\n<p> لا يرفع العلم باهت اللون أو في حالة سيئة </p>"},{"id":"5","key":"position_in_odd","content_en":"<div> <img src=\\"assets/gifs/multiple-flags.gif\\"/> </div>\\r\\n<p>The National Flag is to be in the middle of flags odd numbers</p>","content_ar":"<div> <img src=\\"assets/gifs/multiple-flags.gif\\"/> </div>\\r\\n<p>يكون وسط أعلام فردية العدد وأن لا يعلو عليه علم أخر</p>"},{"id":"6","key":"position_in_even","content_en":"<div> <img src=\\"assets/gifs/building.gif\\"/> </div>\\r\\n<p>In the middle of the right of the flags even numbers</p>","content_ar":"<div> <img src=\\"assets/gifs/building.gif\\"/> </div>\\r\\n<p> يكون في الوسط ناحية اليمين للأعلام الزوجية العدد </p>"},{"id":"7","key":"penalties_legislation","content_en":"<div> <img src=\\"assets/gifs/order.gif\\"/> </div>\\r\\n<p> In case of any violation to the National Flag, sanctions and legislation shall be applied </p>","content_ar":"<div> <img src=\\"assets/gifs/order.gif\\"/> </div>\\r\\n<p> في حال التعرض للعلم الوطني تطبق العقوبات والتشريعات حيال ذلك </p>"},{"id":"8","key":"flag_raising_condition","content_en":"<div> <img src=\\"assets/gifs/mid-flag.gif\\"/> </div>\\r\\n<p> The flag shall never be at half mast, and shall never touch the surfaces of ground and water </p>","content_ar":"<div> <img src=\\"assets/gifs/mid-flag.gif\\"/> </div>\\r\\n<p> لا ينكس العلم أبدا ولا يتم انزاله الى نصف السارية ولا يلامس سطحي اللأرض والماء </p>"}]}');

/***/ }),

/***/ 62931:
/*!***********************************!*\
  !*** ./src/assets/json/mast.json ***!
  \***********************************/
/***/ ((module) => {

"use strict";
module.exports = JSON.parse('{"data":[{"id":"1","title_en":"Dir’iya`s Mast","flag_height_en":"100  m","title_ar":"سارية الدرعية","flag_height_ar":"٠ ٠ ١  متر","flag_area_en":"540  m2","flag_weight_en":"120  kg","flag_total_area_en":"9500 m2","flag_area_ar":"٥٤٠   متر مربع","flag_weight_ar":"٠ ٢ ١  كيلوغرام","flag_total_area_ar":"٠٠ ٥ ٩ متر مربع","location_en":"King Salman square in the Hostorial Dir’iya","location_ar":"تقع في ميدان الملك سلمان في الدرعية التاريخية","latitude":"24.73660159164463","longitude":"46.58461459754791","image":"pic1-app","sort":"1"},{"id":"2","title_en":"Jeddah`s Mast","flag_height_en":"171.4  m","title_ar":"سارية جدة","flag_height_ar":"١ ٧ ١. ٤  متر","flag_area_en":"1,635 m2","flag_weight_en":"570 kg","flag_total_area_en":"26,000 m2","flag_area_ar":"٥ ٣ ٦ ١  متر مربع","flag_weight_ar":"٥٧٠ كيلوغرام","flag_total_area_ar":"٢٦٠٠٠ متر مربع","location_en":"King Abdullah square in Jeddah","location_ar":"تقع في ميدان الملك عبدالله في جدة","latitude":"21.50787147572624","longitude":"39.169732965564826","image":"pic2-app","sort":"2"}]}');

/***/ }),

/***/ 25578:
/*!*****************************************!*\
  !*** ./src/assets/json/milestones.json ***!
  \*****************************************/
/***/ ((module) => {

"use strict";
module.exports = JSON.parse('{"data":[{"id":"1","created_at":"2021-08-08 23:53:36","updated_at":"2021-08-08 23:53:36","deleted_at":null,"title_en":"Recapturing AR RIYAD","date_en":"1902 (1319H)","content_en":"This marks the beginning of uniting the Kingdom of Saudi Arabia. In 1902G, King AbdulAziz bin AbdulRahman Al-Faysal Al-Saud, along with 63 of his troops, entered AR RIYAD, attacked Al-Masmak Fortress and retook AR RIYAD; thus declaring the start of the rule of King AbdulAziz and the foundation of the Kingdom of Saudi Arabia","title_ar":"استرداد الرياض","date_ar":"١٣١٩هـ)  ١٩٠٢(","content_ar":"بداية مرحلة توحيد المملكة العربية السعودية حيث شهدت مدينة الرياض في عام ١٣١٩هـ/١٩٠٢م قيام الملك عبدالعزيز بن عبدالرحمن الفيصل ال سعود مع ٦٣ من رجاله بدخول الرياض ومهاجمة حصن المصمك واسترداد الرياض واعلان بداية حكم الملك عبدالعزيز وانطلاق تأسيس المملكة."},{"id":"2","created_at":"2021-08-08 23:54:35","updated_at":"2021-08-08 23:54:35","deleted_at":null,"title_en":"Battle of AD DILAM","date_en":"1902 (1320H)","content_en":"This battle took place in 1902, when the forces of Ibn Rashid attempted to retake AR RIYAD from King AbdulAziz; thus, the Battle of Ad-Dilam broke out in Al-Kharj and King AbdulAziz was victorious in it; thus proving this dominance and the return of the Saudi State.","title_ar":"معركة الدلم","date_ar":"١٣٢٠هـ)  ١٩٠٢(","content_ar":"حدثت هذه المعركة في عام ١٣٢٠هـ/١٩٠٢ م عندما حاول ابن رشيد استرداد الرياض من الملك عبدالعزيز فحدثت المعركة في مدينة الدلم بالخرج وانتصر فيها الملك عبدالعزيز مثبتاً بذلك نفوذه القوي وعودة الدولة السعودية"},{"id":"3","created_at":"2021-08-08 23:55:25","updated_at":"2021-08-08 23:55:25","deleted_at":null,"title_en":"Battle of FAYDAT AS SIRR","date_en":"1904 (1321H)","content_en":"King AbdulAziz was victorious over one of Ibn Rashid expeditions in FAYDAT AS SIRR, located east of AdDuwadmy.","title_ar":"معركة فيضة السر","date_ar":"١٣٢١هـ","content_ar":"معركة انتصر فيها الملك عبدالعزيز على إحدى حملات ابن رشيد في فيضة السر الواقعة شرقي الدوادمي."},{"id":"4","created_at":"2021-08-08 23:56:11","updated_at":"2021-08-08 23:56:11","deleted_at":null,"title_en":"Entering UNAYZAH","date_en":"1904 (1322H)","content_en":"It was part of the expeditions aimed at unifying the state, as King AbdulAziz entered UNAYZAH peacefully.","title_ar":"عنيزة","date_ar":"١٣٢٢هـ","content_ar":"إحدى حملات توحيد البلاد حيث وصل الملك عبالعزيز إلى عنيزة فدخلها سلماً."},{"id":"5","created_at":"2021-08-08 23:57:06","updated_at":"2021-08-08 23:57:06","deleted_at":null,"title_en":"Entering BURAYDAH","date_en":"1904 (1322H)","content_en":"It was part of the expeditions aimed at unifying the state, as King AbdulAziz headed towards AL QASIM province to unify it within the fold of the State. Upon reaching BURAYDAH, and with the support of some of its men, the city gates were opened and King Abdul-Aziz entered it with no fighting.","title_ar":"بريدة","date_ar":"١٣٢٢هـ","content_ar":"إحدى حملات توحيد البلاد حيث توجه الملك عبدالعزيز إلى القصيم لتوحيده ضمن البلاد وعند وصوله إلى بريدة وبمساندة أحد رجالاتها تم فتح بوابة المدينة ودخلها الملك عبدالعزيز دون معركة."},{"id":"6","created_at":"2021-08-08 23:58:03","updated_at":"2021-08-08 23:58:03","deleted_at":null,"title_en":"Battle of AL BUKAYRIYAH","date_en":"1904 (1322H)","content_en":"In that battle, King AbdulAziz confronted the troops of Ibn Rashid as well as the Ottoman forces backing them. King AbdulAziz implemented a well-drawn plan, through which he managed to defeat Ibn Rashid and the Ottoman forces.","title_ar":"معركة البكيرية","date_ar":"١٣٢٢هـ","content_ar":"واجه فيها الملك عبدالعزيز بقواته جيش ابن رشيد والقوات العثمانية المساندة له وطبق فيها الملك عبدالعزيز خطة محكمة تمكن من خلالها من هزيمة ابن رشيد والقوات العثمانية."},{"id":"7","created_at":"2021-08-08 23:58:53","updated_at":"2021-08-08 23:58:53","deleted_at":null,"title_en":"Battle of ASH SHINANAH","date_en":"1904 (1322H)","content_en":"In that battle, King AbdulAziz confronted the troops of Ibn Rashid as well as the Ottoman forces supporting them. The battle was decisive and ended with the defeat of Ibn Rashid and reinforcing the unification of the State as well as the defeat and escape of the Ottoman forces.","title_ar":"معركة الشنانة","date_ar":"١٣٢٢هـ","content_ar":"واجه فيها الملك عبدالعزيز قوات ابن رشيد والقوات العثمانية المساندة له وكانت حاسمة بهزيمة ابن رشيد وتعزيز توحيد البلاد وفرار القوات العثمانية وهزيمتها."},{"id":"8","created_at":"2021-08-08 23:59:39","updated_at":"2021-08-08 23:59:39","deleted_at":null,"title_en":"Battle of RAWDAT MUHANNA","date_en":"1906 (1324H)","content_en":"It was a decisive battle that brought an end to the presence of the Ottoman forces backing Ibn Rashid. King AbdulAziz defeated Ibn Rashid who was killed in the battle. The Ottoman forces surrendered and handed over the canons and ammunition and were driven out of AL QASIM.","title_ar":"معركة روضة مهنا","date_ar":"١٣٢٤هـ","content_ar":"معركة حاسمة أنهت تواجد القوات العثمانية المساندة لابن رشيد وتمكن فيها الملك عبدالعزيز من هزيمة ابن رشيد الذي قتل فيها واستسلام القوات العثمانية وتسليمها المدافع والذخيرة ومغادرة القصيم."},{"id":"9","created_at":"2021-08-09 00:00:23","updated_at":"2021-08-09 00:00:23","deleted_at":null,"title_en":"Battle of AL KUT in AL AHSA","date_en":"1913 (1331H)","content_en":"It was one of the battles that aimed at uniting the State, through recapturing AL AHSA, driving the Ottoman garrison out of AL KUT palace and the whole province and eventually uniting the eastern part of the State.","title_ar":"معركة الكوت بالأحساء","date_ar":"١٣٣١هـ","content_ar":"من معارك توحيد البلاد تم من خلالها استعادة الأحساء وطرد الحامية العثمانية من قصر الكوت والمنطقة وتوحيد شرقي البلاد."},{"id":"10","created_at":"2021-08-09 00:01:04","updated_at":"2021-08-09 00:01:04","deleted_at":null,"title_en":"Battle of JIRAB","date_en":"1915 (1333H)","content_en":"In that battle, King AbdulAziz confronted the troops of Ibn Rashid in JIRAB, near Al Majma’ah, but no victory was achieved.","title_ar":"معركة جراب","date_ar":"١٣٣٣هـ","content_ar":"واجه فيها الملك عبدالعزيز بقواته حيش سعود بن رشيد في جراب بالقرب من المجمعة ولم يتحقق النصر فيها."},{"id":"11","created_at":"2021-08-09 00:01:43","updated_at":"2021-08-09 00:01:43","deleted_at":null,"title_en":"Battle of TURUBAH","date_en":"1919 (1337H)","content_en":"The forces of King AbdulAziz became victorious in that battle, which marked one of the beginnings of retaking Hijaz.","title_ar":"معركة تربة","date_ar":"١٣٣٧هـ","content_ar":"انتصرت فيها قوات الملك عبدالعزيز وتعد من مراحل بدايات ضم الحجاز."},{"id":"12","created_at":"2021-08-09 00:02:44","updated_at":"2021-08-09 00:02:44","deleted_at":null,"title_en":"Entering `ASIR","date_en":"1920 (1338H)","content_en":"The city was taken by an expedition led by Prince AbdulAziz bin Musa’ad bin Jalawi.","title_ar":"دخول عسير","date_ar":"١٣٣٨هـ","content_ar":"ضم عسير من خلال حملة قادها الأميرعبدالعزيز بن مساعد بن جلوي."},{"id":"13","created_at":"2021-08-09 00:03:25","updated_at":"2021-08-09 00:03:25","deleted_at":null,"title_en":"Entering NAJRAN","date_en":"1920 (1338H)","content_en":"The forces of King AbdulAziz were victorious in it and managed to unite it with the rest of the State provinces.","title_ar":"دخول نجران","date_ar":"١٣٣٨هـ","content_ar":"انتصرت قيها قوات الملك عبدالعزيز وتمكنت من توحيدها مع بقية مناطق البلاد"},{"id":"14","created_at":"2021-08-09 00:04:10","updated_at":"2021-08-09 00:04:10","deleted_at":null,"title_en":"Entering HAIL","date_en":"1921 (1340H)","content_en":"The forces of King AbdulAziz managed to capture HAIL after defeating the forces of Ibn Rashid and the city surrendered.","title_ar":"دخول حائل","date_ar":"١٣٤٠هـ","content_ar":"تمكنت قوات الملك عبدالعزيز من ضم حائل بعد هزيمة قوات ابن رشيد واستسلام المدينة."},{"id":"15","created_at":"2021-08-09 00:04:55","updated_at":"2021-08-09 00:04:55","deleted_at":null,"title_en":"Entering MAKKAH AL MUKARRAMAH","date_en":"1924 (1343H)","content_en":"King AbdulAziz entered MAKKAH AL MUKARRAMAH while in a state of Ihram, with no fighting, thus uniting it with the rest of State provinces.","title_ar":"دخول مكة المكرمة","date_ar":"١٣٤٣هـ","content_ar":"دخل الملك عبدالعزيز مكة المكرمة محرماً دون قتال مستكملاً توحيدها مع مناطق البلاد."},{"id":"16","created_at":"2021-08-09 00:05:35","updated_at":"2021-08-09 00:05:35","deleted_at":null,"title_en":"Entering JIDDAH","date_en":"1925 (1344H)","content_en":"After entering MAKKAH AL MUKARRAMAH, the forces of King AbdulAziz camped outside JIDDAH to avoid fighting till it surrendered and was united with the rest of the State provinces.","title_ar":"دخول جدة","date_ar":"١٣٤٤هـ","content_ar":"بعد دخول مكة المكرمة رابطت قوات الملك عبدالعزيز خارج جدة لتجنب القتال حتى تم استسلامها وتم توحيدها مع مناطق البلاد."},{"id":"17","created_at":"2021-08-09 00:06:14","updated_at":"2021-08-09 00:06:14","deleted_at":null,"title_en":"Entering AL MADINAH AL MUAWWARAH","date_en":"1925 (1344H)","content_en":"The forces of King AbdulAziz camped outside AL MADINAH AL MUAWWARAH, under the leadership of Prince Mohamed bin AbdulAziz, till the garrison stationed in it surrendered.","title_ar":"دخول المدينة المنورة","date_ar":"١٣٤٤هـ","content_ar":"رابطت قوات الملك عبدالعزيز خارج المدينة حتى تم استسلام الحامية التي بها على يد الأمير محمد بن عبدالعزيز."},{"id":"18","created_at":"2021-08-09 00:06:54","updated_at":"2021-08-09 00:06:54","deleted_at":null,"title_en":"Battle of As Sibalah","date_en":"1929 (1347H)","content_en":"It is the battle in which the forces of King AbdulAziz defeated those who rebelled against the State. The battle achieved security and stability.","title_ar":"معركة السبلة","date_ar":"١٣٤٧هـ","content_ar":"انتصرت فيها قوات الملك عبدالعزيز على الخارجين عن طاعة الدولة وتحقق نتيجةً لها الأمن والاستقرار."},{"id":"19","created_at":"2021-08-09 00:07:32","updated_at":"2021-08-09 00:07:32","deleted_at":null,"title_en":"Battle of JAZAN","date_en":"1930 (1349H)","content_en":"It is the battle in which the force of King AbdulAziz successfully entered JAZAN and bring it within the fold of the State and this achieve security and stability in it.","title_ar":"معركة جازان","date_ar":"١٣٤٩هـ","content_ar":"تمكنت قوات الملك عبدالعزيز من دخول جازان وضمها للبلاد وتحقيق الأمن والاستقرار فيها."},{"id":"20","created_at":"2021-08-09 00:08:08","updated_at":"2021-08-09 00:08:08","deleted_at":null,"title_en":"Declaration of the Kingdom of Saudi Arabia","date_en":"1932","content_en":"The foundation of the Kingdom of Saudi Arabia was declared pursuant to Royal Decree No. 2716, dated Thursday, 21st Jumada Al-Oula, 1351H, corresponding to 23rd September, 1932, which is celebrated as the National Day.","title_ar":"إعلان توحيد المملكة العربية السعودية","date_ar":"١٣٥١هـ","content_ar":"صدر إعلان تأسيس المملكة العربية السعودية بأمر ملكي رقم ٢٧١٦ باختيار يوم الخميس ٢١ جمادى الأولى ١٣٥١ هـ ٢٣ سبتمبر ١٩٣٢ الموافق للأول من الميزان ليكون هزا اليوم هو اليوم الوطني للمملكة العربية السعودية لتحتفل فيه بزكرى توحيد البلاد"}]}');

/***/ })

},
/******/ __webpack_require__ => { // webpackRuntimeModules
/******/ "use strict";
/******/ 
/******/ var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
/******/ __webpack_require__.O(0, ["vendor"], () => (__webpack_exec__(8835)));
/******/ var __webpack_exports__ = __webpack_require__.O();
/******/ }
]);
//# sourceMappingURL=main.js.map