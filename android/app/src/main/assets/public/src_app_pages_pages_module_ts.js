(self["webpackChunkSaudi_Flag_App"] = self["webpackChunkSaudi_Flag_App"] || []).push([["src_app_pages_pages_module_ts"],{

/***/ 3707:
/*!***********************************************!*\
  !*** ./src/app/pages/pages-routing.module.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PagesRoutingModule": () => (/* binding */ PagesRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ 29535);



const routes = [
    {
        path: '',
        redirectTo: 'splash',
        pathMatch: 'full',
    },
    {
        path: 'splash',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-src_app_pages_base-page_base-page_ts"), __webpack_require__.e("src_app_pages_splash_splash_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./splash/splash.module */ 56724)).then((m) => m.SplashPageModule),
    },
    {
        path: 'dashboard',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-src_app_pages_base-page_base-page_ts"), __webpack_require__.e("default-src_app_components_shared_module_ts"), __webpack_require__.e("src_app_pages_dashboard_dashboard_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./dashboard/dashboard.module */ 59412)).then((m) => m.DashboardPageModule),
    },
    {
        path: 'milestones',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-src_app_pages_base-page_base-page_ts"), __webpack_require__.e("default-src_app_components_shared_module_ts"), __webpack_require__.e("src_app_pages_milestones_milestones_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./milestones/milestones.module */ 99836)).then((m) => m.MilestonesPageModule),
    },
    {
        path: 'flag_history',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-src_app_pages_base-page_base-page_ts"), __webpack_require__.e("default-src_app_components_shared_module_ts"), __webpack_require__.e("src_app_pages_flag-history_flag-history_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./flag-history/flag-history.module */ 24821)).then((m) => m.FlagHistoryPageModule),
    },
    {
        path: 'flag_rules',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-src_app_pages_base-page_base-page_ts"), __webpack_require__.e("default-src_app_components_shared_module_ts"), __webpack_require__.e("src_app_pages_flag-rules_flag-rules_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./flag-rules/flag-rules.module */ 10307)).then((m) => m.FlagRulesPageModule),
    },
    {
        path: 'mast',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-src_app_pages_base-page_base-page_ts"), __webpack_require__.e("default-src_app_components_shared_module_ts"), __webpack_require__.e("src_app_pages_mast_mast_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./mast/mast.module */ 5245)).then((m) => m.MastPageModule),
    },
    {
        path: 'gallery',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-src_app_pages_base-page_base-page_ts"), __webpack_require__.e("default-src_app_components_shared_module_ts"), __webpack_require__.e("src_app_pages_gallery_gallery_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./gallery/gallery.module */ 81115)).then((m) => m.GalleryPageModule),
    },
    {
        path: 'national_anthem',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-src_app_pages_base-page_base-page_ts"), __webpack_require__.e("default-src_app_components_shared_module_ts"), __webpack_require__.e("src_app_pages_national-anthem_national-anthem_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./national-anthem/national-anthem.module */ 69129)).then((m) => m.NationalAnthemPageModule),
    },
    {
        path: 'contact-us',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-src_app_pages_base-page_base-page_ts"), __webpack_require__.e("default-src_app_components_shared_module_ts"), __webpack_require__.e("src_app_pages_contact-us_contact-us_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./contact-us/contact-us.module */ 5344)).then(m => m.ContactUsPageModule)
    },
];
let PagesRoutingModule = class PagesRoutingModule {
};
PagesRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_1__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule],
    })
], PagesRoutingModule);



/***/ }),

/***/ 59525:
/*!***************************************!*\
  !*** ./src/app/pages/pages.module.ts ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PagesModule": () => (/* binding */ PagesModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 16274);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 93324);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 34595);
/* harmony import */ var _pages_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./pages-routing.module */ 3707);






let PagesModule = class PagesModule {
};
PagesModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        declarations: [],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.ReactiveFormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule,
            _pages_routing_module__WEBPACK_IMPORTED_MODULE_0__.PagesRoutingModule
        ],
        providers: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.Location,
        ]
    })
], PagesModule);



/***/ })

}]);
//# sourceMappingURL=src_app_pages_pages_module_ts.js.map