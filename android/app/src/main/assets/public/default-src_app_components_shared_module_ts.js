(self["webpackChunkSaudi_Flag_App"] = self["webpackChunkSaudi_Flag_App"] || []).push([["default-src_app_components_shared_module_ts"],{

/***/ 84651:
/*!***********************************************************************!*\
  !*** ./src/app/components/contact-us-fab/contact-us-fab.component.ts ***!
  \***********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ContactUsFabComponent": () => (/* binding */ ContactUsFabComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _raw_loader_contact_us_fab_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./contact-us-fab.component.html */ 86470);
/* harmony import */ var _contact_us_fab_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./contact-us-fab.component.scss */ 79940);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var src_app_pages_base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/pages/base-page/base-page */ 62168);
/* harmony import */ var src_app_pages_contact_us_contact_us_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/pages/contact-us/contact-us.page */ 94506);






let ContactUsFabComponent = class ContactUsFabComponent extends src_app_pages_base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injector) {
        super(injector);
    }
    ngOnInit() { }
    openContactUs() {
        this.modals.present(src_app_pages_contact_us_contact_us_page__WEBPACK_IMPORTED_MODULE_3__.ContactUsPage);
    }
};
ContactUsFabComponent.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.Injector }
];
ContactUsFabComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Component)({
        selector: 'contact-us-fab',
        template: _raw_loader_contact_us_fab_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_contact_us_fab_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], ContactUsFabComponent);



/***/ }),

/***/ 95225:
/*!*********************************************************************!*\
  !*** ./src/app/components/flag-rule-des/flag-rule-des.component.ts ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FlagRuleDesComponent": () => (/* binding */ FlagRuleDesComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _raw_loader_flag_rule_des_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./flag-rule-des.component.html */ 99270);
/* harmony import */ var _flag_rule_des_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./flag-rule-des.component.scss */ 36122);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var src_app_pages_base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/pages/base-page/base-page */ 62168);





let FlagRuleDesComponent = class FlagRuleDesComponent extends src_app_pages_base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injector) {
        super(injector);
        this.lang = 'en';
        this.isLoading = true;
        this.isNetworkOngoing = false;
        this.lang = localStorage.getItem('lang');
    }
    ngOnInit() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            yield this.initialize();
            this.sync();
        });
    }
    sync() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            if (!this.isNetworkOngoing) {
                yield this.sqlite.syncFlagRule(this.rule.key);
                this.isNetworkOngoing = false;
            }
        });
    }
    initialize() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            this.item = yield this.sqlite.getFlagRulesData(this.lang, this.rule.key);
            // console.log(data);
            // this.item = this.jsonService.getFlagRulesDesc(this.rule.key);
            console.log(this.item);
            this.isLoading = false;
        });
    }
    close() {
        this.modals.dismiss();
    }
};
FlagRuleDesComponent.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.Injector }
];
FlagRuleDesComponent.propDecorators = {
    lang: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.Input }]
};
FlagRuleDesComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Component)({
        selector: 'app-flag-rule-des',
        template: _raw_loader_flag_rule_des_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_flag_rule_des_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], FlagRuleDesComponent);



/***/ }),

/***/ 33883:
/*!*******************************************************!*\
  !*** ./src/app/components/header/header.component.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HeaderComponent": () => (/* binding */ HeaderComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _raw_loader_header_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./header.component.html */ 97911);
/* harmony import */ var _header_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./header.component.scss */ 64993);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var src_app_pages_base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/pages/base-page/base-page */ 62168);





let HeaderComponent = class HeaderComponent extends src_app_pages_base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injector) {
        super(injector);
        this.title = '';
        this.icon = '';
        this.format = 'png';
        this.lang = 'en';
        this.isModal = false;
        this.assetLink = 'icon';
        this.lang = localStorage.getItem('lang');
    }
    ngOnInit() { }
    goBack() {
        if (this.isModal) {
            this.modals.dismiss();
        }
        else {
            this.nav.pop();
        }
    }
};
HeaderComponent.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Injector }
];
HeaderComponent.propDecorators = {
    title: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Input }],
    icon: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Input }],
    format: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Input }],
    lang: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Input }],
    isModal: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Input }],
    assetLink: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Input }]
};
HeaderComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-header',
        template: _raw_loader_header_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_header_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], HeaderComponent);



/***/ }),

/***/ 52919:
/*!***************************************************************************!*\
  !*** ./src/app/components/milestone-detail/milestone-detail.component.ts ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MilestoneDetailComponent": () => (/* binding */ MilestoneDetailComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _raw_loader_milestone_detail_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./milestone-detail.component.html */ 15762);
/* harmony import */ var _milestone_detail_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./milestone-detail.component.scss */ 14735);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var src_app_pages_base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/pages/base-page/base-page */ 62168);





let MilestoneDetailComponent = class MilestoneDetailComponent extends src_app_pages_base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injector) {
        super(injector);
        this.lang = localStorage.getItem('lang');
    }
    ngOnInit() {
        console.log(this.item);
    }
    close() {
        this.modals.dismiss();
    }
};
MilestoneDetailComponent.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Injector }
];
MilestoneDetailComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-milestone-detail',
        template: _raw_loader_milestone_detail_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_milestone_detail_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], MilestoneDetailComponent);



/***/ }),

/***/ 16369:
/*!*********************************************!*\
  !*** ./src/app/components/shared.module.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SharedModule": () => (/* binding */ SharedModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ 16274);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ 93324);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic/angular */ 34595);
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./header/header.component */ 33883);
/* harmony import */ var _milestone_detail_milestone_detail_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./milestone-detail/milestone-detail.component */ 52919);
/* harmony import */ var _flag_rule_des_flag_rule_des_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./flag-rule-des/flag-rule-des.component */ 95225);
/* harmony import */ var _contact_us_fab_contact_us_fab_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./contact-us-fab/contact-us-fab.component */ 84651);









let SharedModule = class SharedModule {
};
SharedModule = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.NgModule)({
        declarations: [
            _header_header_component__WEBPACK_IMPORTED_MODULE_0__.HeaderComponent,
            _milestone_detail_milestone_detail_component__WEBPACK_IMPORTED_MODULE_1__.MilestoneDetailComponent,
            _flag_rule_des_flag_rule_des_component__WEBPACK_IMPORTED_MODULE_2__.FlagRuleDesComponent,
            _contact_us_fab_contact_us_fab_component__WEBPACK_IMPORTED_MODULE_3__.ContactUsFabComponent
        ],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_6__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__.FormsModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__.ReactiveFormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_8__.IonicModule,
        ],
        exports: [
            _header_header_component__WEBPACK_IMPORTED_MODULE_0__.HeaderComponent,
            _milestone_detail_milestone_detail_component__WEBPACK_IMPORTED_MODULE_1__.MilestoneDetailComponent,
            _flag_rule_des_flag_rule_des_component__WEBPACK_IMPORTED_MODULE_2__.FlagRuleDesComponent,
            _contact_us_fab_contact_us_fab_component__WEBPACK_IMPORTED_MODULE_3__.ContactUsFabComponent
        ]
    })
], SharedModule);



/***/ }),

/***/ 94506:
/*!*****************************************************!*\
  !*** ./src/app/pages/contact-us/contact-us.page.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ContactUsPage": () => (/* binding */ ContactUsPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _raw_loader_contact_us_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./contact-us.page.html */ 53300);
/* harmony import */ var _contact_us_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./contact-us.page.scss */ 7461);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../base-page/base-page */ 62168);





let ContactUsPage = class ContactUsPage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injector) {
        super(injector);
    }
    ngOnInit() {
        this.init();
    }
    init() {
        this.data = this.jsonService.getJsonData('contact-form');
        this.feedback = {
            description: null,
            email: null,
            feedback: null,
            name: null,
            phoneNumber: null,
        };
        console.log(this.feedback);
    }
    sendContactUsData() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            this.responseMsg = this.jsonService.getResponseData('contact-form');
            if (this.feedback.age ||
                this.feedback.description ||
                this.feedback.email ||
                this.feedback.feedback ||
                this.feedback.gender ||
                this.feedback.name ||
                this.feedback.phoneNumber) {
                switch (this.jsonService.lang) {
                    case 'en':
                        console.log('language', this.jsonService.lang);
                        this.utility.presentSuccessToast(this.responseMsg.successRes, 'successToastEn');
                        break;
                    case 'ar':
                        console.log('language', this.jsonService.lang);
                        this.utility.presentSuccessToast(this.responseMsg.successRes, 'successToastAr');
                        break;
                    default:
                        console.log('language', this.jsonService.lang);
                        this.utility.presentSuccessToast(this.responseMsg.successRes, 'successToastEn');
                        break;
                }
            }
            else {
                switch (this.jsonService.lang) {
                    case 'en':
                        console.log('language', this.jsonService.lang);
                        this.utility.presentFailureToast(this.responseMsg.failureRes, 'failureToastEn');
                        break;
                    case 'ar':
                        console.log('language', this.jsonService.lang);
                        this.utility.presentFailureToast(this.responseMsg.failureRes, 'successToastAr');
                        break;
                    default:
                        console.log('language', this.jsonService.lang);
                        this.utility.presentFailureToast(this.responseMsg.failureRes, 'failureToastEn');
                        break;
                }
            }
            // const res = await this.network.postContactUsData(data);
        });
    }
};
ContactUsPage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.Injector }
];
ContactUsPage = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Component)({
        selector: 'app-contact-us',
        template: _raw_loader_contact_us_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_contact_us_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], ContactUsPage);



/***/ }),

/***/ 79940:
/*!*************************************************************************!*\
  !*** ./src/app/components/contact-us-fab/contact-us-fab.component.scss ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJjb250YWN0LXVzLWZhYi5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ 36122:
/*!***********************************************************************!*\
  !*** ./src/app/components/flag-rule-des/flag-rule-des.component.scss ***!
  \***********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".details-header .header-img {\n  width: 75px;\n  height: 50px;\n  object-fit: contain;\n  margin: 15px 0px;\n}\n.details-header .close-icon {\n  width: 50px;\n  height: 100%;\n}\n.details-header ion-toolbar {\n  --background: #6e5946;\n  color: white;\n}\n.details-header .top-heading {\n  font-size: 1.5em;\n  line-height: 1.5em;\n  margin: 0;\n  font-weight: bold !important;\n  padding: 10px 3px;\n  padding-top: 0 !important;\n}\n.rule-detail {\n  margin: 0 18px;\n  text-align: center;\n  margin-top: 75px;\n}\n.rule-detail ::ng-deep p {\n  color: var(--ion-color-primary);\n  font-size: 20px;\n  font-weight: bolder;\n  line-height: 30px;\n  margin: 30px;\n}\n.rule-detail ::ng-deep img {\n  max-height: 250px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImZsYWctcnVsZS1kZXMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBS0k7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7QUFKUjtBQU9JO0VBQ0ksV0FBQTtFQUNBLFlBQUE7QUFMUjtBQVFJO0VBQ0kscUJBQUE7RUFDQSxZQUFBO0FBTlI7QUFTSTtFQUNJLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsNEJBQUE7RUFDQSxpQkFBQTtFQUNBLHlCQUFBO0FBUFI7QUFXQTtFQUNJLGNBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0FBUko7QUFVSTtFQUNJLCtCQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0FBUlI7QUFXSTtFQUNJLGlCQUFBO0FBVFIiLCJmaWxlIjoiZmxhZy1ydWxlLWRlcy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIGlvbi1jb250ZW50IHtcclxuLy8gICAgIC8vIC0tYmFja2dyb3VuZDogdXJsKFwiLi8uLi8uLi8uLi9hc3NldHMvaW1ncy9iZy5wbmdcIikgMCAwLzEwMCUgMTAwJSBuby1yZXBlYXQ7XHJcbi8vIH1cclxuXHJcbi5kZXRhaWxzLWhlYWRlciB7XHJcbiAgICAuaGVhZGVyLWltZyB7XHJcbiAgICAgICAgd2lkdGg6IDc1cHg7XHJcbiAgICAgICAgaGVpZ2h0OiA1MHB4O1xyXG4gICAgICAgIG9iamVjdC1maXQ6IGNvbnRhaW47XHJcbiAgICAgICAgbWFyZ2luOiAxNXB4IDBweDtcclxuICAgIH1cclxuXHJcbiAgICAuY2xvc2UtaWNvbiB7XHJcbiAgICAgICAgd2lkdGg6IDUwcHg7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgfVxyXG5cclxuICAgIGlvbi10b29sYmFyIHtcclxuICAgICAgICAtLWJhY2tncm91bmQ6ICM2ZTU5NDY7XHJcbiAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgfVxyXG5cclxuICAgIC50b3AtaGVhZGluZyB7XHJcbiAgICAgICAgZm9udC1zaXplOiAxLjVlbTtcclxuICAgICAgICBsaW5lLWhlaWdodDogMS41ZW07XHJcbiAgICAgICAgbWFyZ2luOiAwO1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgcGFkZGluZzogMTBweCAzcHg7XHJcbiAgICAgICAgcGFkZGluZy10b3A6IDAgIWltcG9ydGFudDtcclxuICAgIH1cclxufVxyXG5cclxuLnJ1bGUtZGV0YWlsIHtcclxuICAgIG1hcmdpbjogMCAxOHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgbWFyZ2luLXRvcDogNzVweDtcclxuXHJcbiAgICA6Om5nLWRlZXAgcCB7XHJcbiAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxuICAgICAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcclxuICAgICAgICBsaW5lLWhlaWdodDogMzBweDtcclxuICAgICAgICBtYXJnaW46IDMwcHg7XHJcbiAgICB9XHJcblxyXG4gICAgOjpuZy1kZWVwIGltZyB7XHJcbiAgICAgICAgbWF4LWhlaWdodDogMjUwcHg7XHJcbiAgICB9XHJcbn1cclxuIl19 */");

/***/ }),

/***/ 64993:
/*!*********************************************************!*\
  !*** ./src/app/components/header/header.component.scss ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".app-header .header-img {\n  width: 75px;\n  height: 50px;\n  object-fit: contain;\n  margin: 15px 0px;\n}\n.app-header .back-img {\n  width: 50px;\n}\n.app-header ion-toolbar {\n  --background: #6e5946;\n  box-shadow: 0px 5px 5px 0px rgba(0, 0, 0, 0.25);\n  color: white;\n}\n.app-header .top-heading {\n  font-size: 1.3em;\n  padding-bottom: 10px;\n  font-weight: bold !important;\n  white-space: initial;\n}\n.app-header .top-heading span {\n  display: inline-block;\n  white-space: normal;\n  line-height: 30px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImhlYWRlci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFLSTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtBQUpSO0FBT0k7RUFDSSxXQUFBO0FBTFI7QUFRSTtFQUNJLHFCQUFBO0VBQ0EsK0NBQUE7RUFDQSxZQUFBO0FBTlI7QUFjSTtFQUNJLGdCQUFBO0VBQ0Esb0JBQUE7RUFDQSw0QkFBQTtFQUNBLG9CQUFBO0FBWlI7QUFhUTtFQUNJLHFCQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtBQVhaIiwiZmlsZSI6ImhlYWRlci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5hcHAtaGVhZGVyIHtcclxuICAgIGlvbi10b29sYmFyIHtcclxuICAgICAgICAvLyBoZWlnaHQ6IDY1cHg7XHJcbiAgICB9XHJcblxyXG4gICAgLmhlYWRlci1pbWcge1xyXG4gICAgICAgIHdpZHRoOiA3NXB4O1xyXG4gICAgICAgIGhlaWdodDogNTBweDtcclxuICAgICAgICBvYmplY3QtZml0OiBjb250YWluO1xyXG4gICAgICAgIG1hcmdpbjogMTVweCAwcHg7XHJcbiAgICB9XHJcblxyXG4gICAgLmJhY2staW1nIHtcclxuICAgICAgICB3aWR0aDogNTBweDtcclxuICAgIH1cclxuXHJcbiAgICBpb24tdG9vbGJhciB7XHJcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiAjNmU1OTQ2O1xyXG4gICAgICAgIGJveC1zaGFkb3c6IDBweCA1cHggNXB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIH1cclxuXHJcbiAgICBpb24tdGl0bGUge1xyXG4gICAgICAgIC8vIHBhZGRpbmctbGVmdDogOXB4O1xyXG4gICAgICAgIC8vIGZvbnQtd2VpZ2h0OiBib2xkZXI7XHJcbiAgICB9XHJcblxyXG4gICAgLnRvcC1oZWFkaW5nIHtcclxuICAgICAgICBmb250LXNpemU6IDEuM2VtO1xyXG4gICAgICAgIHBhZGRpbmctYm90dG9tOiAxMHB4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgd2hpdGUtc3BhY2U6IGluaXRpYWw7XHJcbiAgICAgICAgc3BhbiB7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICAgICAgd2hpdGUtc3BhY2U6IG5vcm1hbDtcclxuICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ 14735:
/*!*****************************************************************************!*\
  !*** ./src/app/components/milestone-detail/milestone-detail.component.scss ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".details-header ion-toolbar {\n  --background: #5f503e;\n}\n.details-header ion-toolbar .heading {\n  width: 100%;\n  padding-left: 15px;\n  text-align: left;\n}\n.details-header ion-toolbar .heading p {\n  font-weight: bolder;\n  color: white;\n  font-size: 19px;\n  margin: 0;\n}\n.details-header ion-toolbar .close-btn {\n  width: 50px;\n  height: 50px;\n}\n.heading-ar {\n  padding-right: 15px;\n  text-align: right !important;\n}\n.milestone-detail {\n  margin: 0 18px;\n  text-align: center;\n  margin-top: 75px;\n}\n.milestone-detail p {\n  color: var(--ion-color-primary);\n  font-size: 20px;\n  font-weight: bolder;\n  line-height: normal;\n}\n.rule-detail ::ng-deep img {\n  animation: fade-in 1.2s cubic-bezier(0.39, 0.575, 0.565, 1) both;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1pbGVzdG9uZS1kZXRhaWwuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0U7RUFDRSxxQkFBQTtBQUFKO0FBRUk7RUFDRSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQUFOO0FBRU07RUFDRSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsU0FBQTtBQUFSO0FBSUk7RUFDRSxXQUFBO0VBQ0EsWUFBQTtBQUZOO0FBZUE7RUFDRSxtQkFBQTtFQUNBLDRCQUFBO0FBWkY7QUFlQTtFQUNFLGNBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0FBWkY7QUFjRTtFQUNFLCtCQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7QUFaSjtBQWlCRTtFQUVFLGdFQUFBO0FBZEoiLCJmaWxlIjoibWlsZXN0b25lLWRldGFpbC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5kZXRhaWxzLWhlYWRlciB7XHJcbiAgaW9uLXRvb2xiYXIge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjNWY1MDNlO1xyXG5cclxuICAgIC5oZWFkaW5nIHtcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgIHBhZGRpbmctbGVmdDogMTVweDtcclxuICAgICAgdGV4dC1hbGlnbjogbGVmdDtcclxuXHJcbiAgICAgIHAge1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XHJcbiAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTlweDtcclxuICAgICAgICBtYXJnaW46IDA7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAuY2xvc2UtYnRuIHtcclxuICAgICAgd2lkdGg6IDUwcHg7XHJcbiAgICAgIGhlaWdodDogNTBweDtcclxuICAgICAgLy8gYm9yZGVyLXJhZGl1czogNTAlO1xyXG5cclxuICAgICAgLy8gaW9uLWljb24ge1xyXG4gICAgICAvLyAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgICAgLy8gICBjb2xvcjogIzVmNTAzZTtcclxuICAgICAgLy8gICAtLWlvbmljb24tc3Ryb2tlLXdpZHRoOiA3M3B4O1xyXG4gICAgICAvLyAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWCgtNnB4KTtcclxuICAgICAgLy8gfVxyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuLmhlYWRpbmctYXIge1xyXG4gIHBhZGRpbmctcmlnaHQ6IDE1cHg7XHJcbiAgdGV4dC1hbGlnbjogcmlnaHQgIWltcG9ydGFudDtcclxufVxyXG5cclxuLm1pbGVzdG9uZS1kZXRhaWwge1xyXG4gIG1hcmdpbjogMCAxOHB4O1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBtYXJnaW4tdG9wOiA3NXB4O1xyXG5cclxuICBwIHtcclxuICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XHJcbiAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICBmb250LXdlaWdodDogYm9sZGVyO1xyXG4gICAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcclxuICB9XHJcbn1cclxuXHJcbi5ydWxlLWRldGFpbCB7XHJcbiAgOjpuZy1kZWVwIGltZyB7XHJcbiAgICAtd2Via2l0LWFuaW1hdGlvbjogZmFkZS1pbiAxLjJzIGN1YmljLWJlemllcigwLjM5LCAwLjU3NSwgMC41NjUsIDEpIGJvdGg7XHJcbiAgICBhbmltYXRpb246IGZhZGUtaW4gMS4ycyBjdWJpYy1iZXppZXIoMC4zOSwgMC41NzUsIDAuNTY1LCAxKSBib3RoO1xyXG4gIH1cclxufVxyXG4iXX0= */");

/***/ }),

/***/ 7461:
/*!*******************************************************!*\
  !*** ./src/app/pages/contact-us/contact-us.page.scss ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".contact-page {\n  direction: rtl;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRhY3QtdXMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksY0FBQTtBQUNKIiwiZmlsZSI6ImNvbnRhY3QtdXMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnRhY3QtcGFnZXtcbiAgICBkaXJlY3Rpb246IHJ0bDtcbn1cblxuIl19 */");

/***/ }),

/***/ 86470:
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/contact-us-fab/contact-us-fab.component.html ***!
  \***************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-fab vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\">\n  <ion-fab-button (click)=\"openContactUs()\">\n    <ion-icon name=\"mail-outline\"></ion-icon>\n  </ion-fab-button>\n</ion-fab>");

/***/ }),

/***/ 99270:
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/flag-rule-des/flag-rule-des.component.html ***!
  \*************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!-- <ion-content\r\n  [style]=\"{'--background': 'url(./../../../assets/imgs/milestones/'+ item?.id +'.png) 0 0/100% 100% no-repeat'}\"> -->\r\n\r\n<!-- <ion-header class=\"details-header\">\r\n  <ion-toolbar>\r\n    <img class=\"header-img\" [src]=\"'assets/imgs/flag-rules/' + rule.key + '.png' \" slot=\"start\">\r\n    <p class=\"top-heading fade-in\" [class.text-left]=\"lang === 'en'\" [class.text-right]=\"lang === 'ar'\"> {{rule?.value}}\r\n    </p>\r\n    <ion-buttons slot=\"end\">\r\n      <ion-button fill=\"clear\" (click)=\"close()\" color=\"light\">\r\n        <ion-icon name=\"close-circle-outline\" class=\"close-icon\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header> -->\r\n\r\n<app-header [isModal]=\"true\" [title]=\"rule.value\" format=\"png\" [assetLink]=\"'imgs'\" [icon]=\"'flag-rules/' + rule.key\"></app-header>\r\n\r\n<ion-content>\r\n  <img class=\"background-image\" src=\"./../../../assets/imgs/bg.png\">\r\n\r\n\r\n  <div *ngIf=\"!isLoading\">\r\n    <div class=\"rule-detail\" [innerHtml]=\"item.content\"></div>\r\n  </div>\r\n\r\n  <div class=\"spinner\" *ngIf=\"isLoading\">\r\n    <ion-spinner color=\"primary\"></ion-spinner>\r\n  </div>\r\n\r\n</ion-content>\r\n");

/***/ }),

/***/ 97911:
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/header/header.component.html ***!
  \***********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header class=\"app-header\">\r\n  <ion-toolbar>\r\n    <img [hidden]=\"!icon\" class=\"fade-in header-img\" [src]=\"'assets/' + assetLink + '/' + icon + '.' + format\" slot=\"start\">\r\n    <ion-title [hidden]=\"!title\" class=\"top-heading fade-in\" [class.text-left]=\"lang === 'en'\"\r\n      [class.text-right]=\"lang === 'ar'\"> <span>{{title}}</span></ion-title>\r\n    <ion-buttons slot=\"end\">\r\n      <ion-button (click)=\"goBack()\">\r\n        <img class=\"back-img\" src=\"assets/icon/back.png\">\r\n      </ion-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n");

/***/ }),

/***/ 15762:
/*!*******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/milestone-detail/milestone-detail.component.html ***!
  \*******************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-content\r\n  [style]=\"{'--background': 'url(./../../../assets/imgs/milestones/'+ item?.id +'.png) 0 0/100% 100% no-repeat'}\">\r\n\r\n  <ion-header class=\"details-header\">\r\n    <ion-toolbar>\r\n      <div slot=\"start\" class=\"heading\"  [class.heading-ar]=\"lang === 'ar'\">\r\n        <p>\r\n          {{item.title}}\r\n        </p>\r\n        <p>\r\n          {{item.date}}\r\n        </p>\r\n      </div>\r\n\r\n\r\n      <ion-buttons slot=\"end\">\r\n          <ion-button (click)=\"close()\">\r\n            <img class=\"close-btn\" src=\"assets/icon/back.png\">\r\n          </ion-button>\r\n      </ion-buttons>\r\n\r\n    </ion-toolbar>\r\n  </ion-header>\r\n\r\n  <div class=\"milestone-detail\">\r\n    <p>\r\n      {{item.content}}\r\n    </p>\r\n  </div>\r\n\r\n\r\n</ion-content>");

/***/ }),

/***/ 53300:
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/contact-us/contact-us.page.html ***!
  \*********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<app-header [title]=\"data?.title\" format=\"png\" [isModal]=\"true\"></app-header>\n\n<ion-content [ngClass]=\"jsonService.lang == 'ar' ? 'contact-page':''\">\n  <ion-row>\n    <ion-col class=\"ion-padding\" size=\"12\">\n      <h5>{{data?.description}}</h5>\n    </ion-col>\n  </ion-row>\n  <form>\n    <ion-grid>\n      <ion-row>\n        <ion-col size=\"12\">\n          <ion-item>\n            <ion-label> {{data?.name}} </ion-label>\n            <ion-input\n              [(ngModel)]=\"feedback.name\"\n              class=\"input\"\n              name=\"name\"\n            ></ion-input>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col size=\"12\">\n          <ion-item>\n            <ion-label> {{data?.email}} </ion-label>\n            <ion-input\n              [(ngModel)]=\"feedback.email\"\n              class=\"input\"\n              name=\"email\"\n            ></ion-input>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"12\">\n          <ion-item>\n            <ion-label> {{data?.phoneNumber}} </ion-label>\n            <ion-input\n              [(ngModel)]=\"feedback.phoneNumber\"\n              class=\"input\"\n              name=\"phoneNumber\"\n            ></ion-input>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col size=\"12\">\n          <ion-item>\n            <ion-label> {{data?.feedback}}</ion-label>\n            <ion-textarea\n              [(ngModel)]=\"feedback.feedback\"\n              class=\"textarea\"\n              name=\"feedback\"\n              rows=\"5\"\n            ></ion-textarea>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </form>\n</ion-content>\n\n<ion-footer>\n  <ion-row>\n    <ion-col>\n      <ion-button expand=\"block\" (click)=\"sendContactUsData()\">\n        {{data?.submit}}\n      </ion-button>\n    </ion-col>\n  </ion-row>\n</ion-footer>\n");

/***/ })

}]);
//# sourceMappingURL=default-src_app_components_shared_module_ts.js.map