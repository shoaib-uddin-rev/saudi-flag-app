(self["webpackChunkSaudi_Flag_App"] = self["webpackChunkSaudi_Flag_App"] || []).push([["src_app_pages_gallery_gallery_module_ts"],{

/***/ 52666:
/*!******************************************************************************!*\
  !*** ./src/app/pages/gallery/fullsize-gallery/fullsize-gallery.component.ts ***!
  \******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FullsizeGalleryComponent": () => (/* binding */ FullsizeGalleryComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _raw_loader_fullsize_gallery_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./fullsize-gallery.component.html */ 15269);
/* harmony import */ var _fullsize_gallery_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./fullsize-gallery.component.scss */ 49062);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../base-page/base-page */ 62168);





let FullsizeGalleryComponent = class FullsizeGalleryComponent extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injector) {
        super(injector);
        this.images = [];
        this.slideOpts = {};
        const m = localStorage.getItem('nimages');
        if (m) {
            this.images = JSON.parse(m);
        }
    }
    ngOnInit() {
        this.slideOpts = {
            initialSlide: this.index,
        };
    }
    ionViewWillEnter() {
        this.slides.update();
    }
    close() {
        this.modals.dismiss();
    }
};
FullsizeGalleryComponent.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Injector }
];
FullsizeGalleryComponent.propDecorators = {
    slides: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.ViewChild, args: ['slides',] }]
};
FullsizeGalleryComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-fullsize-gallery',
        template: _raw_loader_fullsize_gallery_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_fullsize_gallery_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], FullsizeGalleryComponent);



/***/ }),

/***/ 44386:
/*!*********************************************************!*\
  !*** ./src/app/pages/gallery/gallery-routing.module.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "GalleryPageRoutingModule": () => (/* binding */ GalleryPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 29535);
/* harmony import */ var _gallery_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./gallery.page */ 47640);




const routes = [
    {
        path: '',
        component: _gallery_page__WEBPACK_IMPORTED_MODULE_0__.GalleryPage
    }
];
let GalleryPageRoutingModule = class GalleryPageRoutingModule {
};
GalleryPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], GalleryPageRoutingModule);



/***/ }),

/***/ 81115:
/*!*************************************************!*\
  !*** ./src/app/pages/gallery/gallery.module.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "GalleryPageModule": () => (/* binding */ GalleryPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ 16274);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ 93324);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic/angular */ 34595);
/* harmony import */ var _gallery_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./gallery-routing.module */ 44386);
/* harmony import */ var _gallery_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./gallery.page */ 47640);
/* harmony import */ var src_app_components_shared_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/components/shared.module */ 16369);
/* harmony import */ var _photos_photos_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./photos/photos.component */ 84492);
/* harmony import */ var _fullsize_gallery_fullsize_gallery_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./fullsize-gallery/fullsize-gallery.component */ 52666);








// import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';


let GalleryPageModule = class GalleryPageModule {
};
GalleryPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_7__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_8__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_9__.IonicModule,
            _gallery_routing_module__WEBPACK_IMPORTED_MODULE_0__.GalleryPageRoutingModule,
            src_app_components_shared_module__WEBPACK_IMPORTED_MODULE_2__.SharedModule
        ],
        declarations: [_gallery_page__WEBPACK_IMPORTED_MODULE_1__.GalleryPage, _photos_photos_component__WEBPACK_IMPORTED_MODULE_3__.PhotosComponent, _fullsize_gallery_fullsize_gallery_component__WEBPACK_IMPORTED_MODULE_4__.FullsizeGalleryComponent],
        providers: [
        // PhotoViewer,
        ]
    })
], GalleryPageModule);



/***/ }),

/***/ 47640:
/*!***********************************************!*\
  !*** ./src/app/pages/gallery/gallery.page.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "GalleryPage": () => (/* binding */ GalleryPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _raw_loader_gallery_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./gallery.page.html */ 87239);
/* harmony import */ var _gallery_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./gallery.page.scss */ 15241);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../base-page/base-page */ 62168);
/* harmony import */ var _fullsize_gallery_fullsize_gallery_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./fullsize-gallery/fullsize-gallery.component */ 52666);






// import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
let GalleryPage = class GalleryPage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injector) {
        super(injector);
        this.isLoading = true;
    }
    ngOnInit() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            yield this.initialize();
        });
    }
    initialize() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.title = this.nav.getQueryParams().title;
            this.icon = this.nav.getQueryParams().icon;
            this.images = this.jsonService.getJsonData('gallery');
            ;
            localStorage.setItem('nimages', JSON.stringify(this.images));
            this.isLoading = false;
        });
    }
    showImage(i) {
        console.log(i);
        this.modals.present(_fullsize_gallery_fullsize_gallery_component__WEBPACK_IMPORTED_MODULE_3__.FullsizeGalleryComponent, { index: i });
    }
};
GalleryPage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Injector }
];
GalleryPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-gallery',
        template: _raw_loader_gallery_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_gallery_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], GalleryPage);



/***/ }),

/***/ 84492:
/*!**********************************************************!*\
  !*** ./src/app/pages/gallery/photos/photos.component.ts ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PhotosComponent": () => (/* binding */ PhotosComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _raw_loader_photos_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./photos.component.html */ 19706);
/* harmony import */ var _photos_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./photos.component.scss */ 18045);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../base-page/base-page */ 62168);





let PhotosComponent = class PhotosComponent extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injector) {
        super(injector);
        this.isImageLoaded = false;
    }
    ngOnInit() { }
    imageLoaded() {
        this.isImageLoaded = true;
    }
    getBgImage() {
        return `url(${this.image})`;
    }
};
PhotosComponent.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Injector }
];
PhotosComponent.propDecorators = {
    image: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Input }]
};
PhotosComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-photos',
        template: _raw_loader_photos_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_photos_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], PhotosComponent);



/***/ }),

/***/ 49062:
/*!********************************************************************************!*\
  !*** ./src/app/pages/gallery/fullsize-gallery/fullsize-gallery.component.scss ***!
  \********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("ion-content {\n  --background: #000;\n}\n\nion-toolbar {\n  --background: #000;\n}\n\nion-slides {\n  height: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImZ1bGxzaXplLWdhbGxlcnkuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxrQkFBQTtBQUNKOztBQUVBO0VBQ0ksa0JBQUE7QUFDSjs7QUFFQTtFQUNJLFlBQUE7QUFDSiIsImZpbGUiOiJmdWxsc2l6ZS1nYWxsZXJ5LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjMDAwO1xyXG59XHJcblxyXG5pb24tdG9vbGJhciB7XHJcbiAgICAtLWJhY2tncm91bmQ6ICMwMDA7XHJcbn1cclxuXHJcbmlvbi1zbGlkZXMge1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG59Il19 */");

/***/ }),

/***/ 15241:
/*!*************************************************!*\
  !*** ./src/app/pages/gallery/gallery.page.scss ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".masonry-item {\n  display: inline-block;\n  width: 33.33vw;\n  height: 22.22vh;\n  padding: 1px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImdhbGxlcnkucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQTRCQTtFQUNFLHFCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0FBM0JGIiwiZmlsZSI6ImdhbGxlcnkucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge1xyXG4gICAgLy8gLS1iYWNrZ3JvdW5kOiB1cmwoXCIuLy4uLy4uLy4uL2Fzc2V0cy9pbWdzL2JnLnBuZ1wiKSAwIDAvMTAwJSAxMDAlIG5vLXJlcGVhdDtcclxufVxyXG5cclxuLy8gLmltZy1jb2wge1xyXG4vLyAgICAgcGFkZGluZzogMXB4O1xyXG4vLyAgICAgZGlzcGxheTogZmxleDtcclxuLy8gICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4vLyAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuXHJcblxyXG4vLyAgICAgLy8gaW9uLWltZyB7XHJcbi8vICAgICAvLyAgICAgaGVpZ2h0OiAxNDBweDtcclxuLy8gICAgIC8vICAgICB3aWR0aDogMTAwJTtcclxuLy8gICAgIC8vICAgICBvYmplY3QtZml0OiBjb3ZlcjtcclxuLy8gICAgIC8vIH1cclxuLy8gfVxyXG5cclxuLy8gLm1hc29ucnktY29udGFpbmVyIHtcclxuLy8gICAtd2Via2l0LWNvbHVtbi1jb3VudDogMjtcclxuLy8gICAtbW96LWNvbHVtbi1jb3VudDogMjtcclxuLy8gICBjb2x1bW4tY291bnQ6IDI7XHJcblxyXG4vLyAgIC13ZWJraXQtY29sdW1uLWdhcDogMTVweDtcclxuLy8gICAtbW96LWNvbHVtbi1nYXA6IDE1cHg7XHJcbi8vICAgY29sdW1uLWdhcDogMTVweDtcclxuLy8gfVxyXG5cclxuLm1hc29ucnktaXRlbSB7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIHdpZHRoOiAzMy4zM3Z3O1xyXG4gIGhlaWdodDogMjIuMjJ2aDtcclxuICBwYWRkaW5nOiAxcHg7XHJcbn1cclxuXHJcbi8vIC5tYXNvbnJ5LWl0ZW0gaW1nIHtcclxuLy8gICBkaXNwbGF5OmJsb2NrO1xyXG4vLyAgIHdpZHRoOiAxMDAlO1xyXG4vLyB9XHJcbiJdfQ== */");

/***/ }),

/***/ 18045:
/*!************************************************************!*\
  !*** ./src/app/pages/gallery/photos/photos.component.scss ***!
  \************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("img {\n  width: 100%;\n  height: 100%;\n}\n\n.opacity-0 {\n  opacity: 0;\n}\n\n.opacity-1 {\n  opacity: 1;\n}\n\n.square {\n  width: 100%;\n  background-position: center center !important;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n\n.square:after {\n  content: \"\";\n  display: block;\n  padding-bottom: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInBob3Rvcy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFVQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0FBVEY7O0FBWUE7RUFDRSxVQUFBO0FBVEY7O0FBWUE7RUFDRSxVQUFBO0FBVEY7O0FBWUE7RUFDRSxXQUFBO0VBQ0EsNkNBQUE7RUFDQSw0QkFBQTtFQUNBLHNCQUFBO0FBVEY7O0FBWUE7RUFDRSxXQUFBO0VBQ0EsY0FBQTtFQUNBLG9CQUFBO0FBVEYiLCJmaWxlIjoicGhvdG9zLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gLm91dGVye1xyXG4vLyAgIGRpc3BsYXk6IGZsZXg7XHJcbi8vICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuLy8gICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuLy8gICB3aWR0aDogMTAwJTs7XHJcbi8vICAgaGVpZ2h0OiAzMy4zM3Z3O1xyXG4vLyAgIC8vIGJhY2tncm91bmQ6IGJsdWU7XHJcbi8vICAgdGV4dC1hbGlnbjogY2VudGVyOztcclxuLy8gfVxyXG5cclxuaW1nIHtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuXHJcbi5vcGFjaXR5LTAge1xyXG4gIG9wYWNpdHk6IDA7XHJcbn1cclxuXHJcbi5vcGFjaXR5LTEge1xyXG4gIG9wYWNpdHk6IDE7XHJcbn1cclxuXHJcbi5zcXVhcmUge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlciBjZW50ZXIgIWltcG9ydGFudDtcclxuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbn1cclxuXHJcbi5zcXVhcmU6YWZ0ZXIge1xyXG4gIGNvbnRlbnQ6IFwiXCI7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgcGFkZGluZy1ib3R0b206IDEwMCU7XHJcbn1cclxuIl19 */");

/***/ }),

/***/ 15269:
/*!**********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/gallery/fullsize-gallery/fullsize-gallery.component.html ***!
  \**********************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header>\r\n  <ion-toolbar>\r\n\r\n    <ion-buttons slot=\"end\">\r\n      <!--  -->\r\n      <button style=\"background: inherit;\" (click)=\"close()\">\r\n        <ion-icon style=\"font-size: 35px;\" name=\"close-outline\" color=\"light\"></ion-icon>\r\n      </button>\r\n    </ion-buttons>\r\n\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-slides #slides [options]=\"slideOpts\">\r\n    <ion-slide *ngFor=\"let img of images\">\r\n      <img [src]=\"img.value\">\r\n    </ion-slide>\r\n  </ion-slides>\r\n</ion-content>");

/***/ }),

/***/ 87239:
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/gallery/gallery.page.html ***!
  \***************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<app-header [title]=\"title\" format=\"png\" [icon]=\"icon\"></app-header>\r\n\r\n<ion-content>\r\n  <img class=\"background-image\" src=\"./../../../assets/imgs/bg.png\">\r\n  <ion-grid style=\"padding: 1px;\" *ngIf=\"!isLoading\">\r\n\r\n    <ion-row>\r\n      <ion-col *ngFor=\"let img of images; let i = index\" size=\"4\" style=\"padding: 1px;\" (click)=\"showImage(i)\">\r\n        <app-photos class=\"img-col\" [image]=\"img.value\"></app-photos>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n  </ion-grid>\r\n  <!-- <div class=\"masonry-container\">\r\n    <div class=\"masonry-item\" *ngFor=\"let img of images\">\r\n      <app-photos class=\"img-col\" [image]=\"img.value\"></app-photos>\r\n    </div>\r\n  </div> -->\r\n\r\n\r\n  <div class=\"spinner\" *ngIf=\"isLoading\">\r\n    <ion-spinner color=\"primary\"></ion-spinner>\r\n  </div>\r\n\r\n</ion-content>\r\n\r\n<contact-us-fab></contact-us-fab>\r\n");

/***/ }),

/***/ 19706:
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/gallery/photos/photos.component.html ***!
  \**************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\r\n<div class=\"square\" [style.background-image]=\"getBgImage()\">\r\n</div>\r\n<!-- <div class=\"outer\" *ngIf=\"!isImageLoaded\" [ngClass]=\"isImageLoaded ? 'opacity-0' : 'opacity-1'\">\r\n  <ion-spinner></ion-spinner>\r\n</div> -->\r\n\r\n<!-- <img [ngClass]=\"isImageLoaded ? 'opacity-1 fade-in' : 'opacity-0'\" [src]=\"image\"\r\n  (load)=\"imageLoaded()\" (click)=\"utility.openImage(image)\" /> -->\r\n");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_gallery_gallery_module_ts.js.map