
  cordova.define('cordova/plugin_list', function(require, exports, module) {
    module.exports = [
      {
          "id": "cordova-plugin-inappbrowser.inappbrowser",
          "file": "plugins/cordova-plugin-inappbrowser/www/inappbrowser.js",
          "pluginId": "cordova-plugin-inappbrowser",
        "clobbers": [
          "cordova.InAppBrowser.open"
        ]
        },
      {
          "id": "cordova-plugin-android-permissions.Permissions",
          "file": "plugins/cordova-plugin-android-permissions/www/permissions.js",
          "pluginId": "cordova-plugin-android-permissions",
        "clobbers": [
          "cordova.plugins.permissions"
        ]
        },
      {
          "id": "uk.co.workingedge.phonegap.plugin.launchnavigator.Common",
          "file": "plugins/uk.co.workingedge.phonegap.plugin.launchnavigator/www/common.js",
          "pluginId": "uk.co.workingedge.phonegap.plugin.launchnavigator",
        "clobbers": [
          "launchnavigator"
        ]
        },
      {
          "id": "uk.co.workingedge.phonegap.plugin.launchnavigator.LocalForage",
          "file": "plugins/uk.co.workingedge.phonegap.plugin.launchnavigator/www/localforage.v1.5.0.min.js",
          "pluginId": "uk.co.workingedge.phonegap.plugin.launchnavigator",
        "clobbers": [
          "localforage"
        ]
        },
      {
          "id": "com-sarriaroman-photoviewer.PhotoViewer",
          "file": "plugins/com-sarriaroman-photoviewer/www/PhotoViewer.js",
          "pluginId": "com-sarriaroman-photoviewer",
        "clobbers": [
          "PhotoViewer"
        ]
        },
      {
          "id": "cordova-plugin-app-launcher.Launcher",
          "file": "plugins/cordova-plugin-app-launcher/www/Launcher.js",
          "pluginId": "cordova-plugin-app-launcher",
        "clobbers": [
          "plugins.launcher"
        ]
        },
      {
          "id": "uk.co.workingedge.phonegap.plugin.launchnavigator.LaunchNavigator",
          "file": "plugins/uk.co.workingedge.phonegap.plugin.launchnavigator/www/android/launchnavigator.js",
          "pluginId": "uk.co.workingedge.phonegap.plugin.launchnavigator",
        "merges": [
          "launchnavigator"
        ]
        }
    ];
    module.exports.metadata =
    // TOP OF METADATA
    {
      "com-sarriaroman-photoviewer": "1.2.4",
      "cordova-plugin-android-permissions": "1.1.2",
      "cordova-plugin-app-launcher": "0.4.0",
      "cordova-plugin-inappbrowser": "5.0.0",
      "uk.co.workingedge.phonegap.plugin.launchnavigator": "5.0.6"
    };
    // BOTTOM OF METADATA
    });
    