(self["webpackChunkSaudi_Flag_App"] = self["webpackChunkSaudi_Flag_App"] || []).push([["src_app_pages_dashboard_dashboard_module_ts"],{

/***/ 3601:
/*!*************************************************************!*\
  !*** ./src/app/pages/dashboard/dashboard-routing.module.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DashboardPageRoutingModule": () => (/* binding */ DashboardPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 1855);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 2741);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9535);
/* harmony import */ var _dashboard_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./dashboard.page */ 2268);




const routes = [
    {
        path: '',
        component: _dashboard_page__WEBPACK_IMPORTED_MODULE_0__.DashboardPage
    }
];
let DashboardPageRoutingModule = class DashboardPageRoutingModule {
};
DashboardPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], DashboardPageRoutingModule);



/***/ }),

/***/ 9412:
/*!*****************************************************!*\
  !*** ./src/app/pages/dashboard/dashboard.module.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DashboardPageModule": () => (/* binding */ DashboardPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 1855);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 2741);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ 6274);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 3324);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 4595);
/* harmony import */ var _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./dashboard-routing.module */ 3601);
/* harmony import */ var _dashboard_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./dashboard.page */ 2268);
/* harmony import */ var src_app_components_shared_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/components/shared.module */ 6369);








let DashboardPageModule = class DashboardPageModule {
};
DashboardPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_5__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.IonicModule,
            src_app_components_shared_module__WEBPACK_IMPORTED_MODULE_2__.SharedModule,
            _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_0__.DashboardPageRoutingModule
        ],
        declarations: [_dashboard_page__WEBPACK_IMPORTED_MODULE_1__.DashboardPage]
    })
], DashboardPageModule);



/***/ }),

/***/ 2268:
/*!***************************************************!*\
  !*** ./src/app/pages/dashboard/dashboard.page.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DashboardPage": () => (/* binding */ DashboardPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 1855);
/* harmony import */ var _raw_loader_dashboard_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./dashboard.page.html */ 419);
/* harmony import */ var _dashboard_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./dashboard.page.scss */ 4494);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 2741);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../base-page/base-page */ 2168);





let DashboardPage = class DashboardPage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injector) {
        super(injector);
        this.isLoading = true;
        this.lang = "en";
    }
    ngOnInit() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            yield this.getData();
        });
    }
    goBack() {
        this.nav.pop();
    }
    getData() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            // this.network.getDashboardData().then((res) => {
            //   console.log(res);
            //   this.data = res;
            //   let obj = res.find((x) => x.key == "saudi_flag");
            //   this.title = obj.value;
            //   console.log(this.title);
            // })
            this.lang = yield this.storageService.get('lang');
            const key = 'dashboard';
            this.data = this.jsonService.getJsonData(key);
            let obj = this.data.find((x) => x.key == "saudi_flag");
            this.title = obj.value;
            console.log(this.title);
            this.isLoading = false;
        });
    }
    goTo(item) {
        this.nav.navigateTo('pages/' + item.key, { queryParams: { title: item.value, icon: item.key } });
    }
};
DashboardPage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.Injector }
];
DashboardPage = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Component)({
        selector: 'app-dashboard',
        template: _raw_loader_dashboard_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_dashboard_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], DashboardPage);



/***/ }),

/***/ 4494:
/*!*****************************************************!*\
  !*** ./src/app/pages/dashboard/dashboard.page.scss ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".app-header .header-img {\n  width: 75px;\n  height: 50px;\n  object-fit: contain;\n  margin: 15px 0px;\n}\n.app-header .back-img {\n  width: 40px;\n}\n.app-header ion-toolbar {\n  --background: transparent;\n  color: white;\n  position: fixed;\n  --border-width: 0;\n}\n.app-header .top-heading {\n  font-size: 1.2em;\n  padding-bottom: 10px;\n  font-weight: bold !important;\n  text-transform: uppercase;\n  padding-left: 15px;\n}\nion-content {\n  --background: #f5fcf7;\n}\nion-content ion-col {\n  display: flex;\n  justify-content: center;\n}\nion-content .dash-button {\n  --border-radius: 12px;\n  border-radius: 12px;\n  margin: 3px 0;\n  height: 11vh;\n  font-size: 20px;\n  font-weight: bolder;\n  box-shadow: 1px 5px 6px 0px rgba(0, 0, 0, 0.3);\n  text-transform: capitalize;\n  --padding-end: 9px;\n  --padding-start: 9px;\n  width: 75%;\n}\nion-content .dash-button .btn-icon {\n  width: 50px;\n  height: 50px;\n}\nion-content .dash-button .btn-text {\n  text-align: start;\n  font-size: 1.15em;\n}\n.text-margin-ar {\n  margin-left: auto;\n  margin-right: 10px;\n}\n.text-margin-en {\n  margin-right: auto;\n  margin-left: 10px;\n}\n.card {\n  border-radius: 12px;\n}\n.card ion-item {\n  --border-radius: 12px;\n}\n.card-content {\n  font-weight: 900;\n  font-size: 1.3em;\n}\n.db-button {\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  justify-content: center;\n  --background: #fff !important;\n  width: 95%;\n  min-width: 140px;\n  height: 20vh;\n  box-shadow: 0px 23px 25px 0px rgba(5, 46, 8, 0.07);\n  margin: 3px 0;\n  --border-radius: 8px;\n  border-radius: 8px;\n}\n.db-button .btn-icon {\n  width: 50px;\n}\n.db-button .btn-text {\n  font-size: 18px;\n  font-weight: bolder;\n  text-transform: capitalize;\n  margin: 8px 0;\n  line-height: 1.2;\n  white-space: initial;\n  color: #006c32;\n}\n.flag-banner {\n  display: block;\n  padding: 0;\n  background-color: #f5fcf7;\n}\n.flag-banner .header {\n  display: block;\n  width: 100%;\n  min-height: 230px;\n  margin-bottom: 20px;\n  background: url('bg-curve3.png') no-repeat;\n  background-size: cover;\n  background-position: bottom center;\n  text-align: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImRhc2hib2FyZC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBY0U7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7QUFiSjtBQWdCRTtFQUNFLFdBQUE7QUFkSjtBQWlCRTtFQUNFLHlCQUFBO0VBRUEsWUFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQWhCSjtBQXdCRTtFQUNFLGdCQUFBO0VBQ0Esb0JBQUE7RUFDQSw0QkFBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7QUF0Qko7QUEwQkE7RUFDRSxxQkFBQTtBQXZCRjtBQTZCRTtFQUNFLGFBQUE7RUFDQSx1QkFBQTtBQTNCSjtBQThCRTtFQUNFLHFCQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtFQUNBLDhDQUFBO0VBQ0EsMEJBQUE7RUFDQSxrQkFBQTtFQUNBLG9CQUFBO0VBQ0EsVUFBQTtBQTVCSjtBQThCSTtFQUNFLFdBQUE7RUFDQSxZQUFBO0FBNUJOO0FBK0JJO0VBQ0UsaUJBQUE7RUFFQSxpQkFBQTtBQTlCTjtBQW1DQTtFQUNFLGlCQUFBO0VBQ0Esa0JBQUE7QUFoQ0Y7QUFtQ0E7RUFDRSxrQkFBQTtFQUNBLGlCQUFBO0FBaENGO0FBbUNBO0VBQ0UsbUJBQUE7QUFoQ0Y7QUFrQ0U7RUFDRSxxQkFBQTtBQWhDSjtBQW9DQTtFQUNFLGdCQUFBO0VBQ0EsZ0JBQUE7QUFqQ0Y7QUFxQ0E7RUFDRSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsNkJBQUE7RUFDQSxVQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0VBQ0Esa0RBQUE7RUFDQSxhQUFBO0VBQ0Esb0JBQUE7RUFDQSxrQkFBQTtBQWxDRjtBQW1DRTtFQUNFLFdBQUE7QUFqQ0o7QUFtQ0U7RUFDRSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSwwQkFBQTtFQUNBLGFBQUE7RUFDQSxnQkFBQTtFQUNBLG9CQUFBO0VBQ0EsY0FBQTtBQWpDSjtBQXdEQTtFQUNFLGNBQUE7RUFDQSxVQUFBO0VBQ0EseUJBQUE7QUFyREY7QUF3REE7RUFDRSxjQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSwwQ0FBQTtFQUNBLHNCQUFBO0VBQ0Esa0NBQUE7RUFDQSxrQkFBQTtBQXJERiIsImZpbGUiOiJkYXNoYm9hcmQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gLnRvcGhlYWRlcntcclxuLy8gICAvLyBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbi8vICAgLy8gZGlzcGxheTogYmxvY2s7XHJcbi8vICAgLy8gd2lkdGg6IDEwMCU7XHJcbi8vICAgLy8gaGVpZ2h0OiBhdXRvOztcclxuLy8gICAvLyBiYWNrZ3JvdW5kOiBibHVlO1xyXG4vLyAgIC8vIGhlaWdodDogMTQwcHg7XHJcbi8vIH1cclxuXHJcbi5hcHAtaGVhZGVyIHtcclxuICAvLyBpb24tdG9vbGJhciB7XHJcbiAgLy8gICAgIC8vIGhlaWdodDogNjVweDtcclxuICAvLyB9XHJcblxyXG4gIC5oZWFkZXItaW1nIHtcclxuICAgIHdpZHRoOiA3NXB4O1xyXG4gICAgaGVpZ2h0OiA1MHB4O1xyXG4gICAgb2JqZWN0LWZpdDogY29udGFpbjtcclxuICAgIG1hcmdpbjogMTVweCAwcHg7XHJcbiAgfVxyXG5cclxuICAuYmFjay1pbWcge1xyXG4gICAgd2lkdGg6IDQwcHg7XHJcbiAgfVxyXG5cclxuICBpb24tdG9vbGJhciB7XHJcbiAgICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG4gICAgLy8gYm94LXNoYWRvdzogMHB4IDVweCA1cHggMHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICAtLWJvcmRlci13aWR0aDogMDtcclxuICB9XHJcblxyXG4gIC8vIGlvbi10aXRsZSB7XHJcbiAgLy8gICAgIC8vIHBhZGRpbmctbGVmdDogOXB4O1xyXG4gIC8vICAgICAvLyBmb250LXdlaWdodDogYm9sZGVyO1xyXG4gIC8vIH1cclxuXHJcbiAgLnRvcC1oZWFkaW5nIHtcclxuICAgIGZvbnQtc2l6ZTogMS4yZW07XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMTBweDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkICFpbXBvcnRhbnQ7XHJcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgcGFkZGluZy1sZWZ0OiAxNXB4O1xyXG4gIH1cclxufVxyXG5cclxuaW9uLWNvbnRlbnQge1xyXG4gIC0tYmFja2dyb3VuZDogI2Y1ZmNmNztcclxuXHJcbiAgLy8gLmRhc2gtYnV0dG9uLXJvdyB7XHJcbiAgLy8gICAgIGhlaWdodDogMTV2aDtcclxuICAvLyB9XHJcblxyXG4gIGlvbi1jb2wge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIH1cclxuXHJcbiAgLmRhc2gtYnV0dG9uIHtcclxuICAgIC0tYm9yZGVyLXJhZGl1czogMTJweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDEycHg7XHJcbiAgICBtYXJnaW46IDNweCAwO1xyXG4gICAgaGVpZ2h0OiAxMXZoO1xyXG4gICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcclxuICAgIGJveC1zaGFkb3c6IDFweCA1cHggNnB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMyk7XHJcbiAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuICAgIC0tcGFkZGluZy1lbmQ6IDlweDtcclxuICAgIC0tcGFkZGluZy1zdGFydDogOXB4O1xyXG4gICAgd2lkdGg6IDc1JTtcclxuXHJcbiAgICAuYnRuLWljb24ge1xyXG4gICAgICB3aWR0aDogNTBweDtcclxuICAgICAgaGVpZ2h0OiA1MHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5idG4tdGV4dCB7XHJcbiAgICAgIHRleHQtYWxpZ246IHN0YXJ0O1xyXG4gICAgICAvLyB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gICAgICBmb250LXNpemU6IDEuMTVlbTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbi50ZXh0LW1hcmdpbi1hciB7XHJcbiAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG59XHJcblxyXG4udGV4dC1tYXJnaW4tZW4ge1xyXG4gIG1hcmdpbi1yaWdodDogYXV0bztcclxuICBtYXJnaW4tbGVmdDogMTBweDtcclxufVxyXG5cclxuLmNhcmQge1xyXG4gIGJvcmRlci1yYWRpdXM6IDEycHg7XHJcblxyXG4gIGlvbi1pdGVtIHtcclxuICAgIC0tYm9yZGVyLXJhZGl1czogMTJweDtcclxuICB9XHJcbn1cclxuXHJcbi5jYXJkLWNvbnRlbnQge1xyXG4gIGZvbnQtd2VpZ2h0OiA5MDA7XHJcbiAgZm9udC1zaXplOiAxLjNlbTtcclxufVxyXG5cclxuLy8gMm5kIGFuZCAzcmQgVGhlbWUgY3NzXHJcbi5kYi1idXR0b24ge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIC0tYmFja2dyb3VuZDogI2ZmZiAhaW1wb3J0YW50O1xyXG4gIHdpZHRoOiA5NSU7XHJcbiAgbWluLXdpZHRoOiAxNDBweDtcclxuICBoZWlnaHQ6IDIwdmg7XHJcbiAgYm94LXNoYWRvdzogMHB4IDIzcHggMjVweCAwcHggcmdiYSg1LCA0NiwgOCwgMC4wNyk7XHJcbiAgbWFyZ2luOiAzcHggMDtcclxuICAtLWJvcmRlci1yYWRpdXM6IDhweDtcclxuICBib3JkZXItcmFkaXVzOiA4cHg7XHJcbiAgLmJ0bi1pY29uIHtcclxuICAgIHdpZHRoOiA1MHB4O1xyXG4gIH1cclxuICAuYnRuLXRleHQge1xyXG4gICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcclxuICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG4gICAgbWFyZ2luOiA4cHggMDtcclxuICAgIGxpbmUtaGVpZ2h0OiAxLjI7XHJcbiAgICB3aGl0ZS1zcGFjZTogaW5pdGlhbDtcclxuICAgIGNvbG9yOiAjMDA2YzMyO1xyXG4gIH1cclxufVxyXG5cclxuLy8gNHRoIFRoZW1lIGNzc1xyXG4vLyAudGhlbWUtNCAuY29sOm50aC1jaGlsZChldmVuKXtcclxuLy8gICAgIHBhZGRpbmc6IDAgMTBweCAwIDA7XHJcbi8vIH1cclxuLy8gLnRoZW1lLTQgLmNvbDpudGgtY2hpbGQob2RkKXtcclxuLy8gICAgIHBhZGRpbmc6IDAgMCAwIDEwcHg7XHJcbi8vIH1cclxuLy8gLnRoZW1lLTQgLmNvbCAuZGItYnV0dG9ue1xyXG4vLyAgICAgd2lkdGg6IDEwMCU7XHJcbi8vICAgICAtLWJvcmRlci1yYWRpdXM6IDA7XHJcbi8vICAgICBib3JkZXItcmFkaXVzOiAwO1xyXG4vLyAgICAgYm94LXNoYWRvdzogMHB4IDIzcHggMzJweCAwcHggcmdiYSg1LCA0NiwgOCwgMC4wNyk7XHJcbi8vICAgICBtYXJnaW46IDA7XHJcbi8vICAgICBwYWRkaW5nOiAwO1xyXG4vLyAgICAgYm9yZGVyOiAxcHggc29saWQgd2hpdGU7XHJcbi8vIH1cclxuXHJcbi8vIEZsYWcgQmFubmVyIGNzc1xyXG5cclxuLmZsYWctYmFubmVyIHtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBwYWRkaW5nOiAwO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmNWZjZjc7XHJcbn1cclxuXHJcbi5mbGFnLWJhbm5lciAuaGVhZGVyIHtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICB3aWR0aDogMTAwJTtcclxuICBtaW4taGVpZ2h0OiAyMzBweDtcclxuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG4gIGJhY2tncm91bmQ6IHVybChcIi4vLi4vLi4vLi4vYXNzZXRzL2ltZ3MvYmctY3VydmUzLnBuZ1wiKSBuby1yZXBlYXQ7XHJcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBib3R0b20gY2VudGVyO1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4vLyAuaGVhZGVyIC5iYW5uZXItbG9nb3tcclxuLy8gICAgIG1heC13aWR0aDogNjUlO1xyXG4vLyAgICAgbWFyZ2luLWJvdHRvbTogMjVweDtcclxuLy8gfVxyXG4vLyAuaGVhZGVyIC5jb250ZW50e1xyXG4vLyAgICAgZGlzcGxheTogZmxleDtcclxuLy8gICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuLy8gICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbi8vIH1cclxuLy8gLmhlYWRlciAuZ3JlZXRpbmd7XHJcbi8vICAgICBmb250LXNpemU6IDE4cHg7XHJcbi8vICAgICBmb250LXdlaWdodDogNDAwO1xyXG4vLyAgICAgZm9udC1zdHlsZTogaXRhbGljO1xyXG4vLyAgICAgY29sb3I6ICNmZmY7XHJcbi8vICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4vLyAgICAgbGluZS1oZWlnaHQ6IDE7XHJcbi8vICAgICBtYXJnaW4tYm90dG9tOiA4cHg7XHJcbi8vICAgICBtYXJnaW4tdG9wOiAwO1xyXG4vLyB9XHJcbi8vIC5oZWFkZXIgLm5hbWV7XHJcbi8vICAgICBmb250LXNpemU6IDIycHg7XHJcbi8vICAgICBmb250LXdlaWdodDogNjAwO1xyXG4vLyAgICAgZm9udC1zdHlsZTogaXRhbGljO1xyXG4vLyAgICAgY29sb3I6ICNmZmY7XHJcbi8vICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4vLyAgICAgbGluZS1oZWlnaHQ6IDE7XHJcbi8vICAgICBtYXJnaW46IDA7XHJcbi8vIH1cclxuLy8gLmhlYWRlciAud2VhdGhlcntcclxuLy8gICAgIGRpc3BsYXk6IGZsZXg7XHJcbi8vICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4vLyAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuLy8gICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuLy8gfVxyXG4vLyAuaGVhZGVyIC53ZWF0aGVyIC5pY29ue1xyXG4vLyAgICAgd2lkdGg6IDUwcHg7XHJcbi8vICAgICBtYXJnaW4tYm90dG9tOiA4cHg7XHJcbi8vIH1cclxuLy8gLmhlYWRlciAud2VhdGhlciAudGVtcHtcclxuLy8gICAgIGZvbnQtc2l6ZTogMThweDtcclxuLy8gICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbi8vICAgICBjb2xvcjogI2ZmZjtcclxuLy8gICAgIGxpbmUtaGVpZ2h0OiAxO1xyXG4vLyAgICAgbWFyZ2luOiAwIDAgNXB4O1xyXG4vLyB9XHJcbi8vIC5oZWFkZXIgLndlYXRoZXIgLmZvcmVjYXN0e1xyXG4vLyAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4vLyAgICAgY29sb3I6ICNmZmY7XHJcbi8vICAgICBsaW5lLWhlaWdodDogMTtcclxuLy8gICAgIG1hcmdpbjogMDtcclxuLy8gfVxyXG4vLyAuZmxhZy1iYW5uZXIgLmRlc2N7XHJcbi8vICAgICBwYWRkaW5nOiAwIDIwcHggMjBweDtcclxuLy8gICAgIGZvbnQtc2l6ZTogMThweDtcclxuLy8gICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbi8vICAgICBjb2xvcjogIzAwMDtcclxuLy8gICAgIGxpbmUtaGVpZ2h0OiAxO1xyXG4vLyB9XHJcblxyXG4vLyAudGhlbWUtNXtcclxuLy8gICAgIHBhZGRpbmc6IDAgMTVweDtcclxuLy8gfVxyXG4vLyAudGhlbWUtNSAuY29sIC5uZXctYnV0dG9ue1xyXG4vLyAgICAgZGlzcGxheTogZmxleDtcclxuLy8gICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbi8vICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4vLyAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbi8vICAgICB3aWR0aDogMTAwJTtcclxuLy8gICAgIG1pbi1oZWlnaHQ6IDEyMHB4O1xyXG4vLyAgICAgLS1ib3JkZXItcmFkaXVzOiAwO1xyXG4vLyAgICAgYm9yZGVyLXJhZGl1czogMDtcclxuLy8gICAgIGJveC1zaGFkb3c6IG5vbmU7XHJcbi8vICAgICAtLWJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcclxuLy8gICAgIG1hcmdpbjogMDtcclxuLy8gICAgIHBhZGRpbmc6IDA7XHJcbi8vICAgICBib3JkZXItbGVmdDogM3B4IHNvbGlkICNmMWYxZjE7XHJcbi8vICAgICBib3JkZXItcmlnaHQ6IDNweCBzb2xpZCAjZjFmMWYxO1xyXG4vLyAgICAgYm9yZGVyLXRvcDogM3B4IHNvbGlkICNmMWYxZjE7XHJcbi8vICAgICBib3JkZXItYm90dG9tOiAzcHggc29saWQgI2YxZjFmMTtcclxuLy8gICAgIC0tYmFja2dyb3VuZDogI2ZmZiAhaW1wb3J0YW50O1xyXG4vLyB9XHJcbi8vIC50aGVtZS01IC5jb2x7XHJcbi8vICAgICBwYWRkaW5nOiAwO1xyXG4vLyB9XHJcblxyXG4vLyAuYnRuLWljb257XHJcbi8vICAgICB3aWR0aDogNjBweDtcclxuLy8gfVxyXG4vLyAuYnRuLXRleHR7XHJcbi8vICAgICBmb250LXNpemU6IDE1cHg7XHJcbi8vICAgICBmb250LXdlaWdodDogNjAwO1xyXG4vLyAgICAgY29sb3I6ICMwMDZkNGU7XHJcbi8vICAgICBsaW5lLWhlaWdodDogMTtcclxuLy8gICAgIG1hcmdpbjogNXB4IDA7XHJcbi8vIH1cclxuIl19 */");

/***/ }),

/***/ 419:
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/dashboard/dashboard.page.html ***!
  \*******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!-- [class.text-left]=\"lang === 'en'\" -->\r\n<ion-header class=\"app-header\">\r\n  <ion-toolbar>\r\n    <!-- <img [hidden]=\"!icon\" class=\"fade-in header-img\" [src]=\"'assets/icon/' + icon + '.' + format\" slot=\"start\"> -->\r\n    <!-- <ion-title [hidden]=\"!title\" class=\"top-heading fade-in\" [class.text-left]=\"lang === 'en'\" slot=\"start\"\r\n      [class.text-right]=\"lang === 'ar'\"></ion-title> -->\r\n    <ion-buttons slot=\"end\">\r\n      <ion-button (click)=\"goBack()\">\r\n        <img class=\"back-img\" src=\"assets/icon/back.png\" />\r\n      </ion-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <!-- <img class=\"background-image\" src=\"./../../../assets/imgs/bg.png\">\r\n  <ion-grid *ngIf=\"!isLoading\" class=\"ion-margin-vertical\">\r\n    <ion-row class=\"dash-button-row\" *ngFor=\"let item of data;let i = index\">\r\n      <ion-col size=\"12\" *ngIf=\"i <= 5\">\r\n        <ion-button (click)=\"goTo(item)\" class=\"dash-button flip-in-hor-bottom\">\r\n          <img class=\"btn-icon\" [src]=\"'assets/icon/' + item.key + '.png'\">\r\n          <p class=\"btn-text\"  [ngClass]=\"lang == 'ar' ? 'text-margin-ar' : 'text-margin-en'\">\r\n            {{item?.value}}\r\n          </p>\r\n        </ion-button>\r\n\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n  </ion-grid> -->\r\n\r\n  <!-- Theme 3 -->\r\n  <ion-grid class=\"flag-banner\" *ngIf=\"!isLoading\">\r\n    <ion-row>\r\n      <ion-col size=\"12\" class=\"header\"></ion-col>\r\n    </ion-row>\r\n    <ion-row class=\"ion-no-padding dash-button-row\">\r\n      <ion-col\r\n        *ngFor=\"let item of data;let i = index\"\r\n        [size]=\"6\"\r\n        [hidden]=\"i == 0\"\r\n        class=\"ion-no-padding\"\r\n      >\r\n        <ion-button\r\n          *ngIf=\"i <= 4\"\r\n          color=\"primary2\"\r\n          (click)=\"goTo(item)\"\r\n          class=\"db-button fade-in-top\"\r\n        >\r\n          <div>\r\n            <img\r\n              class=\"btn-icon\"\r\n              [src]=\"'assets/icon/' + item.key + '-green' + '.png'\"\r\n            />\r\n            <p class=\"btn-text\">{{item?.value}}</p>\r\n          </div>\r\n        </ion-button>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n    <ion-row class=\"ion-no-padding dash-button-row ion-justify-content-center\">\r\n      <ion-col class=\"ion-no-padding\" size=\"6\">\r\n        <ion-button\r\n          color=\"primary2\"\r\n          (click)=\"goTo(data[5])\"\r\n          class=\"db-button fade-in-top\"\r\n        >\r\n          <div>\r\n            <img\r\n              class=\"btn-icon\"\r\n              [src]=\"'assets/icon/' + data[5].key + '-green' + '.png'\"\r\n            />\r\n            <p class=\"btn-text\">{{data[5]?.value}}</p>\r\n          </div>\r\n        </ion-button>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n\r\n  <div class=\"spinner\" *ngIf=\"isLoading\">\r\n    <ion-spinner color=\"primary\"></ion-spinner>\r\n  </div>\r\n</ion-content>\r\n");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_dashboard_dashboard_module_ts.js.map