import { Component } from '@angular/core';
import { EventsService } from './services/events.service';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { SqliteService } from './services/sqlite.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

  dir = "ltr";

  constructor(
    private events: EventsService,
    private androidPermissions: AndroidPermissions,
    private sqlite: SqliteService
  ) {
    this.initialize().then(() => {
      this.events.subscribe('language:change', (lang) => {
        if (lang == "en") {
          this.dir = "ltr"
        } else if (lang == "ar") {
          this.dir = "rtl"
        }
      })
    })

  }

  initialize() {
    return new Promise(async resolve => {
      await this.askForPermissions();
      await this.sqlite.initialize();
    })
  }

  askForPermissions() {
    return new Promise(async (resolve) => {
      this.androidPermissions.checkPermission(
        this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE
      ).then((async (result) => {
        if (result.hasPermission) {
          resolve(true);
        } else {
          this.androidPermissions.requestPermission(
            this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE
          ).then((result) => {
            if (result.hasPermission) {
              resolve(true);
            } else {
              this.exitApp();
            }
          })
        }
      }))
    })
  }

  exitApp() {
    navigator['app'].exitApp();
  }
}
