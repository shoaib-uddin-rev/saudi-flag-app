import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from 'src/app/pages/base-page/base-page';

@Component({
  selector: 'app-milestone-detail',
  templateUrl: './milestone-detail.component.html',
  styleUrls: ['./milestone-detail.component.scss'],
})
export class MilestoneDetailComponent extends BasePage implements OnInit {

  item: any;
  lang: any;

  constructor(
    injector: Injector
  ) {
    super(injector)
    this.lang = localStorage.getItem('lang');
  }

  ngOnInit() {
    console.log(this.item);
  }

  close() {
    this.modals.dismiss();
  }
}
