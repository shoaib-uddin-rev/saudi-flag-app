import { Component, Injector, Input, OnInit } from '@angular/core';
import { BasePage } from 'src/app/pages/base-page/base-page';

@Component({
  selector: 'app-flag-rule-des',
  templateUrl: './flag-rule-des.component.html',
  styleUrls: ['./flag-rule-des.component.scss'],
})
export class FlagRuleDesComponent extends BasePage implements OnInit {

  @Input() lang = 'en';
  rule;
  item;
  isLoading = true;
  isNetworkOngoing = false;

  constructor(
    injector: Injector
  ) {
    super(injector);
    this.lang = localStorage.getItem('lang');
  }

  async ngOnInit() {
    await this.initialize();
    this.sync()
  }

  async sync() {
    if (!this.isNetworkOngoing) {
      await this.sqlite.syncFlagRule(this.rule.key);
      this.isNetworkOngoing = false;
    }
  }

  async initialize() {
    this.item = await this.sqlite.getFlagRulesData(this.lang, this.rule.key);
    // console.log(data);
    // this.item = this.jsonService.getFlagRulesDesc(this.rule.key);
    console.log(this.item);
    this.isLoading = false;
  }

  close() {
    this.modals.dismiss();
  }
}
