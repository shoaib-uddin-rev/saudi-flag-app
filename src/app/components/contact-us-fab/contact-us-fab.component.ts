import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from 'src/app/pages/base-page/base-page';
import { ContactUsPage } from 'src/app/pages/contact-us/contact-us.page';

@Component({
  selector: 'contact-us-fab',
  templateUrl: './contact-us-fab.component.html',
  styleUrls: ['./contact-us-fab.component.scss'],
})
export class ContactUsFabComponent extends BasePage implements OnInit {
  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {}

  openContactUs() {
    this.modals.present(ContactUsPage);
  }
}
