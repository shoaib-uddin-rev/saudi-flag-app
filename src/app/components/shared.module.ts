import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { HeaderComponent } from './header/header.component';
import { MilestoneDetailComponent } from './milestone-detail/milestone-detail.component';
import { FlagRuleDesComponent } from './flag-rule-des/flag-rule-des.component';
import { ContactUsFabComponent } from './contact-us-fab/contact-us-fab.component';

@NgModule({
    declarations: [
        HeaderComponent,
        MilestoneDetailComponent,
        FlagRuleDesComponent,
        ContactUsFabComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        IonicModule,
    ],
    exports: [
        HeaderComponent,
        MilestoneDetailComponent,
        FlagRuleDesComponent,
        ContactUsFabComponent
    ]
})
export class SharedModule { }
