import { Component, Injector, Input, OnInit } from '@angular/core';
import { BasePage } from 'src/app/pages/base-page/base-page';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent extends BasePage implements OnInit {

  @Input() title = '';
  @Input() icon = '';
  @Input() format = 'png';
  @Input() lang = 'en';
  @Input() isModal = false;
  @Input() assetLink = 'icon';


  constructor(injector: Injector) {
    super(injector);
    this.lang = localStorage.getItem('lang');
  }

  ngOnInit() {}

  goBack() {
    if(this.isModal){
      this.modals.dismiss();
    }else{
      this.nav.pop();
    }

  }
}
