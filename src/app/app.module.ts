import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { InterceptorService } from './services/interceptor.service';
import { NgxPubSubService } from '@pscoped/ngx-pub-sub';
import { ApiService } from './services/api.service';
import { StorageService } from './services/basic/storage.service';
import { EventsService } from './services/events.service';
import { NetworkService } from './services/network.service';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { AppLauncher, AppLauncherOptions } from '@ionic-native/app-launcher/ngx';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { SimplebarAngularModule } from 'simplebar-angular';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, HttpClientModule, SimplebarAngularModule],
  providers: [
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true },
    NgxPubSubService,
    EventsService,
    StorageService,
    ApiService,
    NetworkService,
    InAppBrowser,
    AppLauncher,
    LaunchNavigator,
    AndroidPermissions
    // PhotoViewer
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
