import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage extends BasePage implements OnInit {
  data: any[];
  title: any;
  isLoading = true;
  lang = 'en';

  constructor(injector: Injector) {
    super(injector);
  }

  async ngOnInit() {
    await this.getData();
  }

  goBack() {
    this.nav.pop();
  }

  async getData() {
    this.network.getDashboardData().then(async (res) => {
      console.log(res);
      this.data = res;

      let objFlag = res.find((x) => x.key == 'saudi_flag');
      this.title = objFlag.value;
      console.log(this.title);
      this.lang = await this.storageService.get('lang');
      const key = 'dashboard';
      // this.data = this.jsonService.getJsonData(key);
      let obj = this.data.find((x: { key: string }) => x.key == 'saudi_flag');
      this.title = obj.value;
      console.log(this.title);
      this.isLoading = false;
    });
  }

  goTo(item: { key: string; value: any }) {
    this.nav.navigateTo('pages/' + item.key, {
      queryParams: { title: item.value, icon: item.key },
    });
  }
}
