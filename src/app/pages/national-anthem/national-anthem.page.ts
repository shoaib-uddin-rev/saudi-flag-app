import { Component, Injector, OnDestroy, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-national-anthem',
  templateUrl: './national-anthem.page.html',
  styleUrls: ['./national-anthem.page.scss'],
})
export class NationalAnthemPage extends BasePage implements OnInit, OnDestroy {

  title: any;
  icon: any;
  isPlaying = false;
  lang = '';
  timelaspse = -1;
  timer_ids = [];
  interval_times_en = [
    0,
    10000,
    14000,
    18000,
    23000,
    29000
  ]
  interval_times_ar = [
    0,
    10000,
    15000,
    17000,
    23000,
    29000
  ]

  constructor(
    injector: Injector
  ) {
    super(injector)
    this.audioService.preload();
    this.storageService.get('lang').then((x) => {
      this.lang = x;
    })
  }

  ngOnInit() {
    this.title = this.nav.getQueryParams().title;
    this.icon = this.nav.getQueryParams().icon;
    console.log(this.title);
  }

  ngOnDestroy(): void {
    if (this.isPlaying) {
      this.playPause();
    }
  }

  playPause() {
    if (!this.isPlaying) {
      this.audioService.play();

      let array = [];

      if (this.lang == 'en') {
        array = this.interval_times_en;
      } else if (this.lang == 'ar') {
        array = this.interval_times_ar;
      }

      array.forEach(element => {
        const id = setTimeout(() => {
          this.timelaspse++;

          if (this.timelaspse >= 5) {
            this.clearTimeout();
            this.playPause();
            return;
          }

        }, element);

        this.timer_ids.push(id);
      });


    } else if (this.isPlaying) {
      this.audioService.pause();
      this.clearTimeout();
      this.timelaspse = -1;
    }
    console.log("here");
    this.isPlaying = !this.isPlaying;
  }

  clearTimeout() {
    this.timer_ids.forEach(element => {
      if (element) {
        clearTimeout(element);
      }
    });
  }
}
