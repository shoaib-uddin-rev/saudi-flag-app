import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GalleryPageRoutingModule } from './gallery-routing.module';

import { GalleryPage } from './gallery.page';
import { SharedModule } from 'src/app/components/shared.module';
// import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { PhotosComponent } from './photos/photos.component';
import { FullsizeGalleryComponent } from './fullsize-gallery/fullsize-gallery.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GalleryPageRoutingModule,
    SharedModule
  ],
  declarations: [GalleryPage, PhotosComponent, FullsizeGalleryComponent],
  providers: [
    // PhotoViewer,

  ]
})
export class GalleryPageModule { }
