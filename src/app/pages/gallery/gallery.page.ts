import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { BasePage } from '../base-page/base-page';
import { FullsizeGalleryComponent } from './fullsize-gallery/fullsize-gallery.component';
// import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.page.html',
  styleUrls: ['./gallery.page.scss'],
})
export class GalleryPage extends BasePage implements OnInit {

  title: any;
  icon: any;
  images: any;
  isLoading = true;

  constructor(
    injector: Injector,
    // private photoViewer: PhotoViewer
  ) {
    super(injector)
  }

  async ngOnInit() {
    await this.initialize();
  }

  async initialize() {
    this.title = this.nav.getQueryParams().title;
    this.icon = this.nav.getQueryParams().icon;
    this.images = this.jsonService.getJsonData('gallery');;
    localStorage.setItem('nimages', JSON.stringify(this.images));
    this.isLoading = false;
  }

  showImage(i) {
    console.log(i);
    this.modals.present(FullsizeGalleryComponent, {index: i});
  }
}
