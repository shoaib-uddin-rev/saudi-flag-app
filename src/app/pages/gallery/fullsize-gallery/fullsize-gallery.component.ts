import { Component, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { BasePage } from '../../base-page/base-page';

@Component({
  selector: 'app-fullsize-gallery',
  templateUrl: './fullsize-gallery.component.html',
  styleUrls: ['./fullsize-gallery.component.scss'],
})
export class FullsizeGalleryComponent extends BasePage implements OnInit {

  @ViewChild('slides') slides: IonSlides;
  images = [];
  index: number;
  slideOpts = {};

  constructor(
    injector: Injector
  ) {
    super(injector)
    const m = localStorage.getItem('nimages');
    if (m) {
      this.images = JSON.parse(m);
    }
  }

  ngOnInit() {
    this.slideOpts = {
      initialSlide: this.index,
    }
  }

  public ionViewWillEnter() {
    this.slides.update();
  }

  close() {
    this.modals.dismiss();
  }
}
