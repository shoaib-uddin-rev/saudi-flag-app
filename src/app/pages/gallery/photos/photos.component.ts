import { Component, Injector, Input, OnInit } from '@angular/core';
import { BasePage } from '../../base-page/base-page';

@Component({
  selector: 'app-photos',
  templateUrl: './photos.component.html',
  styleUrls: ['./photos.component.scss'],
})
export class PhotosComponent extends BasePage implements OnInit {

  @Input() image: string;

  isImageLoaded = false;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {}

  imageLoaded(){
    this.isImageLoaded = true;
  }

  getBgImage(){
    return `url(${this.image})`
  }

}
