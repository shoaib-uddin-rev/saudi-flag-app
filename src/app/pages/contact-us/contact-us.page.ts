import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.page.html',
  styleUrls: ['./contact-us.page.scss'],
})
export class ContactUsPage extends BasePage implements OnInit {
  data: any;
  feedback;
  responseMsg: any;
  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.init();
  }

  init() {
    this.data = this.jsonService.getJsonData('contact-form');
    this.feedback = {
      description: null,
      email: null,
      feedback: null,
      name: null,
      phoneNumber: null,
    };
    console.log(this.feedback);
  }

  async sendContactUsData() {
    this.responseMsg = this.jsonService.getResponseData('contact-form');
    if (
      this.feedback.age ||
      this.feedback.description ||
      this.feedback.email ||
      this.feedback.feedback ||
      this.feedback.gender ||
      this.feedback.name ||
      this.feedback.phoneNumber
    ) {
      switch (this.jsonService.lang) {
        case 'en':
          console.log('language', this.jsonService.lang);
          this.utility.presentSuccessToast(
            this.responseMsg.successRes,
            'successToastEn'
          );
          break;
        case 'ar':
          console.log('language', this.jsonService.lang);
          this.utility.presentSuccessToast(
            this.responseMsg.successRes,
            'successToastAr'
          );
          break;
        default:
          console.log('language', this.jsonService.lang);
          this.utility.presentSuccessToast(
            this.responseMsg.successRes,
            'successToastEn'
          );
          break;
      }
    } else {
      switch (this.jsonService.lang) {
        case 'en':
          console.log('language', this.jsonService.lang);
          this.utility.presentFailureToast(
            this.responseMsg.failureRes,
            'failureToastEn'
          );
          break;
        case 'ar':
          console.log('language', this.jsonService.lang);
          this.utility.presentFailureToast(
            this.responseMsg.failureRes,
            'successToastAr'
          );
          break;
        default:
          console.log('language', this.jsonService.lang);
          this.utility.presentFailureToast(
            this.responseMsg.failureRes,
            'failureToastEn'
          );
          break;
      }
    }
    // const res = await this.network.postContactUsData(data);
  }
}
