import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';
import { SplashScreen } from '@capacitor/splash-screen';

@Component({
  selector: 'app-splash',
  templateUrl: './splash.page.html',
  styleUrls: ['./splash.page.scss'],
})
export class SplashPage extends BasePage implements OnInit {
  loading = true;
  animation = true;
  data: any;
  saudiFlag;
  language;
  showImage = false;
  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    SplashScreen.hide();
    this.loading = true;

    setTimeout(() => {
      this.animation = false;
    }, 3000);

    this.data = this.jsonService.getSplashData();
    this.saudiFlag = this.data.find((x) => x.key == 'saudi_flag');
    this.language = this.data.find((x) => x.key == 'language');
    this.loading = false;
  }

  goToDashboard(lang) {
    this.storageService.set('lang', lang);
    this.events.publish('language:change', lang);
    this.nav.navigateTo('pages/dashboard');
  }
}
