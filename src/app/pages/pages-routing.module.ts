import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'splash',
    pathMatch: 'full',
  },
  {
    path: 'splash',
    loadChildren: () =>
      import('./splash/splash.module').then((m) => m.SplashPageModule),
  },
  {
    path: 'dashboard',
    loadChildren: () =>
      import('./dashboard/dashboard.module').then((m) => m.DashboardPageModule),
  },
  {
    path: 'milestones',
    loadChildren: () =>
      import('./milestones/milestones.module').then(
        (m) => m.MilestonesPageModule
      ),
  },
  {
    path: 'flag_history',
    loadChildren: () =>
      import('./flag-history/flag-history.module').then(
        (m) => m.FlagHistoryPageModule
      ),
  },
  {
    path: 'flag_rules',
    loadChildren: () =>
      import('./flag-rules/flag-rules.module').then(
        (m) => m.FlagRulesPageModule
      ),
  },
  {
    path: 'mast',
    loadChildren: () =>
      import('./mast/mast.module').then((m) => m.MastPageModule),
  },
  {
    path: 'gallery',
    loadChildren: () =>
      import('./gallery/gallery.module').then((m) => m.GalleryPageModule),
  },
  {
    path: 'national_anthem',
    loadChildren: () =>
      import('./national-anthem/national-anthem.module').then(
        (m) => m.NationalAnthemPageModule
      ),
  },  {
    path: 'contact-us',
    loadChildren: () => import('./contact-us/contact-us.module').then( m => m.ContactUsPageModule)
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {}
