import { ThisReceiver } from '@angular/compiler';
import {
  Component,
  ElementRef,
  Injector,
  OnInit,
  ViewChild,
  Renderer2,
} from '@angular/core';
import { IonContent } from '@ionic/angular';
import { MilestoneDetailComponent } from 'src/app/components/milestone-detail/milestone-detail.component';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-milestones',
  templateUrl: './milestones.page.html',
  styleUrls: ['./milestones.page.scss'],
})
export class MilestonesPage extends BasePage implements OnInit {
  @ViewChild('content') private content: IonContent;
  @ViewChild('scroll', { read: ElementRef }) public scroll: ElementRef<any>;
  @ViewChild('scrolldiv', { read: ElementRef })
  public scrolldiv: ElementRef<any>;
  title: any;
  icon: any;
  data: any = [];
  percent = 0;
  number = 0;
  slide_ended = false;
  timer;
  isLoading = true;
  top = 0;
  lang = 'en';
  isNetworkOngoing = false;
  slideOpts = {
    on: {
      beforeInit() {
        const swiper = this;
        swiper.classNames.push(`${swiper.params.containerModifierClass}flip`);
        swiper.classNames.push(`${swiper.params.containerModifierClass}3d`);
        const overwriteParams = {
          slidesPerView: 1,
          slidesPerColumn: 1,
          slidesPerGroup: 1,
          watchSlidesProgress: true,
          spaceBetween: 0,
          virtualTranslate: true,
        };
        swiper.params = Object.assign(swiper.params, overwriteParams);
        swiper.originalParams = Object.assign(
          swiper.originalParams,
          overwriteParams
        );
      },
      setTranslate() {
        const swiper = this;
        const { $, slides, rtlTranslate: rtl } = swiper;
        for (let i = 0; i < slides.length; i += 1) {
          const $slideEl = slides.eq(i);
          let progress = $slideEl[0].progress;
          if (swiper.params.flipEffect.limitRotation) {
            progress = Math.max(Math.min($slideEl[0].progress, 1), -1);
          }
          const offset$$1 = $slideEl[0].swiperSlideOffset;
          const rotate = -180 * progress;
          let rotateY = rotate;
          let rotateX = 0;
          let tx = -offset$$1;
          let ty = 0;
          if (!swiper.isHorizontal()) {
            ty = tx;
            tx = 0;
            rotateX = -rotateY;
            rotateY = 0;
          } else if (rtl) {
            rotateY = -rotateY;
          }

          $slideEl[0].style.zIndex =
            -Math.abs(Math.round(progress)) + slides.length;

          if (swiper.params.flipEffect.slideShadows) {
            // Set shadows
            let shadowBefore = swiper.isHorizontal()
              ? $slideEl.find('.swiper-slide-shadow-left')
              : $slideEl.find('.swiper-slide-shadow-top');
            let shadowAfter = swiper.isHorizontal()
              ? $slideEl.find('.swiper-slide-shadow-right')
              : $slideEl.find('.swiper-slide-shadow-bottom');
            if (shadowBefore.length === 0) {
              shadowBefore = swiper.$(
                `<div class="swiper-slide-shadow-${swiper.isHorizontal() ? 'left' : 'top'
                }"></div>`
              );
              $slideEl.append(shadowBefore);
            }
            if (shadowAfter.length === 0) {
              shadowAfter = swiper.$(
                `<div class="swiper-slide-shadow-${swiper.isHorizontal() ? 'right' : 'bottom'
                }"></div>`
              );
              $slideEl.append(shadowAfter);
            }
            if (shadowBefore.length)
              shadowBefore[0].style.opacity = Math.max(-progress, 0);
            if (shadowAfter.length)
              shadowAfter[0].style.opacity = Math.max(progress, 0);
          }
          $slideEl.transform(
            `translate3d(${tx}px, ${ty}px, 0px) rotateX(${rotateX}deg) rotateY(${rotateY}deg)`
          );
        }
      },
      setTransition(duration) {
        const swiper = this;
        const { slides, activeIndex, $wrapperEl } = swiper;
        slides
          .transition(duration)
          .find(
            '.swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left'
          )
          .transition(duration);
        if (swiper.params.virtualTranslate && duration !== 0) {
          let eventTriggered = false;
          // eslint-disable-next-line
          slides.eq(activeIndex).transitionEnd(function onTransitionEnd() {
            if (eventTriggered) return;
            if (!swiper || swiper.destroyed) return;

            eventTriggered = true;
            swiper.animating = false;
            const triggerEvents = ['webkitTransitionEnd', 'transitionend'];
            for (let i = 0; i < triggerEvents.length; i += 1) {
              $wrapperEl.trigger(triggerEvents[i]);
            }
          });
        }
      },
    },
  };
  pdfs = [];

  constructor(injector: Injector, public renderer2: Renderer2) {
    super(injector);
    this.lang = localStorage.getItem('lang');
  }

  async ngOnInit() {
    await this.initialize();
    this.sync();
  }

  sync() {
    if (!this.isNetworkOngoing) {
      this.sqlite.syncMilestonesData().then(() => {
        this.isNetworkOngoing = !this.isNetworkOngoing;
        console.log(this.isNetworkOngoing);
      });
    }
  }

  async initialize() {
    this.title = this.nav.getQueryParams().title;
    this.icon = this.nav.getQueryParams().icon;
    const lang = await this.storageService.get('lang');
    // this.data = this.jsonService.getJsonData('milestones');
    this.data = await this.sqlite.getMileStonesData(lang);
    console.log('from sqlite', this.data);

    if (this.lang == 'en') {
      for (var i = 0; i < 10; i++) {
        this.pdfs.push(i + 1);
      }
    }

    if (this.lang == 'ar') {
    }

    this.isLoading = false;
  }

  // logEnd = false;
  async logScrolling($event) {
    const el = await this.content.getScrollElement();

    let sct: number;

    // if (total_height > el.scrollHeight - el.offsetHeight) {
    //   sct = (el.scrollTop * (scrollHeight / (height - scrollHeight))) - 152;
    // } else {
    console.log(
      el.scrollTop,
      el.scrollHeight - el.offsetHeight,
      el.offsetHeight,
      el.scrollHeight
    );

    if (el.scrollTop + 152 < el.scrollHeight - el.offsetHeight) {
      sct =
        el.scrollTop * (el.offsetHeight / (el.scrollHeight - el.offsetHeight));
    }

    // }

    this.top = sct;
    let rtop = this.top.toString() + 'px';

    this.renderer2.setStyle(this.scroll.nativeElement, 'margin-top', rtop);
  }

  details(item) {
    // console.log(item);
    this.modals.present(MilestoneDetailComponent, { item: item });
  }

  onSlideChange() {
    this.number++;
    console.log(this.slide_ended);
    this.percent = (this.number / this.pdfs.length) * 100;
  }

  logEndFunc($event) {
    // this.top = 0;
    // this.logScrolling($event);
    // $event.target.complete();
    // this.logEnd = true;
  }
}
