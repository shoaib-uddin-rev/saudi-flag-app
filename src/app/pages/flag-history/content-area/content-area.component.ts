import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  OnInit,
  Renderer2,
  ViewChild,
} from '@angular/core';
import { IonContent } from '@ionic/angular';
import { EventsService } from 'src/app/services/events.service';

@Component({
  selector: 'app-content-area',
  templateUrl: './content-area.component.html',
  styleUrls: ['./content-area.component.scss'],
})
export class ContentAreaComponent implements OnInit {
  @ViewChild('scroll', { read: ElementRef }) public scroll: ElementRef<any>;
  @ViewChild('scrolldiv', { read: ElementRef })
  public scrolldiv: ElementRef<any>;
  @ViewChild('imageView') imageView: ElementRef;
  @ViewChild('htmlcontent') htmlcontent: ElementRef;
  @ViewChild('parentScroll') parentScroll: ElementRef;

  @Input() image;
  // @Input() content;
  @Input() slide;
  @Input() showScroll = true;
  @Input() lang = 'en';
  top = 0;
  textHeight = 240;
  imageHeight = 0;
  textScrollHeight = 240;
  options = { autoHide: false };
  private tcontentHeight: number;

  get contentHeight(): number {
    return this.tcontentHeight;
  }

  @Input() set contentHeight(value: number) {
    this.tcontentHeight = value;
    console.log(value);
  }

  private tcontent: any;

  get content(): any {
    return this.tcontent;
  }

  @Input() set content(value: any) {
    this.tcontent = value;
    this.getImageHeight();
  }

  constructor(public renderer2: Renderer2) {
    this.lang = localStorage.getItem('lang');
  }

  ngOnInit() {}

  getImageHeight() {
    setTimeout(async () => {
      const scrollElement = await this.imageView.nativeElement;
      const p = scrollElement.clientHeight;

      const textElement = await this.htmlcontent.nativeElement;
      const q = textElement.scrollHeight;

      console.log(q);
      if (p != 0) {
        this.imageHeight = p;
        console.log('image height', p);
        console.log('text height', q);
        console.log('scroll height', this.contentHeight);

        // this.textHeight = this.contentHeight - p - 40;
        console.log(this.textHeight);
      }
    }, 1000);
  }

  async logScrolling($event) {
    const offset = $event.target.offsetHeight;
    // console.log($event);
    const scrollTop = $event.target.scrollTop;
    const scrollHeight = $event.target.scrollHeight;

    const parent = await this.parentScroll.nativeElement;
    const po = parent.scrollTop;
    // console.log('po' + po);
    // console.log('parent' + parent);
    // let _offset = scrollHeight > 1000 ? (offset + scrollHeight) : offset - 200;
    // const denominator: number = scrollHeight > 1000 ? (_offset + scrollTop + 2000) : _offset + scrollTop + 250;
    // const sct: number = (scrollHeight) / denominator * scrollTop;
    console.log(scrollHeight);
    const sh = scrollHeight > 1000 ? scrollHeight + 150 : scrollHeight - 100;
    // console.log('sh' + sh);

    this.top = (this.textHeight / sh) * po; //this.top + 5;
    // console.log(scrollHeight, po, this.textHeight, this.top);
    // if (po >= offset) {
    //   return;
    // } else {
    let rtop = this.top.toString() + 'px';
    // console.log(rtop);
    this.renderer2.setStyle(this.scroll.nativeElement, 'margin-top', rtop);
    // }
  }
}
