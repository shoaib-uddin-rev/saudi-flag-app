import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FlagHistoryPageRoutingModule } from './flag-history-routing.module';

import { FlagHistoryPage } from './flag-history.page';
import { SharedModule } from 'src/app/components/shared.module';
import { ContentAreaComponent } from './content-area/content-area.component';
import { SimplebarAngularModule } from 'simplebar-angular';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FlagHistoryPageRoutingModule,
    SharedModule,
    SimplebarAngularModule
  ],
  declarations: [FlagHistoryPage, ContentAreaComponent]
})
export class FlagHistoryPageModule {}
