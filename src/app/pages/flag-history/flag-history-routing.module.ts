import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FlagHistoryPage } from './flag-history.page';

const routes: Routes = [
  {
    path: '',
    component: FlagHistoryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FlagHistoryPageRoutingModule {}
