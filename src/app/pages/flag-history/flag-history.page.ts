import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { IonContent, IonSlides } from '@ionic/angular';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-flag-history',
  templateUrl: './flag-history.page.html',
  styleUrls: ['./flag-history.page.scss'],
})
export class FlagHistoryPage extends BasePage implements OnInit {
  @ViewChild('slides', { static: true }) slides: IonSlides;
  @ViewChild('ionContent') public ionContent: IonContent;
  loading = true;
  title: any;
  sort: number = 0;
  icon: any;
  first_slide_data = [];
  second_slide_data = [];
  third_slide_data = [];
  imagenum = 1;
  lang = 'en';
  contentHeight = 0;
  isNetworkOngoing = false;

  constructor(injector: Injector) {
    super(injector);
    this.lang = localStorage.getItem('lang');
  }

  async ngOnInit() {
    await this.initialize();
    this.sync();
  }

  async sync() {
    if (!this.isNetworkOngoing) {
      await this.sqlite.syncFlagHistoryData();
      this.isNetworkOngoing = false;
    }
  }

  async initialize() {
    this.title = this.nav.getQueryParams().title;
    this.icon = this.nav.getQueryParams().icon;
    // this.network.getFlagHistory().then((res) => {
    //   console.log(res);
    // })
    const key = 'flag_history';
    // const res = this.jsonService.getJsonData(key);
    const res = await this.sqlite.getFlagHistoryData(this.lang);
    this.first_slide_data = res.filter((x) => x.sort == '1');
    this.second_slide_data = res.filter((x) => x.sort == '2');
    this.third_slide_data = res.filter((x) => x.sort == '3');
    this.loading = false;

    const scrollElement = await this.ionContent.getScrollElement();
    this.contentHeight = scrollElement.scrollHeight;
  }

  async moveSlide(num) {
    this.sort = num;

    const scrollElement = await this.ionContent.getScrollElement();
    this.contentHeight = scrollElement.scrollHeight;
    console.log(this.contentHeight);
  }
}
