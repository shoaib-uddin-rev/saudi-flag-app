import { Component, Injector, OnInit } from '@angular/core';
import { FlagRuleDesComponent } from 'src/app/components/flag-rule-des/flag-rule-des.component';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-flag-rules',
  templateUrl: './flag-rules.page.html',
  styleUrls: ['./flag-rules.page.scss'],
})
export class FlagRulesPage extends BasePage implements OnInit {

  title: any;
  icon: any;
  data: any;
  isLoading = true;

  constructor(
    injector: Injector
  ) {
    super(injector)
  }

  async ngOnInit() {
    await this.initialize();
  }

  async initialize() {
    this.title = this.nav.getQueryParams().title;
    this.icon = this.nav.getQueryParams().icon;
    const key = 'flag_rules';
    this.data = this.jsonService.getJsonData(key);
    this.isLoading = false;
  }

  desc(item) {
    this.modals.present(FlagRuleDesComponent, { rule: item })
  }
}
