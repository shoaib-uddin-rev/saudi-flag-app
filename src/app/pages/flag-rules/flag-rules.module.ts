import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FlagRulesPageRoutingModule } from './flag-rules-routing.module';

import { FlagRulesPage } from './flag-rules.page';
import { SharedModule } from 'src/app/components/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FlagRulesPageRoutingModule,
    SharedModule
  ],
  declarations: [FlagRulesPage]
})
export class FlagRulesPageModule {}
