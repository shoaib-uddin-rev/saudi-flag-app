import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FlagRulesPage } from './flag-rules.page';

const routes: Routes = [
  {
    path: '',
    component: FlagRulesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FlagRulesPageRoutingModule {}
