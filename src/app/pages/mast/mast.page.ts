import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-mast',
  templateUrl: './mast.page.html',
  styleUrls: ['./mast.page.scss'],
})
export class MastPage extends BasePage implements OnInit {

  title: any;
  icon: any;
  num = 0;
  data: any;
  isLoading = true;
  flag_height: any;
  flag_area: any;
  flag_weight: any;
  flag_total_area: any
  language: any;
  isNetworkOngoing = false;

  constructor(
    injector: Injector
  ) {
    super(injector)
  }

  async ngOnInit() {
    await this.initialize();
    this.sync();
  }

  async sync() {
    if (!this.isNetworkOngoing) {
      await this.sqlite.syncMastData();
      this.isNetworkOngoing = false;
    }
  }

  async initialize() {
    this.language = await this.storageService.get('lang');
    this.title = this.nav.getQueryParams().title;
    this.icon = this.nav.getQueryParams().icon;
    const lang = this.jsonService.getJsonData('mast');
    // console.log(this.data);
    this.data = await this.sqlite.getMastData(this.language);
    console.log(lang);
    this.flag_area = lang.filter((x) => x.key == 'flag_area');
    this.flag_height = lang.filter((x) => x.key == 'flag_height');
    this.flag_total_area = lang.filter((x) => x.key == 'flag_total_area');
    this.flag_weight = lang.filter((x) => x.key == 'flag_weight');
    console.log(this.flag_weight)
    this.isLoading = false;
  }

  returnBg(img) {
    return `url("./../../../assets/imgs/mast/${img}.jpg")`;
  }


  openLatLong(data){
    console.log(data);
    this.utility.openMap(data);
  }

}
