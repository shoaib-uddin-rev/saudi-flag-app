import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MastPage } from './mast.page';

const routes: Routes = [
  {
    path: '',
    component: MastPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MastPageRoutingModule {}
