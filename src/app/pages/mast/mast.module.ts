import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MastPageRoutingModule } from './mast-routing.module';

import { MastPage } from './mast.page';
import { SharedModule } from 'src/app/components/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MastPageRoutingModule,
    SharedModule
  ],
  declarations: [MastPage]
})
export class MastPageModule {}
