import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { EventsService } from './events.service';
import { ApiService } from './api.service';
import { UtilityService } from './utility.service';

@Injectable({
  providedIn: 'root',
})
export class NetworkService {
  constructor(
    public utility: UtilityService,
    public api: ApiService,
    private events: EventsService
  ) {
    // console.log('Hello NetworkProvider Provider');
  }

  getSplashData() {
    return this.httpGetResponse('get_splash_data', null, null);
  }

  getDashboardData() {
    return this.httpGetResponse('get_dashboard_data', null);
  }

  getMilestones() {
    return this.httpGetResponse('get_milestone_data', null);
  }

  getFlagHistory() {
    return this.httpGetResponse('get_flaghistory_data', null);
  }

  getFlagRulesMain() {
    return this.httpGetResponse('get_flagrules_data', null);
  }

  getFlagRuleData(key) {
    return this.httpGetResponse('get_flagrules_data/' + key, null);
  }

  getMastData() {
    return this.httpGetResponse('get_mast_data', null);
  }

  getGalleryImages() {
    return this.httpGetResponse('get_gallery_data', null);
  }

  postContactUsData(data) {
    console.log('contact data', data);
    return this.httpPostResponse('post_contact_us', data, null);
  }
  // get requests -- end

  httpPostResponse(
    key,
    data,
    id = null,
    showloader = false,
    showError = false,
    contenttype = 'application/json'
  ) {
    return this.httpResponse(
      'post',
      key,
      data,
      id,
      showloader,
      showError,
      contenttype
    );
  }

  httpGetResponse(
    key,
    id = null,
    showloader = false,
    showError = false,
    contenttype = 'application/json'
  ) {
    return this.httpResponse(
      'get',
      key,
      {},
      id,
      showloader,
      showError,
      contenttype
    );
  }

  httpPatchResponse(
    key,
    data,
    id = null,
    showloader = false,
    showError = false,
    contenttype = 'application/json'
  ) {
    return new Promise((resolve, reject) => {
      id = id ? `/${id}` : '';
      const url = key + id;

      this.api.patch(key, data).subscribe((res: any) => {
        if (res.bool !== true) {
          if (showError) {
            this.utility.presentSuccessToast(res.message, '');
          }
          reject(null);
        } else {
          resolve(res.result);
        }
      });
    });
  }

  // default 'Content-Type': 'application/json',
  httpResponse(
    type = 'get',
    key,
    data,
    id = null,
    showloader = false,
    showError = true,
    contenttype = 'application/json'
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      if (showloader == true) {
        this.utility.showLoader();
      }

      const _id = id ? '/' + id : '';
      const url = key + _id;

      const seq =
        type == 'get' ? this.api.get(url, {}) : this.api.post(url, data);

      seq.subscribe(
        (res: any) => {
          if (showloader === true) {
            this.utility.hideLoader();
          }

          if (res.bool !== true) {
            if (showError) {
              this.utility.presentSuccessToast(res.message, '');
            }
            reject(null);
          } else {
            resolve(res.data);
          }
        },
        (err) => {
          const error = err.error;
          if (showloader === true) {
            this.utility.hideLoader();
          }

          if (showError) {
            this.utility.presentFailureToast(error.message, '');
          }

          console.log(err);

          // if(err.status === 401){
          //   this.router.navigate(['splash']);
          // }

          reject(null);
        }
      );
    });
  }

  showFailure(err) {
    // console.error('ERROR', err);
    const _error = err ? err.message : 'check logs';
    this.utility.presentFailureToast(_error, '');
  }

  getActivityLogs() {
    return this.httpGetResponse('get_activity_logs', null, true);
  }
}
