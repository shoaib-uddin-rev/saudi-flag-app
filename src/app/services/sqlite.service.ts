import { Injectable, Injector } from '@angular/core';
import { Platform } from '@ionic/angular';
import { NetworkService } from './network.service';
import { UtilityService } from './utility.service';
import { StorageService } from './basic/storage.service';
import { CapacitorSQLite, JsonSQLite } from '@capacitor-community/sqlite';
import { Capacitor } from '@capacitor/core';
import { Network } from '@capacitor/network';

declare let window: any;
const SQL_DB_NAME = '__flagapp.db';

@Injectable({
    providedIn: 'root'
})

export class SqliteService {

    db: any;
    msg = 'Sync In Progress ...';
    milestones = require('./../../assets/json/milestones.json');
    flag_history = require('./../../assets/json/flag_history.json');
    flag_rules = require('./../../assets/json/flag_rules.json');
    mast = require('./../../assets/json/mast.json');
    interval;

    constructor(
        injector: Injector,
        // public sqlite: SQLite,
        public platform: Platform,
        private storage: StorageService,
        private network: NetworkService,
        private utility: UtilityService) { }

    initialize() {
        return new Promise(async resolve => {
            if (Capacitor.getPlatform() != 'web') {
                await this.initializeDatabase();
                resolve(true);
            } else {
                resolve(true);
            }
        });
    }

    async initializeDatabase() {

        return new Promise(async resolve => {
            await this.platform.ready();
            // initialize database object
            const flag = await this.createDatabase();
            if (!flag) {
                resolve(false);
                return;
            }
            // initialize all tables
            this.storage.get('is_database_initialized').then(async v => {
                if (!v) {
                    // await this.initializeSync();
                    await this.initializeMilestonesTable();
                    await this.setMileStonesInDatabase(this.milestones.data);
                    await this.initializeFlaghistoryTable();
                    await this.setFlagHistoryInDatabase(this.flag_history.data);
                    await this.initializeFlagrulesTable();
                    await this.setFlagrulesInDatabase(this.flag_rules.data);
                    await this.initializeMastTable();
                    await this.setMastInDatabase(this.mast.data);
                }
            })
            this.storage.set('is_database_initialized', true);
            resolve(true);
        });
    }

    // initializeSync() {
    //     return new Promise<any>((resolve) => {
    //         this.interval = setInterval(() => {

    //         })
    //     })
    // }

    async createDatabase() {
        return new Promise(async (resolve) => {
            if (Capacitor.getPlatform() != 'web') {
                const dbName = SQL_DB_NAME;
                const dbNames = [SQL_DB_NAME];
                const ret = await CapacitorSQLite.checkConnectionsConsistency({ dbNames: dbNames });
                console.log("SQLITE INITIALIZED", ret);
                if (ret.result == false) {
                    await CapacitorSQLite.createConnection({ database: dbName })
                    await CapacitorSQLite.open({ database: dbName });
                    resolve(true);
                }
                else {
                    await CapacitorSQLite.open({ database: dbName });
                    resolve(true);
                }
            } else {
                this.db = window.openDatabase(
                    SQL_DB_NAME,
                    '1.0',
                    'DEV',
                    5 * 1024 * 1024
                );
                this.msg = 'Database initialized';
                resolve(true);
            }
        });
    }

    async syncMilestonesData() {
        return new Promise<any>(async (resolve) => {
            const status = await Network.getStatus();
            if (status.connected) {
                let update = await this.network.getMilestones();
                if (update) {
                    await this.setMileStonesInDatabase(update);
                }
                resolve(true);
            } else {
                resolve(false);
            }
        })
    }

    async syncFlagHistoryData() {
        return new Promise<any>(async (resolve) => {
            const status = await Network.getStatus();
            if (status.connected) {
                const update = await this.network.getFlagHistory();
                if (update) {
                    await this.setFlagHistoryInDatabase(update);
                }
                console.log(update);
                resolve(true);
            } else {
                resolve(false);
            }
        })
    }

    async syncMastData() {
        return new Promise<any>(async (resolve) => {
            const status = await Network.getStatus();
            if (status.connected) {
                const update = await this.network.getMastData();
                if (update) {
                    await this.setMastInDatabase(update);
                }
                console.log(update);
                resolve(true);
            } else {
                resolve(false);
            }
        })
    }

    async syncFlagRule(key) {
        return new Promise<any>(async (resolve) => {
            const status = await Network.getStatus();
            if (status.connected) {
                const update = await this.network.getFlagRuleData(key);
                if (update) {
                    await this.setFlagrulesInDatabase([update]);
                }
                console.log(update);
                resolve(true);
            } else {
                resolve(false);
            }
        })
    }

    async initializeMilestonesTable() {
        return new Promise(resolve => {
            // create statement
            let sql = 'CREATE TABLE IF NOT EXISTS milestones(';
            sql += 'id INT PRIMARY KEY, ';
            sql += 'title_en TEXT, ';
            sql += 'date_en TEXT, ';
            sql += 'content_en TEXT, ';
            sql += 'title_ar TEXT, ';
            sql += 'date_ar TEXT, ';
            sql += 'content_ar TEXT )';

            this.msg = 'Initializing milestones ...';
            console.log(this.msg);
            resolve(this.execute(sql, []));
        });
    }

    async setMileStonesInDatabase(milestones) {
        return new Promise(async resolve => {

            const insertRows = [];

            for (const row of milestones) {

                let sql = 'INSERT OR REPLACE into milestones(';
                sql += 'id, ';
                sql += 'title_en, ';
                sql += 'date_en, ';
                sql += 'content_en, ';
                sql += 'title_ar, ';
                sql += 'date_ar, ';
                sql += 'content_ar )';

                sql += ' VALUES ';

                const values = [];

                sql += '( ';
                sql += '?, ';
                values.push(row.id);
                sql += '?, ';
                values.push(row.title_en);
                sql += '?, ';
                values.push(row.date_en);
                sql += '?, ';
                values.push(row.content_en);
                sql += '?, ';
                values.push(row.title_ar);
                sql += '?, ';
                values.push(row.date_ar);
                sql += '? ';
                values.push(row.content_ar);
                sql += ') ';

                // await this.execute(sql, values);
                insertRows.push([
                    sql,
                    values
                ]);

            }

            await this.prepareBatch(insertRows);

            resolve(true);

        });
    }

    async initializeFlaghistoryTable() {
        return new Promise(resolve => {
            // create statement
            let sql = 'CREATE TABLE IF NOT EXISTS flag_history(';
            sql += 'id INT PRIMARY KEY, ';
            sql += 'tab TEXT, ';
            sql += 'sort TEXT, ';
            sql += 'date_en TEXT, ';
            sql += 'date_ar TEXT, ';
            sql += 'content_en TEXT, ';
            sql += 'content_ar TEXT )';

            this.msg = 'Initializing flag_history ...';
            console.log(this.msg);
            resolve(this.execute(sql, []));
        });
    }

    async setFlagHistoryInDatabase(history) {
        return new Promise(async resolve => {

            const insertRows = [];

            for (const row of history) {

                let sql = 'INSERT OR REPLACE into flag_history(';
                sql += 'id, ';
                sql += 'tab, ';
                sql += 'sort, ';
                sql += 'date_en, ';
                sql += 'date_ar, ';
                sql += 'content_en, ';
                sql += 'content_ar )';

                sql += ' VALUES ';

                const values = [];

                sql += '( ';
                sql += '?, ';
                values.push(row.id);
                sql += '?, ';
                values.push(row.tab);
                sql += '?, ';
                values.push(row.sort);
                sql += '?, ';
                values.push(row.date_en);
                sql += '?, ';
                values.push(row.date_ar);
                sql += '?, ';
                values.push(row.content_en);
                sql += '? ';
                values.push(row.content_ar);
                sql += ') ';

                // await this.execute(sql, values);
                insertRows.push([
                    sql,
                    values
                ]);

            }

            await this.prepareBatch(insertRows);

            resolve(true);

        });
    }

    async initializeMastTable() {
        return new Promise(resolve => {
            // create statement
            let sql = 'CREATE TABLE IF NOT EXISTS mast(';
            sql += 'id INT PRIMARY KEY, ';
            sql += 'title_en TEXT, ';
            sql += 'flag_height_en TEXT, ';
            sql += 'title_ar TEXT, ';
            sql += 'flag_height_ar TEXT, ';
            sql += 'flag_area_en TEXT, ';
            sql += 'flag_weight_en TEXT, ';
            sql += 'flag_total_area_en TEXT, ';
            sql += 'flag_area_ar TEXT, ';
            sql += 'flag_weight_ar TEXT, ';
            sql += 'flag_total_area_ar TEXT, ';
            sql += 'location_en TEXT, ';
            sql += 'location_ar TEXT, ';
            sql += 'latitude TEXT, ';
            sql += 'longitude TEXT, ';
            sql += 'image TEXT, ';
            sql += 'sort TEXT )';

            this.msg = 'Initializing mast ...';
            console.log(this.msg);
            resolve(this.execute(sql, []));
        });
    }

    async setMastInDatabase(mast) {
        return new Promise(async resolve => {

            const insertRows = [];

            for (const row of mast) {

                let sql = 'INSERT OR REPLACE into mast(';
                sql += 'id, ';
                sql += 'title_en, ';
                sql += 'flag_height_en, ';
                sql += 'title_ar, ';
                sql += 'flag_height_ar, ';
                sql += 'flag_area_en, ';
                sql += 'flag_weight_en, ';
                sql += 'flag_total_area_en, ';
                sql += 'flag_area_ar, ';
                sql += 'flag_weight_ar, ';
                sql += 'flag_total_area_ar, ';
                sql += 'location_en, ';
                sql += 'location_ar, ';
                sql += 'latitude, ';
                sql += 'longitude, ';
                sql += 'image, ';
                sql += 'sort )';

                sql += ' VALUES ';

                const values = [];

                sql += '( ';
                sql += '?, ';
                values.push(row.id);
                sql += '?, ';
                values.push(row.title_en);
                sql += '?, ';
                values.push(row.flag_height_en);
                sql += '?, ';
                values.push(row.title_ar);
                sql += '?, ';
                values.push(row.flag_height_ar);
                sql += '?, ';
                values.push(row.flag_area_en);
                sql += '?, ';
                values.push(row.flag_weight_en);
                sql += '?, ';
                values.push(row.flag_total_area_en);
                sql += '?, ';
                values.push(row.flag_area_ar);
                sql += '?, ';
                values.push(row.flag_weight_ar);
                sql += '?, ';
                values.push(row.flag_total_area_ar);
                sql += '?, ';
                values.push(row.location_en);
                sql += '?, ';
                values.push(row.location_ar);
                sql += '?, ';
                values.push(row.latitude);
                sql += '?, ';
                values.push(row.longitude);
                sql += '?, ';
                values.push(row.image);
                sql += '? ';
                values.push(row.sort);
                sql += ') ';

                // await this.execute(sql, values);
                insertRows.push([
                    sql,
                    values
                ]);

            }

            await this.prepareBatch(insertRows);

            resolve(true);

        });
    }

    async initializeFlagrulesTable() {
        return new Promise(resolve => {
            // create statement
            let sql = 'CREATE TABLE IF NOT EXISTS flag_rules(';
            sql += 'id INT PRIMARY KEY, ';
            sql += 'key TEXT, ';
            sql += 'content_en TEXT, ';
            sql += 'content_ar TEXT )';

            this.msg = 'Initializing flag_rules ...';
            console.log(this.msg);
            resolve(this.execute(sql, []));
        });
    }

    async setFlagrulesInDatabase(rules) {
        return new Promise(async resolve => {

            const insertRows = [];

            for (const row of rules) {

                let sql = 'INSERT OR REPLACE into flag_rules(';
                sql += 'id, ';
                sql += 'key, ';
                sql += 'content_en, ';
                sql += 'content_ar )';

                sql += ' VALUES ';

                const values = [];

                sql += '( ';
                sql += '?, ';
                values.push(row.id);
                sql += '?, ';
                values.push(row.key);
                sql += '?, ';
                values.push(row.content_en);
                sql += '? ';
                values.push(row.content_ar);
                sql += ') ';

                // await this.execute(sql, values);
                insertRows.push([
                    sql,
                    values
                ]);

            }

            await this.prepareBatch(insertRows);

            resolve(true);

        });
    }

    async getMileStonesData(lang) {
        return new Promise(async resolve => {
            const sql = `SELECT id,content_${lang},title_${lang},date_${lang} FROM milestones`;
            const d = await this.execute(sql, []);

            if (!d) {
                resolve(null);
                return;
            }
            // var data = d as any[];
            const data = this.getRows(d);
            let milestones = [];
            if (data.length > 0) {

                data.forEach(element => {
                    let object = {
                        id: element["id"],
                        content: element["content_" + lang],
                        title: element["title_" + lang],
                        date: element["date_" + lang]
                    }
                    milestones.push(object);
                });

                resolve(milestones);
            } else {
                resolve(null);
            }
        })
    }

    async getFlagHistoryData(lang) {
        return new Promise<any[]>(async resolve => {
            const sql = `SELECT id,content_${lang},tab,sort,date_${lang} FROM flag_history`;
            const d = await this.execute(sql, []);

            if (!d) {
                resolve(null);
                return;
            }
            // var data = d as any[];
            const data = this.getRows(d);
            let flag_history = [];
            if (data.length > 0) {

                data.forEach(element => {
                    let object = {
                        id: element["id"],
                        content: element["content_" + lang],
                        date: element["date_" + lang],
                        sort: element["sort"],
                        tab: element["tab"]
                    }
                    flag_history.push(object);
                });

                resolve(flag_history);
            } else {
                resolve(null);
            }
        })
    }

    async getFlagRulesData(lang, key) {
        return new Promise<any>(async resolve => {
            const sql = `SELECT id,key,content_${lang} FROM flag_rules where key = ?`;
            const d = await this.execute(sql, [key]);

            if (!d) {
                resolve(null);
                return;
            }
            // var data = d as any[];
            const data = this.getRows(d);
            if (data.length > 0) {
                const flag_rule = {
                    id: data[0].id,
                    content: data[0]["content_" + lang],
                    key: data[0].key
                };
                resolve(flag_rule);
            } else {
                resolve(null);
            }
        })
    }

    async getMastData(lang) {
        return new Promise<any[]>(async resolve => {
            const sql = `SELECT id,title_${lang},flag_height_${lang},flag_area_${lang},flag_weight_${lang},flag_total_area_${lang},location_${lang},latitude,longitude,image,sort FROM mast`;
            const d = await this.execute(sql, []);

            if (!d) {
                resolve(null);
                return;
            }
            // var data = d as any[];
            const data = this.getRows(d);
            let mast = [];
            if (data.length > 0) {

                data.forEach(element => {
                    let object = {
                        id: element["id"],
                        title: element["title_" + lang],
                        flag_height: element["flag_height_" + lang],
                        flag_area: element["flag_area_" + lang],
                        flag_weight: element["flag_weight_" + lang],
                        flag_total_area: element["flag_total_area_" + lang],
                        location: element["location_" + lang],
                        latitude: element["latitude"],
                        sort: element["sort"],
                        longitude: element["longitude"],
                        image: element["image"],
                    }
                    mast.push(object);
                });

                resolve(mast);
            } else {
                resolve(null);
            }
        })
    }

    execute(sql, params) {

        return new Promise(async resolve => {

            return CapacitorSQLite.query({ database: SQL_DB_NAME, statement: sql, values: params })
                .then((response) => {
                    resolve(response);
                })
                .catch((err) => {
                    console.error(err);
                    resolve(null);
                });

        });
    }

    prepareBatch(insertRows) {

        return new Promise(async resolve => {

            const size = 250; const arrayOfArrays = [];

            for (let i = 0; i < insertRows.length; i += size) {
                arrayOfArrays.push(insertRows.slice(i, i + size));
            }

            for (const element of arrayOfArrays) {
                await this.executeBatch(element);
                // await this.execute(s, p)
            }

            resolve(true);

        });
    }

    executeBatch(array) {

        return new Promise(async resolve => {
            if (!this.db) {
                await this.platform.ready();
                await this.createDatabase();
            }

            let command = array[0][0];

            if (!command) {
                resolve(null);
            }

            let cmd = command.split('VALUES')[0] + "VALUES ";
            let values = [];

            for (let i = 0; i < array.length; i++) {
                let extractedArray = array[i];
                let brackets = array[i][0].split('VALUES')[1];
                cmd += brackets + (i != array.length - 1 ? ", " : "");
                values = values.concat(array[i][1])
            }
            // console.log("batch sql cmd: ", cmd);
            // console.log("batch sql cmd 00 : ", values);

            return CapacitorSQLite.run({ database: SQL_DB_NAME, statement: cmd, values: values })
                .then((response) => {
                    console.log("Response:", { response });
                    resolve(response);
                })
                .catch((err) => {
                    console.error(err);
                    resolve(null);
                });

        });
    }

    getRows(data) {
        const items = [];
        for (let i = 0; i < data.values.length; i++) {
            const item = data.values[i];

            items.push(item);
        }

        return items;
    }

    async getAllRecords(table) {

        return new Promise(async resolve => {
            const sql = 'SELECT * FROM ' + table;
            const values = [];

            const d = await this.execute(sql, values);

            if (!d) {
                resolve([]);
                return;
            }

            // var data = d as any[];
            const data = this.getRows(d);
            if (data.length > 0) {
                resolve(data);
            } else {
                resolve([]);
            }

        });
    }
}
