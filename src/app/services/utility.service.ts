import { StringsService } from './basic/strings.service';
import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { AlertsService } from './basic/alerts.service';
import { LoadingService } from './basic/loading.service';
import { StorageService } from './basic/storage.service';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Config } from '../config/main.config';
import {
  AppLauncher,
  AppLauncherOptions,
} from '@ionic-native/app-launcher/ngx';
import {
  LaunchNavigator,
  LaunchNavigatorOptions,
} from '@ionic-native/launch-navigator/ngx';
import { Capacitor } from '@capacitor/core';
// import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';

@Injectable({
  providedIn: 'root',
})
export class UtilityService {
  alphalist;
  pdfLink = 'http://thisisvantage.com/saudiflag/public';
  constructor(
    public loading: LoadingService,
    public plt: Platform,
    public alerts: AlertsService,
    public strings: StringsService,
    public storage: StorageService,
    private iab: InAppBrowser,
    private appLauncher: AppLauncher,
    private launchNavigator: LaunchNavigator // private photoViewer: PhotoViewer
  ) {}

  // openImage(link){
  //   this.photoViewer.show(link);
  // }

  async openMap(data) {
    // const destination = [data.latitude, data.longitude];

    let destination = data.latitude + ',' + data.longitude;

    if (this.plt.is('ios')) {
      window.open('maps://?q=' + destination, '_system');
    } else {
      let label = encodeURI('Label');
      window.open('geo:0,0?q=' + destination + '(' + label + ')', '_system');
    }
    // this.launchNavigator.navigate(destination)
    //   .then(
    //     success => console.log('Launched navigator'),
    //     error => console.log('Error launching navigator', error)
    //   );

    // const env = Capacitor.getPlatform();

    // const options: LaunchNavigatorOptions = {
    //   start: address,
    //   app: env === 'android' ? this.launchNavigator.APP.GOOGLE_MAPS : this.launchNavigator.APP.APPLE_MAPS
    // };

    // this.launchNavigator.navigate(address, options)
    // .then(
    //   success => console.log('Launched navigator'),
    //   error => console.log('Error launching navigator', error)
    // );

    // this.launchNavigator.navigate(address);
    // const options: LaunchNavigatorOptions = {
    //   start: 'London, ON',
    //   app: LaunchNavigator.APPS.UBER
    // }

    // this.launchNavigator.navigate('Toronto, ON', options)
    //   .then(
    //     success => console.log('Launched navigator'),
    //     error => console.log('Error launching navigator', error)
    //   );
  }
  openPdfLink() {
    const url = this.pdfLink + '/rules-info.pdf';
    this.iab.create(url, '_system');
  }

  async openPdfFile() {}

  async showLoader(msg = 'Please wait...') {
    return await this.loading.showLoader(msg);
  }

  async hideLoader() {
    return await this.loading.hideLoader();
  }

  showAlert(msg, title = 'Alert'): Promise<any> {
    return this.alerts.showAlert(msg, title);
  }

  presentToast(msg) {
    return this.alerts.presentToast(msg);
  }

  presentSuccessToast(msg, cssClass) {
    return this.alerts.presentSuccessToast(msg, cssClass);
  }

  presentFailureToast(msg, cssClass) {
    return this.alerts.presentFailureToast(msg, cssClass);
  }

  presentConfirm(
    okText = 'OK',
    cancelText = 'Cancel',
    title = 'Are You Sure?',
    message = ''
  ): Promise<boolean> {
    return this.alerts.presentConfirm(
      (okText = okText),
      (cancelText = cancelText),
      (title = title),
      (message = message)
    );
  }

  presentInput(
    okText = 'OK',
    cancelText = 'Cancel',
    title = 'Are You Sure?',
    message = ''
  ): Promise<any> {
    return this.alerts.presentInput(
      (okText = okText),
      (cancelText = cancelText),
      (title = title),
      (message = message)
    );
  }

  /** Storage Service */

  setKey(key, value) {
    return this.storage.set(key, value);
  }

  getKey(key) {
    return this.storage.get(key);
  }

  /** Strings Service */

  capitalizeEachFirst(str) {
    return this.strings.capitalizeEachFirst(str);
  }

  checkIfMatchingPasswords(passwordKey, passwordConfirmationKey) {
    return this.strings.checkIfMatchingPasswords(
      passwordKey,
      passwordConfirmationKey
    );
  }
}
