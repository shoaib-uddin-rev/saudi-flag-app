import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class JsonService {
  lang = 'en';

  constructor() {}

  getJsonData(key) {
    this.lang = localStorage.getItem('lang');
    const { data } = require(`./../../assets/json/${this.lang}/${key}.json`);
    return data;
  }

  getSplashData() {
    const { data } = require(`./../../assets/json/splash.json`);
    return data;
  }

  getFlagRulesDesc(key) {
    this.lang = localStorage.getItem('lang');
    const data = require(`./../../assets/json/${this.lang}/flag_rules_desc.json`);
    return data[key];
  }

  getResponseData(key) {
    this.lang = localStorage.getItem('lang');
    const {
      responsemsg,
    } = require(`./../../assets/json/${this.lang}/${key}.json`);
    return responsemsg;
  }
}
