import { NetworkService } from './network.service';
import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AudioService {

  audio: any;
  received: boolean = false;

  constructor(
    public network: NetworkService,
    public platform: Platform) {
  }


  preload() {
    this.audio = new Audio();
    this.audio.src = '../../assets/audio/anthem.mp3';
    this.audio.load();
  }

  play(): void {
    this.audio.play();
  }

  pause(): void {
    this.audio.pause();
    this.audio.currentTime = 0;
  }
}
